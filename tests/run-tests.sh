#!/bin/bash

# Usage
#	./run-tests.sh <test-type> <bin>

_SCRIPT="$(readlink -f ${BASH_SOURCE[0]})"
_MYDIR="$(dirname $_SCRIPT)"

TYPE=$1
_BIN=$2
PWD=`pwd`
BIN="$PWD/$_BIN"

function run {
	printf "File: $1 - "
	${BIN} $1 &> $2
	head -n1 $2 | grep "OK" &> /dev/null
	if [[ $? != 0 ]]; then
		printf "\033[93mWRONG\033[0m\n"
		echo "============ OUTPUT"
		cat $2
		echo "============ SOURCE"
		cat -n $1
	else
		printf "\033[93mOK\033[0m\n"
	fi
}

function run-bad {
	printf "File: $1 - "
	${BIN} $1 &> $2
	head -n1 $2 | grep "ERROR" &> /dev/null
	if [[ $? != 0 ]]; then
		printf "\033[93mWRONG\033[0m\n"
		echo "============ OUTPUT"
		cat $2
		echo "============ SOURCE"
		cat -n $1
		echo
	else
		printf "\033[93mOK\033[0m\n"
	fi
}

function run-backend-test {
	TMP=$1
	CODE=$2
	OUT=$3
	IN=$4
	BC=${CODE%.lat}.bc

	printf "File: $CODE - "
	# Compilation
	${BIN} $CODE &> /dev/null
	if [ "$?" != 0 ]; then
		echo "COMPILATION FAILED!"
		exit 1
	fi
	if [ "$IN" == "" ]; then
		lli ${BC} > ${TMP} 
	else
		lli ${BC} > ${TMP} < ${IN}
	fi
	diff ${TMP} ${OUT} &> /dev/null
	if [[ $? != 0 ]]; then
		printf "\033[93mWRONG\033[0m\n"
		diff ${TMP} ${OUT}
	else
		printf "\033[93mOK\033[0m\n"
	fi
}

case $TYPE in
	test-syntax)
		TMP=/tmp/latte-syntax-test
		echo "========================================================="
		echo "===== RUNNING SYNTAX TESTS"
		echo -e "=========================================================\n\n"
		for p in `find tests -type f -name "*.lat"`
		do
			printf "File: $p - "
			${BIN} < $p &> $TMP
			grep "Parse Succesful!" $TMP &> /dev/null
			if [[ $? != 0 ]]; then
				printf "\033[93mWRONG\033[0m\n"
				echo "============ OUTPUT"
				cat $TMP
				echo "============ SOURCE"
				cat -n $p
			else
				printf "\033[93mOK\033[0m\n"
			fi
		done
		;;

	test-frontend)
		TMP=/tmp/latte-frontend-test
		echo "========================================================="
		echo "===== RUNNING FRONTEND TESTS"
		echo -e "=========================================================\n\n"
		echo "========================================================="
		echo "===== GOOD TESTS"
		echo -e "=========================================================\n\n"
		for p in `find tests -type f -name "*.lat" | grep good`
		do
			run $p $TMP
		done

		for p in `find tests -type f -name "*.lat" | grep extensions`
		do
			run $p $TMP
		done

		echo
		echo "========================================================="
		echo "===== BAD TESTS"
		echo -e "=========================================================\n\n"
		for p in `find tests -type f -name "*.lat" | grep bad`
		do
			run-bad $p $TMP
		done
		;;

	test-backend)
		TMP=/tmp/latte-backend-test
		echo "========================================================="
		echo "===== RUNNING BACKEND TESTS"
		echo -e "=========================================================\n\n"
		echo "========================================================="
		echo "===== TESTS - CORE"
		echo -e "=========================================================\n\n"

		for p in `find tests/lattests -type f -name "*.lat" | sort | grep good`
		do
			# run $p $TMP
			BASE=${p%.*}
			CODE=${BASE}.lat
			IN=${BASE}.input
			OUT=${BASE}.output
			if [[ ! -e $IN ]]; then
				IN=
			fi
			run-backend-test $TMP $CODE $OUT $IN
		done

		for p in `find tests/mrjp-tests -type f -name "*.lat" | grep basic`
		do
			BASE=${p%.*}
			CODE=${BASE}.lat
			IN=${BASE}.input
			OUT=${BASE}.output
			if [[ ! -e $IN ]]; then
				IN=
			fi
			run-backend-test $TMP $CODE $OUT $IN
		done

		echo
		echo "========================================================="
		echo "===== TESTS - ARRAYS"
		echo -e "=========================================================\n\n"
		for p in `find tests/lattests -type f -name "*.lat" | grep arrays`
		do
			BASE=${p%.*}
			CODE=${BASE}.lat
			IN=${BASE}.input
			OUT=${BASE}.output
			if [[ ! -e $IN ]]; then
				IN=
			fi
			run-backend-test $TMP $CODE $OUT $IN
		done
		for p in `find tests/mrjp-tests -type f -name "*.lat" | grep arrays`
		do
			BASE=${p%.*}
			CODE=${BASE}.lat
			IN=${BASE}.input
			OUT=${BASE}.output
			if [[ ! -e $IN ]]; then
				IN=
			fi
			run-backend-test $TMP $CODE $OUT $IN
		done

		echo
		echo "========================================================="
		echo "===== TESTS - STRUCT"
		echo -e "=========================================================\n\n"
		for p in `find tests/lattests -type f -name "*.lat" | grep struct`
		do
			BASE=${p%.*}
			CODE=${BASE}.lat
			IN=${BASE}.input
			OUT=${BASE}.output
			if [[ ! -e $IN ]]; then
				IN=
			fi
			run-backend-test $TMP $CODE $OUT $IN
		done

		echo
		echo "========================================================="
		echo "===== TESTS - OBJECTS"
		echo -e "=========================================================\n\n"
		for p in `find tests/lattests -type f -name "*.lat" | grep objects`
		do
			BASE=${p%.*}
			CODE=${BASE}.lat
			IN=${BASE}.input
			OUT=${BASE}.output
			if [[ ! -e $IN ]]; then
				IN=
			fi
			run-backend-test $TMP $CODE $OUT $IN
		done

		for p in `find tests/mrjp-tests -type f -name "*.lat" | grep virtual`
		do
			BASE=${p%.*}
			CODE=${BASE}.lat
			IN=${BASE}.input
			OUT=${BASE}.output
			if [[ ! -e $IN ]]; then
				IN=
			fi
			run-backend-test $TMP $CODE $OUT $IN
		done

		find tests -type f -name "*.ll" -exec rm {} \;
		find tests -type f -name "*.bc" -exec rm {} \;
		;;

	*)
		echo "Wrong type..."
		exit
		;;
esac

exit

TEST_DIR=tests
TMP=${TEST_DIR}/test-output

function run_tests {
        COMPILER=$1
        for el in $(ls -1 ${TEST_DIR}/*.ins);
        do
                fullname=$(basename ${el})
                basename=$(echo ${fullname} | cut -d'.' -f 1)
                cmp=${TEST_DIR}/${basename}.output

                ./${COMPILER} ${el}

                if [[ ${COMPILER} == insc_jvm ]]; then
                        java -cp ${TEST_DIR} ${basename} > ${TMP}
                else
                        llvm_exec=${TEST_DIR}/${basename}.bc
                        lli ${llvm_exec} > ${TMP}
                fi

		diff $TMP $cmp &> /dev/null
                if [ $? -gt 0 ]; then
                        echo -e "[\033[91mFAIL\033[0m]: \033[93m$el\033[0m"
                        diff $TMP $cmp
                else
                        echo -e "[ \033[92mOK\033[0m ]: \033[93m$el\033[0m"
                fi
                rm -f $TMP ${TEST_DIR}/*.j ${TEST_DIR}/*.class ${TEST_DIR}/*.ll ${TEST_DIR}/*.bc
        done

}


