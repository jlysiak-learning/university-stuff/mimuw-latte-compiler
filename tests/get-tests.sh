#!/bin/bash

_script="$(readlink -f ${BASH_SOURCE[0]})"
_mydir="$(dirname $_script)"

cd $_mydir

if [ ! -e lattests ]; then
        wget https://www.mimuw.edu.pl/~ben/Zajecia/Mrj2018/Latte/lattests121017.tgz
        tar xzvf lattests121017.tgz
        rm lattests121017.tgz
fi

if [ ! -e mrjp-tests ];
then
        git clone https://github.com/tomwys/mrjp-tests 
fi

echo "All tests downloaded!"

