#!/bin/bash

_script="$(readlink -f ${BASH_SOURCE[0]})"
_mydir="$(dirname $_script)"

cd $_mydir

rm -rf lattests
rm -rf mrjp-tests
rm -rf my-tests/*.ll my-tests/*.bc
