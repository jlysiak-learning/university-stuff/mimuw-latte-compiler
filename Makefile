
BUILD_DIR=build
SRC=src

SCRIPTS=${SRC}/scripts
GRAMMAR=${SRC}/grammar/Latte.cf

INCLUDE = ${SRC}/include/

CC=g++
CXXFLAGS = -W -Wall -Wno-unused-parameter -O3 -g -I $(INCLUDE) #-I $(BUILD_DIR)

FLEX=flex
FLEX_OPTS=-PLatte

BISON=bison
BISON_OPTS=-t -pLatte

TAR=latc_llvm

#==============================================================================
#==== BNFC

ifeq ($(shell hostname), students)
	BNFC=/home/students/inf/PUBLIC/MRJP/bin/bnfc
else
	BNFC=bnfc
endif

BNFC_FLAGS = -m -cpp -l -p Latte

BNFC_GEN = Absyn.C Printer.C Latte.y Latte.l
BNFC_OUT=$(addprefix ${BUILD_DIR}/, ${BNFC_GEN})

BNFC_SRCS = Absyn.C Parser.C Lexer.C Printer.C
BNFC_OBJS = $(patsubst %.C,%.o,$(BNFC_SRCS))


#==============================================================================
#==== LATTE COMPILER 

LATTE_SRCS = $(notdir $(shell find $(SRC) -type f -name "*.cpp"))
LATTE_OBJS = $(patsubst %.cpp,%.o,$(LATTE_SRCS))

PATH_SRCS = $(SRC)/source/$(shell find $(SRC) -type d)
vpath %.cpp $(PATH_SRCS)

# Main binary
COMPILER = $(BUILD_DIR)/LatteCompiler 

#==============================================================================

all: build $(TAR)

build:
	mkdir build
	cp $(SRC)/include/latte/latte/Absyn.h $(BUILD_DIR)/Absyn.H

latc_llvm: $(COMPILER)
	cp $(SCRIPTS)/$@ $@

latf: $(COMPILER)
	cp $(SCRIPTS)/$@ $@

$(COMPILER): $(addprefix $(BUILD_DIR)/,$(BNFC_OBJS)) $(addprefix $(BUILD_DIR)/,$(LATTE_OBJS))
	$(CC)  $^ -o $@

$(BUILD_DIR)/%.o: %.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

# Compile 
$(BUILD_DIR)/%.o: $(BUILD_DIR)/%.C  
	$(CC) $(CXXFLAGS) -c $< -o $@

# Build generated parser
latp: $(BNFC_OUT)
	$(MAKE) -C ${BUILD_DIR}
	cp $(BUILD_DIR)/TestLatte $@

parser-report.txt: $(BUILD_DIR)/Latte.y
	cd $(BUILD_DIR); bison -v --locations -t -x -g \
		--report='all' --report-file=../$@ Latte.y

$(BUILD_DIR)/Lexer.C: $(BUILD_DIR)/Latte.l
	cp $(SRC)/include/latte/latte/Parser.h $(BUILD_DIR)/Parser.H
	cd $(BUILD_DIR); ${FLEX} -o Lexer.C Latte.l

$(BUILD_DIR)/Parser.C: $(BUILD_DIR)/Latte.y
	cp $(SRC)/include/latte/latte/Parser.h $(BUILD_DIR)/Parser.H
	cd $(BUILD_DIR); ${BISON} Latte.y -o Parser.C

# Check grammar file and generate parser & lexer
$(BNFC_OUT): $(GRAMMAR)
	$(BNFC) $(BNFC_FLAGS) -o $(BUILD_DIR) $<

test-syntax: latp
	./tests/get-tests.sh
	./tests/run-tests.sh $@ $<

test-frontend: latf build
	./tests/get-tests.sh
	./tests/run-tests.sh $@ $<

test-backend: latc_llvm build 
	./tests/get-tests.sh
	./tests/run-tests.sh $@ $<

clean:
	rm -rf $(BUILD_DIR) $(TAR) parser-report.txt latp latf

cleanall: clean
	./tests/rm-tests.sh

.PHONY: clean cleanall
