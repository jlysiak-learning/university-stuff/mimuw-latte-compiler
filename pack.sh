#!/bin/bash

FILE=$1
NAME=jl345639-latte
mkdir $NAME
cp -r src lib README Makefile tests $NAME

tar czvf ${FILE}.tar.gz $NAME
rm -rf $NAME

