#!/bin/bash

FILE=runtime.cpp
filename="${FILE%.*}"

clang++ -O0 -o ${filename}.ll -emit-llvm -S $FILE
llvm-as ${filename}.ll
