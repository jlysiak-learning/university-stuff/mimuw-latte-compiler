// Jacek Lysiak
//
// Latte runtime library
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


extern "C" {

int readInt() {
	int x;
	scanf("%d", &x);
	return x;
}

char * readString() {
	char buff[256];
	scanf("%255s", buff);
	char * s = (char *) calloc(strlen(buff) + 1, sizeof(char));
	strcpy(s, buff);
	return s;
}

void printString(char * s) {
	printf("%s\n", s);
}

void printInt(int x) {
	printf("%d\n", x);
}

void error() {
	printf("runtime error\n");
	exit(1);
}

char * _concat(const char *a, const char *b) {
	unsigned int l = strlen(a) + strlen(b) + 1;
	char * n = (char *) calloc(l, sizeof(char));
	strcat(n,a);
	strcat(n,b);
	return n;
}

bool _strcmp(const char *a, const char *b) {
	return strcmp(a, b) == 0;
}

bool _strcmpn(const char *a, const char *b) {
	return !_strcmp(a, b);
}

char * _calloc(int sz) {
	return (char *) calloc(sz, 1); 
}

}
