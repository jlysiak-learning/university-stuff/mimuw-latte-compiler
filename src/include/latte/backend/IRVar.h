#ifndef __LATTE_IRVAR__
#define __LATTE_IRVAR__

#include <string>
#include <iostream>
#include <latte/backend/IRType.h>

namespace Backend {

class IRVar : public IRObject {
public:
	IRVar(std::shared_ptr<IRType> t, std::string id) : _type(t->clone()), _id(id) {}
	IRVar(std::shared_ptr<IRVar> var, std::string id) : _type(var->_type->clone()), _id(id) {}

	virtual std::string code() const; // Generated code `type %id`
	virtual std::string lstr() const; // Label string `%id`
	std::string tstr() const; // Type string

	bool ptr() const {return _type->ptr(); }
	void to_ptr() { _type->set_ptr(true); }
	void no_ptr() { _type->set_ptr(false); }

	std::shared_ptr<IRType> type() { return _type; }

protected:
	std::shared_ptr<IRType> _type;
	std::string _id;
};

class IRConst : public IRVar {
public:
	IRConst(int i) : 
		IRVar(std::make_shared<IRInt>(), std::to_string(i)) {}

	IRConst(bool b) : 
		IRVar(std::make_shared<IRBool>(), (b?"true":"false")) {}

	IRConst() : 
		IRVar(std::make_shared<IRVoid>(),"") {}

	IRConst(std::shared_ptr<IRType> t, std::string l) : 
		IRVar(t, l) {}

	virtual std::string lstr() const;
};

class IRNull : public IRVar {
public:
	IRNull() : IRVar(std::make_shared<IRString>(), "null") {}
	virtual std::string lstr() const;
};

}

#endif 

