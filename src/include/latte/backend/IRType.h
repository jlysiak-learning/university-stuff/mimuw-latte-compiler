#ifndef __LATTE_IRTYPE__
#define __LATTE_IRTYPE__

#include <string>
#include <memory>
#include <iostream>

#include <latte/common/Type.h>
#include <latte/backend/IRObject.h>

namespace Backend {

typedef enum {
	IRTYPE_NULL,	// `null`
	IRTYPE_VOID,	// `void`
	IRTYPE_BOOL,	// `i1`
	IRTYPE_STRING,	// `i8*`
	IRTYPE_INT,	// `i32`
	IRTYPE_CHARARR,	// `[ N x i8 ]`
	IRTYPE_CLASS,	// custom
	IRTYPE_VTABLE,	// custom
	IRTYPE_FUN	// custom
} irtype_t;

class IRType : public IRObject {
public:
	IRType(bool isPtr) : _ptr(isPtr) {}
	IRType(const IRType& t) : _ptr(t._ptr) {}

	static std::shared_ptr<IRType> from_common_type(std::shared_ptr<Common::Type>);

	std::string code() const { return ptr() ? typestr() + "*" : typestr(); }
	virtual std::string typestr() const = 0;
	virtual std::shared_ptr<IRType> clone() const = 0;

	virtual irtype_t type() const = 0;
	template <class T> bool is() const { return type()  == T::myType; }

	bool ptr() const { return _ptr; }
	void set_ptr(bool v) { _ptr = v; }
	
	virtual	int type_size() const = 0;
	int ptr_size() const { return 8; }
private:
	bool _ptr;
};


class IRVoid : public IRType {
public:
	IRVoid() : IRType(false) {}

	std::shared_ptr<IRType> clone() const { return std::make_shared<IRVoid>(); }

	static const irtype_t myType = IRTYPE_VOID;
	irtype_t type() const { return myType; }
	std::string typestr() const { return std::string("void"); }

	virtual	int type_size() const { return 0; }
};

class IRBool : public IRType {
public:
	IRBool() : IRType(false) {}
	IRBool(bool p) : IRType(p) {}
	
	std::shared_ptr<IRType> clone() const { return std::make_shared<IRBool>(ptr()); }

	static const irtype_t myType = IRTYPE_BOOL;
	irtype_t type() const { return myType; }

	std::string typestr() const { return std::string("i1"); }

	int type_size() const { return 1; }
};

class IRInt : public IRType {
public:
	IRInt() : IRType(false) {}
	IRInt(bool p) : IRType(p) {}

	std::shared_ptr<IRType> clone() const { return std::make_shared<IRInt>(ptr()); }

	static const irtype_t myType = IRTYPE_INT;
	irtype_t type() const { return myType; }

	std::string typestr() const { return std::string("i32"); }

	int type_size() const { return 4; }

};

class IRString : public IRType {
public:
	IRString() : IRType(false) {}
	IRString(bool p) : IRType(p) {}

	std::shared_ptr<IRType> clone() const { return std::make_shared<IRString>(ptr()); }

	static const irtype_t myType = IRTYPE_STRING;
	irtype_t type() const { return myType; }

	std::string typestr() const { return std::string("i8*"); }
	
	int type_size() const { return 8; }
};

class IRCharArr : public IRType {
public:
	IRCharArr(int l) : IRType(false), _len(l) {}
	IRCharArr(bool p, int l) : IRType(p), _len(l) {}
	IRCharArr(const IRCharArr& arr) : IRType(arr), _len(arr._len) {}

	std::shared_ptr<IRType> clone() const { return std::make_shared<IRCharArr>(*this); }

	static const irtype_t myType = IRTYPE_CHARARR;
	irtype_t type() const { return myType; }

	std::string typestr() const { return std::string("[ " + std::to_string(_len) + " x i8 ]"); }

	int type_size() const { return _len; }
private:
	int _len;
};

class IRClass : public IRType {
public:
	IRClass(std::string label) : IRType(false) , _label(label) {}
	IRClass(std::string label, bool p) : IRType(p) , _label(label) {}

	std::shared_ptr<IRType> clone() const { return std::make_shared<IRClass>(_label, ptr()); }

	static const irtype_t myType = IRTYPE_CLASS;
	irtype_t type() const { return myType; }

	std::string typestr() const { return "%class." + _label + "*"; }
	int type_size() const { return 8; }

	std::string name() { return _label; }

private:
	std::string _label;
};

class IRVTable : public IRType {
public:
	IRVTable(std::string tstr) : IRType(false), _label(tstr) {};

	std::shared_ptr<IRType> clone() const { return std::make_shared<IRVTable>(_label); }

	static const irtype_t myType = IRTYPE_VTABLE;
	irtype_t type() const { return myType; }

	std::string typestr() const { return _label + "*"; }
	int type_size() const { return 8; }

	std::string name() { return _label; }

private:
	std::string _label;
};

class IRFun : public IRType {
public:
	IRFun(std::string tstr) : IRType(false), _label(tstr) {};

	std::shared_ptr<IRType> clone() const { return std::make_shared<IRFun>(_label); }

	static const irtype_t myType = IRTYPE_FUN;
	irtype_t type() const { return myType; }

	std::string typestr() const { return _label; }
	int type_size() const { return 8; }

	std::string name() { return _label; }

private:
	std::string _label;
};
}

#endif 

