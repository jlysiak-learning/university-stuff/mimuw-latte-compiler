#ifndef __LATTE_IRBLOCK__
#define __LATTE_IRBLOCK__

#include <string>
#include <iostream>
#include <memory>
#include <vector>

#include <latte/backend/IRCode.h>

namespace Backend {

class IRBlock {
public:
	IRBlock(std::string label);

	friend std::ostream& operator<< (std::ostream& output, IRBlock const& b);

	std::string str() const;
	std::string label();

	void add_code(std::shared_ptr<IRCode>);

	bool ends_with_retvoid() const;
	bool ends_with_ret() const;
	bool ends_with_goto() const;
	bool ends_with_if() const;

	std::string fst_exit_label() const;
	std::string snd_exit_label() const;

	const std::vector<std::string>& entry_labels() const { return _entries; }
	void add_entry_label(std::string label);

private:
	std::vector<std::shared_ptr<IRCode>> _code;
	std::vector<std::string> _entries;
};

}

#endif 

