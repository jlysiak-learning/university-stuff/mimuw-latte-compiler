#ifndef __LATTE_IRGENERATOR__
#define __LATTE_IRGENERATOR__ 

#include <memory>
#include <vector>
#include <string>
#include <iostream>

#include <latte/common/FunctionDefinition.h>
#include <latte/common/Environment.h>
#include <latte/common/Logger.h>

#include <latte/backend/IRFunction.h>
#include <latte/backend/IREnvironment.h>
#include <latte/backend/LabelGenerator.h>
#include <latte/backend/IRClassDef.h>

namespace Backend {

using namespace Latte;

class IRGenerator {
public:
	IRGenerator(Common::Environment &env, Common::Logger &logger);
	void generate();
	void print(std::ostream& output);

private:
	Common::Environment& _env;
	Common::Logger& _logger;

	std::map<std::string, std::shared_ptr<IRFunction> > _functions;
	std::map<std::string, std::shared_ptr<IRGlobalString> > _irGlobalStrings;
	std::map<std::string, std::shared_ptr<IRDeclaration> > _irDeclarations;
	std::map<std::string, std::shared_ptr<IRClassDef> > _irClasses;

	 void _decompose_function(
		 std::shared_ptr<IREnvironment> env, 
		 std::shared_ptr<Common::FunctionDefinition> fun);
	 void _create_class_hierarchy();
};

}

#endif 

