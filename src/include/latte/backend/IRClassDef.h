#ifndef __LATTE_IRCLASS__
#define __LATTE_IRCLASS__

#include <string>
#include <map>
#include <vector>

#include <latte/common/ClassDefinition.h>
#include <latte/backend/IRType.h>
#include <latte/backend/IRFunction.h>


namespace Backend {

class IRClassDef {
public:
	IRClassDef(std::string name) : _name(name) {}
	IRClassDef(std::shared_ptr<Common::ClassDefinition>); 

	int size() const { return _size; }

	void add_property(std::shared_ptr<IRType>, std::string);
	void declare_method(std::string, std::string);
	void add_method(std::shared_ptr<IRFunction>);
	const std::map<std::string, std::string > &  methods_map() { return _methodNameMap; }
	int get_method_idx(std::string);
	std::shared_ptr<IRFunction> get_method(std::string name) { return _methodMap[name]; }

	std::string type_declaration() const; 
	std::string type_label() const; 
	std::shared_ptr<IRType> type_objptr() const;

	bool has_vtable() const { return _methodOrder.size() > 0; } 
	std::string vtable_label() const; 
	std::string vtable_type_label() const; 
	std::string vtable_typedef() const; 
	std::string vtable_definition() const; 
	std::shared_ptr<IRVar> vtable_var() const;
	std::shared_ptr<IRType> vtable_objptr() const;


	int get_property_num(std::string id) const { return _orderMap.at(id); }
	bool has_property(std::string id) const { return _orderMap.find(id) != _orderMap.end(); }
	std::shared_ptr<IRType> get_property_type(std::string id) const { return _typeMap.at(id); }

private:
	std::string _name;
	int _size;
	int _align;
	std::map<std::string, int> _orderMap;
	std::map<std::string, std::shared_ptr<IRType> > _typeMap;
	std::vector<std::string> _propertyOrder;

	// For vtable generation
	std::map<std::string, std::shared_ptr<IRFunction> > _methodMap;
	std::map<std::string, std::string > _methodNameMap;
	std::vector<std::string> _methodOrder;

	void _update();
	void _add_from(std::shared_ptr<Common::ClassDefinition>);

};

}

#endif 

