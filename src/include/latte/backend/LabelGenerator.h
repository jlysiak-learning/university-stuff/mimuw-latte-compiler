#ifndef __LATTE_LABELGENERATOR__
#define __LATTE_LABELGENERATOR__


#include <string>


namespace Backend {

class LabelGenerator {
public:
	LabelGenerator() : _tmp_id(0), _block_id(0), _glob_id(0) {}

	std::string next_temp_label();
	std::string next_block_label();
	std::string next_glob_label();

private:
	int _tmp_id;
	int _block_id;
	int _glob_id;
	
};

}

#endif 
