#ifndef __LATTE_DECLARATION__
#define __LATTE_DECLARATION__

#include <vector>
#include <memory>
#include <latte/backend/IRObject.h>
#include <latte/backend/IRType.h>

namespace Backend {

class IRDeclaration : public IRObject {
public:
	IRDeclaration(
			std::string, 
			std::shared_ptr<IRType>
		);
	void add_arg(std::shared_ptr<IRType>);
	std::string code() const;

private:
	std::string _name;
	std::shared_ptr<IRType> _type;
	std::vector<std::shared_ptr<IRType> > _argstype;
};

}

#endif
