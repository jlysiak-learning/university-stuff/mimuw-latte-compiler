#ifndef __LATTE_IROBJECT__
#define __LATTE_IROBJECT__

#include <string>
#include <iostream>

namespace Backend {

class IRObject {
public:
	virtual std::string code() const = 0; // Code generation
	friend std::ostream& operator<< (std::ostream& output, const IRObject& t);
};

}

#endif 

