#ifndef __LATTE_IRCODE__
#define __LATTE_IRCODE__

#include <string>
#include <memory>
#include <utility>
#include <vector>

#include <latte/common/Type.h>
#include <latte/common/MulOp.h>
#include <latte/common/AddOp.h>
#include <latte/common/RelOp.h>

#include <latte/backend/IRVar.h>
#include <latte/backend/IRGlobalString.h>

namespace Backend {

typedef enum {
	IRCODE_LABEL,
	IRCODE_GOTO,
	IRCODE_RET,
	IRCODE_RETV,
	IRCODE_IF,
	IRCODE_MEMB,
	IRCODE_LOAD,
	IRCODE_STORE,
	IRCODE_ADD,
	IRCODE_MUL,
	IRCODE_REL,
	IRCODE_CONCAT,
	IRCODE_AND,
	IRCODE_OR,
	IRCODE_NOT,
	IRCODE_ALLOC,
	IRCODE_PHI,
	IRCODE_CALL,
	IRCODE_GETGLOBALSTR,
	IRCODE_CAST,
	IRCODE_GETPROP,
	IRCODE_GETELEM,
	IRCODE_TRUNC,
	IRCODE_ZEXT,
	IRCODE_CALLPTR
} ircodetype_t;

class IRCode : public IRObject {
public:
	virtual std::string code() const = 0;
	virtual ircodetype_t type() const = 0;

	template <class T> bool is() const { return type()  == T::myType; }
};

class IRCodeGoto: public IRCode {
public:
	static const ircodetype_t myType = IRCODE_GOTO;

	IRCodeGoto(std::string label) : _label(label) {}
	std::string code() const;
	std::string label() const { return _label; }
	ircodetype_t type() const { return myType; }
private:
	std::string _label;
};

class IRCodeLabel: public IRCode {
public:
	static const ircodetype_t myType = IRCODE_LABEL;
	IRCodeLabel(std::string label) : _label(label) {}
	std::string code() const;
	std::string label() const { return _label; }
	ircodetype_t type() const { return myType; }
private:
	std::string _label;
};

class IRCodeRet: public IRCode {
public:
	IRCodeRet(std::shared_ptr<IRVar> var) : _var(var) {}
	std::string code() const;

	static const ircodetype_t myType = IRCODE_RET;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _var;
};

class IRCodeRetV: public IRCode {
public:
	IRCodeRetV() {}
	std::string code() const;

	static const ircodetype_t myType = IRCODE_RETV;
	ircodetype_t type() const { return myType; }
private:
};

class IRCodeIf: public IRCode {
public:
	IRCodeIf(std::shared_ptr<IRVar> var, std::string lt, std::string lf) : _var(var), _lt(lt), _lf(lf) {}
	std::string code() const;

	static const ircodetype_t myType = IRCODE_IF;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _var;
	std::string _lt, _lf;
};

class IRCodeMemb: public IRCode {
public:
	IRCodeMemb(std::shared_ptr<IRVar> tar,
		std::shared_ptr<IRVar> from, 
		std::string membName) : 
		_tar(tar), _from(from), _membName(membName) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_MEMB;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _tar, _from;
	std::string _membName;
};

class IRCodeLoad: public IRCode {
public:
	IRCodeLoad(std::shared_ptr<IRVar> where,
		std::shared_ptr<IRVar> from) : 
		_where(where), _from(from) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_LOAD;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _where, _from;
};

class IRCodeStore: public IRCode {
public:
	IRCodeStore(std::shared_ptr<IRVar> where,
		std::shared_ptr<IRVar> what) : 
		_where(where), _what(what) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_STORE;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _where, _what;
};

class IRCodeAlloc: public IRCode {
public:
	IRCodeAlloc(std::shared_ptr<IRVar> where,
		std::shared_ptr<IRType> what) : 
		_where(where), _what(what) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_ALLOC;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _where;
	std::shared_ptr<IRType> _what;
};

class IRCodeAdd: public IRCode {
public:
	IRCodeAdd(std::shared_ptr<IRVar> res,
		Common::addop_t op,
		std::shared_ptr<IRVar> x1,
		std::shared_ptr<IRVar> x2) : 
		_op(op), _res(res), _x1(x1), _x2(x2) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_ADD;
	ircodetype_t type() const { return myType; }
private:
	Common::addop_t _op;
	std::shared_ptr<IRVar> _res, _x1, _x2;
};

class IRCodeMul: public IRCode {
public:
	IRCodeMul(std::shared_ptr<IRVar> res,
		Common::mulop_t op,
		std::shared_ptr<IRVar> x1,
		std::shared_ptr<IRVar> x2) : 
		_op(op), _res(res), _x1(x1), _x2(x2) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_MUL;
	ircodetype_t type() const { return myType; }
private:
	Common::mulop_t _op;
	std::shared_ptr<IRVar> _res, _x1, _x2;
};


class IRCodeRel: public IRCode {
public:
	IRCodeRel(std::shared_ptr<IRVar> res,
		Common::relop_t op,
		std::shared_ptr<IRVar> x1,
		std::shared_ptr<IRVar> x2) : 
		_op(op), _res(res), _x1(x1), _x2(x2) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_REL;
	ircodetype_t type() const { return myType; }
private:
	Common::relop_t _op;
	std::shared_ptr<IRVar> _res, _x1, _x2;
};

class IRCodeConcat: public IRCode {
public:
	IRCodeConcat(std::shared_ptr<IRVar> res,
		std::shared_ptr<IRVar> x1,
		std::shared_ptr<IRVar> x2) : 
		_res(res), _x1(x1), _x2(x2) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_CONCAT;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _res, _x1, _x2;
};

class IRCodeAnd: public IRCode {
public:
	IRCodeAnd(std::shared_ptr<IRVar> res,
		std::shared_ptr<IRVar> x1,
		std::shared_ptr<IRVar> x2) : 
		_res(res), _x1(x1), _x2(x2) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_AND;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _res, _x1, _x2;
};

class IRCodeOr: public IRCode {
public:
	IRCodeOr(std::shared_ptr<IRVar> res,
		std::shared_ptr<IRVar> x1,
		std::shared_ptr<IRVar> x2) : 
		_res(res), _x1(x1), _x2(x2) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_OR;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _res, _x1, _x2;
};

class IRCodeNot: public IRCode {
public:
	IRCodeNot(std::shared_ptr<IRVar> res,
		std::shared_ptr<IRVar> x1) : 
		_res(res), _x1(x1) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_NOT;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _res, _x1;
};

class IRCodePhi: public IRCode {
public:
	IRCodePhi(std::shared_ptr<IRVar> res) : _res(res) {}
	void add(std::shared_ptr<IRVar>, std::string);

	std::string code() const;

	static const ircodetype_t myType = IRCODE_PHI;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _res;
	std::vector<std::pair<std::shared_ptr<IRVar>, std::string> > _entries;
};

class IRCodeCallPtr: public IRCode {
public:
	IRCodeCallPtr(std::shared_ptr<IRVar> res, std::shared_ptr<IRVar> ptr) : _res(res), _ptr(ptr) {}
	void add_arg(std::shared_ptr<IRVar>);

	std::string code() const;

	static const ircodetype_t myType = IRCODE_CALLPTR;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _res, _ptr;
	std::vector<std::shared_ptr<IRVar> > _args;
};

class IRCodeCall: public IRCode {
public:
	IRCodeCall(std::shared_ptr<IRVar> res, std::string fullname) : _res(res), _id(fullname) {}
	void add_arg(std::shared_ptr<IRVar>);

	std::string code() const;

	static const ircodetype_t myType = IRCODE_CALL;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _res;
	std::string _id;
	std::vector<std::shared_ptr<IRVar> > _args;
};

class IRCodeGetGlobalStr : public IRCode {
public:
	IRCodeGetGlobalStr(std::shared_ptr<IRVar> res,
		std::shared_ptr<IRGlobalString> gstr) : 
		_res(res), _gstr(gstr) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_GETGLOBALSTR;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _res;
	std::shared_ptr<IRGlobalString> _gstr;
};

class IRCodeCast: public IRCode {
public:
	IRCodeCast(std::shared_ptr<IRVar> where,
		std::shared_ptr<IRVar> what) : 
		_where(where), _what(what) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_CAST;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _where, _what;
};

class IRCodeGetProperty: public IRCode {
public:
	IRCodeGetProperty(
		std::shared_ptr<IRVar> where,
		std::shared_ptr<IRVar> from,
		std::string base_label,
		int order) : 
		_where(where), 
		_from(from), 
		_base_label(base_label), 
		_order(order) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_GETPROP;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _where, _from;
	std::string _base_label;
	int _order;
};

class IRCodeGetElem: public IRCode {
public:
	IRCodeGetElem(
		std::shared_ptr<IRVar> where,
		std::shared_ptr<IRVar> base,
		std::shared_ptr<IRVar> idx) : 
		_where(where), 
		_base(base), 
		_idx(idx) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_GETELEM;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _where, _base, _idx;
};

class IRCodeTrunc: public IRCode {
public:
	IRCodeTrunc(std::shared_ptr<IRVar> where,
		std::shared_ptr<IRVar> what) : 
		_where(where), _what(what) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_TRUNC;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _where, _what;
};

class IRCodeZext: public IRCode {
public:
	IRCodeZext(std::shared_ptr<IRVar> where,
		std::shared_ptr<IRVar> what) : 
		_where(where), _what(what) {}

	std::string code() const;

	static const ircodetype_t myType = IRCODE_ZEXT;
	ircodetype_t type() const { return myType; }
private:
	std::shared_ptr<IRVar> _where, _what;
};

}

#endif 
