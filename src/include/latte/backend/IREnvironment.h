/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 */

#ifndef __LATTE_IRENVIRONMENT__
#define __LATTE_IRENVIRONMENT__

#include <memory>
#include <map>
#include <string>
#include <vector>
#include <latte/common/Environment.h>
#include <latte/common/Type.h>

#include <latte/backend/LabelGenerator.h>
#include <latte/backend/IRGlobalString.h>
#include <latte/backend/IRDeclaration.h>
#include <latte/backend/IRVar.h>
#include <latte/backend/IRClassDef.h>

namespace Backend
{

class IREnvironment : public Common::Environment {
public:
	IREnvironment(const Common::Environment& env, 
		std::string scope,
		std::map<std::string, std::shared_ptr<IRGlobalString> >&,
		std::map<std::string, std::shared_ptr<IRDeclaration> >&,
		std::map<std::string, std::shared_ptr<IRClassDef> >&,
		LabelGenerator& );

	IREnvironment(const IREnvironment& env);
	
	std::shared_ptr<IRVar> add_function_argument(std::shared_ptr<Common::Type> type, std::string name);
	std::shared_ptr<IRVar> get_function_arg(std::string name);

	std::shared_ptr<IRVar> add_local_variable(std::shared_ptr<IRVar> var, std::string name);
	std::shared_ptr<IRVar> add_local_variable(std::shared_ptr<Common::Type> type, std::string name);
	std::shared_ptr<IRVar> get_local_variable(std::string name);

	std::string local_variable_label(std::string name); 

	const std::map<std::string, std::shared_ptr<IRVar> >& function_arguments() { return _functionArgs; }


	std::string scope() const { return _scope; }

	bool in_runtime_lib(std::string name);
	void add_runtime_function(std::string name, std::shared_ptr<IRDeclaration> decl);
	std::shared_ptr<IRVar> add_global_string(std::string name, std::string s);
	std::shared_ptr<IRVar> add_global_string(std::string s);

	std::shared_ptr<IRVar> next_var(std::shared_ptr<IRVar>);
	std::shared_ptr<IRVar> next_var(std::shared_ptr<IRType>);
	std::string next_block_label();
	std::string next_temp_label();

	std::shared_ptr<IRClassDef> get_irclass(std::string name) { return _classes.at(name); }

private:
	std::map<std::string, std::shared_ptr<IRVar> > _instanceVariables;
	std::map<std::string, std::shared_ptr<IRVar> > _functionArgs;
	std::map<std::string, std::shared_ptr<IRVar> > _variables;

	std::map<std::string, int > _nestLvls;
	int _nestLvl;

	static std::map<std::string, int > _counters;

	std::string _scope;
	std::map<std::string, std::shared_ptr<IRGlobalString> >& _globalStrings;
	std::map<std::string, std::shared_ptr<IRDeclaration> >& _globalDeclarations;
	std::map<std::string, std::shared_ptr<IRClassDef> >& _classes;
	LabelGenerator& _lgen;
};

}

#endif // __LATTE_IRENVIRONMENT__

