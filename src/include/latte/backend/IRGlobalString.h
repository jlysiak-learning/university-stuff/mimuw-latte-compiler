#ifndef __LATTE_GLOBALSTRING__
#define __LATTE_GLOBALSTRING__

#include <latte/backend/IRVar.h>


namespace Backend {

class IRGlobalString : public IRVar {
public:
	IRGlobalString(std::string label, std::string content) : 
		IRVar(std::make_shared<IRCharArr>(content.length()+1), label), 
		_content(content) {}

	std::string code() const;
	std::string lstr() const; 
	int len() const;

private:
	std::string _content;
};

}

#endif
