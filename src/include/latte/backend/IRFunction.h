#ifndef __LATTE_IRFUNCTION__
#define __LATTE_IRFUNCTION__ 

#include <memory>
#include <vector>
#include <string>
#include <set>

#include <latte/error.h>
#include <latte/latte/Absyn.h>

#include <latte/common/ClassDefinition.h>
#include <latte/common/FunctionDefinition.h>

#include <latte/backend/IRBlock.h>
#include <latte/backend/IRVar.h>
#include <latte/backend/IRType.h>
#include <latte/backend/IRCode.h>

namespace Backend {

using namespace Latte;

class IRFunction {
public:
	IRFunction(
		std::string scopeName, 
		std::string name, 
		std::shared_ptr<IRType>);


	void add_arg(std::shared_ptr<IRVar>);
	void add_block(std::shared_ptr<IRBlock>);
	std::map<std::string, std::shared_ptr<IRBlock>>& blocks() { return _blocks; }

	void register_function_call(std::string fullname);
	void register_global_usage(std::string fullname);

	const std::set<std::string>& get_used_globals() { return _globalsUsed; }
	const std::set<std::string>& get_called_functions() { return _functionsCalled; }


	std::string full_name() const { return _scopeName != "" ? _scopeName + "." + _name : _name; }

	friend std::ostream& operator<< (std::ostream& output, IRFunction const& fun);

	std::string name() const {return _name;}
	std::string typestr() const;
	std::string varstr() const;

private:
	
	std::string _name;
	std::string _scopeName;

	std::shared_ptr<IRType> _type;
	std::vector<std::shared_ptr<IRVar> > _args;

	std::vector<std::string> _labels;

	std::map<std::string, std::shared_ptr<IRBlock>> _blocks;

	std::set<std::string> _globalsUsed;
	std::set<std::string> _functionsCalled;
};

}

#endif 

