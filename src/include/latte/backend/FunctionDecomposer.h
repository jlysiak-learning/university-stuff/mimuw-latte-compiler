/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 */

#ifndef __LATTE_FUNCTIONDECOMPOSER__
#define __LATTE_FUNCTIONDECOMPOSER__

#include <memory>
#include <latte/common/LocalEnvironment.h>
#include <latte/common/Logger.h>
#include <latte/common/Type.h>
#include <latte/latte/Absyn.h>

#include <latte/backend/LabelGenerator.h>
#include <latte/backend/IRCode.h>
#include <latte/backend/IRFunction.h>
#include <latte/backend/IRBlock.h>
#include <latte/backend/IREnvironment.h>
#include <latte/backend/IRDeclaration.h>

namespace Backend
{

using namespace Latte;

class FunctionDecomposer : public Visitor
{
private:
	std::shared_ptr<IREnvironment> _env;
	Common::Logger& _logger;

	std::shared_ptr<Common::Type> _inType;
	std::shared_ptr<IRFunction> _irfun;
	std::shared_ptr<IRBlock> _activeBlock;
	std::shared_ptr<IRClassDef> _thisClass;
	std::shared_ptr<IRVar> _for_iterator;
	int _in_loop;
	std::vector<std::shared_ptr<IRCode>> _allocs;
	
	std::shared_ptr<IRVar> _load_if_needed(Expr *expr);
  	std::shared_ptr<IRVar> _sdd_global_string(std::string, std::string );
  	std::shared_ptr<IRVar> _generate_default_initializer(std::shared_ptr<IRVar> var);
	bool _in_runtime_lib(std::string name);
  	void _add_runtime_function(std::string, std::shared_ptr<IRDeclaration>);

	std::shared_ptr<IRVar> _malloc(int n);
	std::shared_ptr<IRVar> _malloc(std::shared_ptr<IRVar> var_n, int el_sz);
	std::shared_ptr<IRVar> _alloca(std::shared_ptr<IRType> what_type);
	std::shared_ptr<IRVar> _cast(std::shared_ptr<IRVar> what, std::shared_ptr<IRType> to_type);
	void _store(std::shared_ptr<IRVar> what, std::shared_ptr<IRVar> where);
	std::shared_ptr<IRVar> _get_class_property(
                                std::shared_ptr<IRVar> objptr,
                                std::string base_label,
                                std::shared_ptr<IRType> prop_type,
                                int order);
	std::shared_ptr<IRVar> _load(std::shared_ptr<IRVar> var_ptr2);
	std::shared_ptr<IRVar> _getelementptr(
			std::shared_ptr<IRVar> var_base_ptr,
			std::shared_ptr<IRVar> var_idx);
	std::shared_ptr<IRVar> _zext(std::shared_ptr<IRVar> var);
	std::shared_ptr<IRVar> _trunc(std::shared_ptr<IRVar> var);


	void _decompose(Blk*);
public:
	FunctionDecomposer(std::shared_ptr<IREnvironment>, Common::Logger& ); 

	void generate_code(std::shared_ptr<Common::FunctionDefinition>, std::shared_ptr<IRFunction>);

	//==== Visitor 
	void visitIdentP(IdentP *p);
	void visitProgram(Program *p);
	void visitTopDef(TopDef *p);
	void visitFunDef(FunDef *p);
	void visitArg(Arg *p);
	void visitClassDef(ClassDef *p);
	void visitClassBlock(ClassBlock *p);
	void visitClassBlockDef(ClassBlockDef *p);
	void visitClsFldDefItem(ClsFldDefItem *p);
	void visitBlock(Block *p);
	void visitStmt(Stmt *p);
	void visitDeclItem(DeclItem *p);
	void visitClassType(ClassType *p);
	void visitBasicType(BasicType *p);
	void visitNonVoidType(NonVoidType *p);
	void visitType(Type *p);
	void visitCastOn(CastOn *p);
	void visitExpr(Expr *p);
	void visitAddOp(AddOp *p);
	void visitMulOp(MulOp *p);
	void visitRelOp(RelOp *p);
	void visitIdentPos(IdentPos *p);
	void visitProg(Prog *p);
	void visitClsDefTop(ClsDefTop *p);
	void visitFnDefTop(FnDefTop *p);
	void visitFnDef(FnDef *p);
	void visitAr(Ar *p);
	void visitClsDef(ClsDef *p);
	void visitClsExtDef(ClsExtDef *p);
	void visitClsBlk(ClsBlk *p);
	void visitClsMthDef(ClsMthDef *p);
	void visitClsFldDef(ClsFldDef *p);
	void visitClsFldDefIt(ClsFldDefIt *p);
	void visitBlk(Blk *p);
	void visitDecl(Decl *p);
	void visitNoInit(NoInit *p);
	void visitInit(Init *p);
	void visitAssign(Assign *p);
	void visitIncr(Incr *p);
	void visitDecr(Decr *p);
	void visitRet(Ret *p);
	void visitVRet(VRet *p);
	void visitWhile(While *p);
	void visitFor(For *p);
	void visitIf(If *p);
	void visitIfElse(IfElse *p);
	void visitBStmt(BStmt *p);
	void visitEmpty(Empty *p);
	void visitExp(Exp *p);
	void visitTClass(TClass *p);
	void visitTInt(TInt *p);
	void visitTStr(TStr *p);
	void visitTBool(TBool *p);
	void visitTNVC(TNVC *p);
	void visitTNVB(TNVB *p);
	void visitTArray(TArray *p);
	void visitTSingle(TSingle *p);
	void visitTVoid(TVoid *p);
	void visitCastOnClass(CastOnClass *p);
	void visitCastOnArr(CastOnArr *p);
	void visitCastOnBasic(CastOnBasic *p);
	void visitENewArr(ENewArr *p);
	void visitENewCls(ENewCls *p);
	void visitELitInt(ELitInt *p);
	void visitELitTrue(ELitTrue *p);
	void visitELitFalse(ELitFalse *p);
	void visitEString(EString *p);
	void visitEVar(EVar *p);
	void visitENull(ENull *p);
	void visitEMemb(EMemb *p);
	void visitEFunCall(EFunCall *p);
	void visitEMembCall(EMembCall *p);
	void visitEAt(EAt *p);
	void visitECast(ECast *p);
	void visitNeg(Neg *p);
	void visitNot(Not *p);
	void visitEMul(EMul *p);
	void visitEAdd(EAdd *p);
	void visitERel(ERel *p);
	void visitEAnd(EAnd *p);
	void visitEOr(EOr *p);
	void visitPlus(Plus *p);
	void visitMinus(Minus *p);
	void visitTimes(Times *p);
	void visitDiv(Div *p);
	void visitMod(Mod *p);
	void visitLTH(LTH *p);
	void visitLE(LE *p);
	void visitGTH(GTH *p);
	void visitGE(GE *p);
	void visitEQU(EQU *p);
	void visitNE(NE *p);
	void visitListTopDef(ListTopDef *p);
	void visitListArg(ListArg *p);
	void visitListClsFldDefItem(ListClsFldDefItem *p);
	void visitListClassBlockDef(ListClassBlockDef *p);
	void visitListStmt(ListStmt *p);
	void visitListDeclItem(ListDeclItem *p);
	void visitListExpr(ListExpr *p);

	void visitInteger(Integer x);
	void visitChar(Char x);
	void visitDouble(Double x);
	void visitString(String x);
	void visitIdent(Ident x);

};

}

#endif
