/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Latte types
 */

#ifndef __LATTE_TYPES__
#define __LATTE_TYPES__

#include <memory>
#include <string>
#include <latte/latte/Absyn.h>

namespace Common {

typedef enum {
	TYPE_NULL,
	TYPE_VOID,
	TYPE_BOOL,
	TYPE_INT,
	TYPE_STRING,
	TYPE_ARRAY,
	TYPE_CLASS
} type_t;

class Type {
public:
	virtual type_t type() const = 0;
	virtual std::string str() const = 0;

	template <class T> bool is() const { return type()  == T::myType; }
	 
	static std::shared_ptr<Type> from_type_AST(Latte::Type * typeAstPtr);
	static std::shared_ptr<Type> from_cast_AST(Latte::CastOn * castAstPtr);

	bool operator== (const Type& other);
	bool operator!= (const Type& other);
	bool is_cast_possible(std::shared_ptr<Type> onType);
	bool is_relop_compatible(relop_t op);
};

class Null : public Type {
public:
	static const type_t myType = TYPE_NULL;

	type_t type() const { return TYPE_NULL; }
	std::string str() const { return std::string("NULL"); }
};

class Void : public Type {
public:
	static const type_t myType = TYPE_VOID;

	type_t type() const { return TYPE_VOID; }
	std::string str() const { return std::string("VOID"); }
};

class Bool : public Type {
public:
	static const type_t myType = TYPE_BOOL;

	type_t type() const { return TYPE_BOOL; }
	std::string str() const { return std::string("BOOLEAN"); }
};

class Int : public Type {
public:
	static const type_t myType = TYPE_INT;

	type_t type() const { return TYPE_INT; }
	std::string str() const { return std::string("INT"); }
};


class String : public Type {
public:
	static const type_t myType = TYPE_STRING;

	type_t type() const { return TYPE_STRING; }
	std::string str() const { return std::string("STRING"); }
};

class Array : public Type {
public:
	Array() : _elem_type(nullptr) {}
	Array(std::shared_ptr<Common::Type> T) : _elem_type(T) {}

	static const type_t myType = TYPE_ARRAY;

	type_t type() const { return TYPE_ARRAY; }
	std::string str() const { return std::string("ARRAY"); }

	std::shared_ptr<Type> elem_type() { return _elem_type; }
	void set_elem_type(std::shared_ptr<Type> elem_type) {_elem_type = elem_type;}

private:
	std::shared_ptr<Type> _elem_type;
};

class Class : public Type {
public:
	static const type_t myType = TYPE_CLASS;

	Class(std::string name) : _name(name) {}

	type_t type() const { return TYPE_CLASS; }
	std::string name() const {return _name;}
	std::string str() const { return std::string("CLASS(" + _name + ")"); }
private:
	std::string _name;
};

}
#endif  // __LATTE_TYPES__

