/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Compiler environment.
 */

#ifndef __LATTE_ENVIRONMENT__
#define __LATTE_ENVIRONMENT__

#include <memory>
#include <map>
#include <string>
#include <vector>

#include <latte/common/FunctionDefinition.h>
#include <latte/common/ClassDefinition.h>

namespace Common
{

/**
 * Main environment.
 */
class Environment {
public:
	Environment();
	Environment(const Environment& env);

	bool is_class_defined(std::string name);

	std::shared_ptr<Common::ClassDefinition> get_class_def(std::string name);
	std::shared_ptr<Common::FunctionDefinition> get_fun_def(std::string name);

	void add_class_def(std::shared_ptr<Common::ClassDefinition> clsPtr);
	void add_fun_def(std::shared_ptr<Common::FunctionDefinition> funPtr);

	bool is_reserved_ident(std::string name) const;
	bool entrypoint_defined() const;

	const std::map<std::string, std::shared_ptr<ClassDefinition> >& classes() const { return _classDefs; }
	const std::map<std::string, std::shared_ptr<FunctionDefinition> >& functions() const { return _functionDefs; }

	bool is_child_of(std::shared_ptr<Type> child, std::shared_ptr<Type> base);

protected:
	std::map<std::string, std::shared_ptr<ClassDefinition> > _classDefs;
	std::map<std::string, std::shared_ptr<FunctionDefinition> > _functionDefs;
	std::vector<std::string> _reserved_idents;
};

}

#endif // __LATTE_ENVIRONMENT__

