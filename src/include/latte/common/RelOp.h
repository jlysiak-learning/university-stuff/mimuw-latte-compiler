
#ifndef __LATTE_RELOP__
#define __LATTE_RELOP__

#include <string>

namespace Common {

typedef enum {
	RELOP_EQU,
	RELOP_NE,
	RELOP_GE,
	RELOP_GTH,
	RELOP_LE,
	RELOP_LTH
} relop_t;

std::string relop_to_string(relop_t op);

}


#endif // __LATTE_RELOP__
