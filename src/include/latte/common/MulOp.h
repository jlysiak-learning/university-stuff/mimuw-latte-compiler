#ifndef __LATTE_MULOP__
#define __LATTE_MULOP__

namespace Common {

typedef enum {
	MULOP_MUL,
	MULOP_DIV,
	MULOP_MOD
} mulop_t;

}

#endif
