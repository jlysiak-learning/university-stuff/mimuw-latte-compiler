/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Logs handler.
 */

#ifndef __LATTE_LOGGER__
#define __LATTE_LOGGER__

#include <ostream>
#include <vector>
#include <chrono>
#include <string>

#include <latte/error.h>

namespace Common {


typedef enum {
	LOG_LVL_ERR = 0,
	LOG_LVL_WARN,
	LOG_LVL_INFO,
	LOG_LVL_DEBUG
} log_lvl_t;


class LogEntry {
public:
	LogEntry(log_lvl_t lvl, std::string msg);
	LogEntry(log_lvl_t lvl, std::string msg, int line);

	friend std::ostream& operator<< (std::ostream &output, LogEntry const& entry);

	log_lvl_t level() const;

private:
	int _line;
	log_lvl_t _level;
	std::string _msg;
	std::chrono::system_clock::time_point _time;

	std::string _format_message() const;
};


class Logger {
public:
	Logger(std::ostream &output);
	Logger(std::ostream &output, log_lvl_t lvl);

	void set_level(log_lvl_t lvl);
	bool has_errors();

	void error(std::string msg);
	void error_at(int line, std::string msg);
	void warn(std::string msg);
	void warn_at(int line, std::string msg);
	void info(std::string msg);
	void info_at(int line, std::string msg);
	void debug(std::string msg);
	void debug_at(int line, std::string msg);

private:

	void _add_entry(log_lvl_t lvl, std::string msg);
	void _add_entry(log_lvl_t lvl, std::string msg, int line);

	std::ostream &_output;
	std::vector<LogEntry> _logs;
	log_lvl_t _log_lvl;
};

}

#endif // __LATTE_LOGGER__
