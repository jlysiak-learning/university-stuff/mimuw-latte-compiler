/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Class definition
 */

#ifndef __LATTE_CLASSDEF__
#define __LATTE_CLASSDEF__

#include <map>
#include <string>

#include <latte/common/FunctionDefinition.h>
#include <latte/common/Type.h>

namespace Common
{

class ClassDefinition { 

public:
	ClassDefinition(int line, std::string name);
	ClassDefinition(int line, std::string name, std::string parent_name);

	void set_parent(std::shared_ptr<ClassDefinition> parent);
	bool valid();

	int get_line() const { return _line; }
	std::string get_name() const { return _name; }

	std::shared_ptr<ClassDefinition> get_parent() const { return _parent; }
	std::string get_parent_name() const { return _parentName; }

	bool add_field(std::shared_ptr<Type>, std::string name);
	bool add_method(std::shared_ptr<FunctionDefinition>);

	bool has_field(std::string name);
	bool has_method(std::string name);
	bool has_ident(std::string name);

	bool has_field_visible(std::string name);
	bool has_method_visible(std::string name);
	bool has_ident_visible(std::string name);

	const std::map<std::string, std::shared_ptr<Type> >& get_fields() const { return _fields; }
	const std::map<std::string, std::shared_ptr<FunctionDefinition> >& get_methods() const { return _methods; }
	std::shared_ptr<FunctionDefinition> get_method(std::string name); 
	std::shared_ptr<Type> get_field_type(std::string name); 

	std::shared_ptr<Type>&  type() { return _type; }

private:

	int _line;

	std::string _name;
	std::string _parentName;
	std::shared_ptr<ClassDefinition> _parent;
	std::map<std::string, std::shared_ptr<FunctionDefinition> > _methods;
	std::map<std::string, std::shared_ptr<Type> > _fields;	
	std::shared_ptr<Type> _type;

};

}

#endif // __LATTE_CLASSDEF__


