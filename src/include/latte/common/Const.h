/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Const class
 */

#ifndef __LATTE_CONST__
#define __LATTE_CONST__

#include <string>
#include <memory>
#include <latte/common/Type.h>
#include <latte/common/RelOp.h>
#include <latte/common/AddOp.h>
#include <latte/common/MulOp.h>

namespace Common {


class Const {
public:

	Const() : _type(TYPE_NULL), _b(0), _i(0), _s("NULL") {}
	Const(bool b) : _type(TYPE_BOOL), _b(b), _i((b ? 1 : 0)), _s(b ? "T" : "F") {}
	Const(int i) : _type(TYPE_INT), _b(i!=0), _i(i), _s(std::to_string(i)) {}
	Const(std::string s) : _type(TYPE_STRING), _b(false), _i(0), _s(s) {}

	static std::shared_ptr<Const> mul(const std::shared_ptr<Const>&, const std::shared_ptr<Const>&, mulop_t);
	static std::shared_ptr<Const> add(const std::shared_ptr<Const>&, const  std::shared_ptr<Const>&, addop_t);
	static std::shared_ptr<Const> rel(const std::shared_ptr<Const>&, const std::shared_ptr<Const>&, relop_t);
	std::shared_ptr<Const> cast_on(std::shared_ptr<Type>);

	friend std::shared_ptr<Const> operator&& (const std::shared_ptr<Const>& c1, const std::shared_ptr<Const>& c2);
	friend std::shared_ptr<Const> operator|| (const std::shared_ptr<Const>& c1, const std::shared_ptr<Const>& c2);
	friend std::shared_ptr<Const> operator! (const std::shared_ptr<Const>& c);
	friend std::shared_ptr<Const> operator- (const std::shared_ptr<Const>& c);

	bool b() const { return _b; }
	int i() const { return _i; }
	std::string s() const { return _s; }

	std::string to_string() const;
private:
	type_t _type;
	bool _b;
	int _i;
	std::string _s;
};

}

#endif
