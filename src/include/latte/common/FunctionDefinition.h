/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Class definition
 */

#ifndef __LATTE_FUNDEF__
#define __LATTE_FUNDEF__


#include <vector>
#include <string>
#include <memory>
#include <utility>

#include <latte/common/Type.h>
#include <latte/latte/Absyn.h>


namespace Common
{

class FunctionDefinition { 

public:
	FunctionDefinition(std::shared_ptr<Type> type, std::string name);
	FunctionDefinition (int line, std::shared_ptr<Type> type, std::string name);
	void add_arg(std::shared_ptr<Type> type, std::string name);
	void add_body(Latte::Blk *ast_tree);

	int arg_num() const { return (int) _args.size(); }
	int get_line() const { return _line; }
	std::string get_name() const { return _name; }
	Latte::Blk * get_body() const { return _fun_body; }
	std::shared_ptr<Type> type() const { return _type; }
	const std::vector<std::pair<std::shared_ptr<Type>, std::string> >& get_args() const { return _args; }
	std::string str() const;

	bool is_arg_name_defined(std::string name);


	bool isPredefined() const { return _predefined; }

	bool operator== (const FunctionDefinition& );
	bool operator!= (const FunctionDefinition& );

private:
	bool _predefined;
	int _line;

	std::shared_ptr<Type> _type;
	std::string _name;
	std::vector<std::pair<std::shared_ptr<Type>, std::string> > _args;

	Latte::Blk *_fun_body;
};

}

#endif 


