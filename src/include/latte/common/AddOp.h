
#ifndef __LATTE_ADDOP__
#define __LATTE_ADDOP__

#include <string>

namespace Common {

typedef enum {
	ADDOP_ADD,
	ADDOP_SUB
} addop_t;

std::string addop_to_string(addop_t op);

}


#endif // __LATTE_ADDOP__
