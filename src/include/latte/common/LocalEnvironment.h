/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Local environment.
 */

#ifndef __LATTE_LOCALENVIRONMENT__
#define __LATTE_LOCALENVIRONMENT__

#include <memory>
#include <map>
#include <string>
#include <vector>
#include <latte/common/Environment.h>

namespace Common
{

class LocalEnvironment : public Environment {
public:
	LocalEnvironment(const Environment& env);
	LocalEnvironment(const LocalEnvironment& env);

	bool add_variable(std::shared_ptr<Type>& type, std::string name);

	std::shared_ptr<Type> get_variable_type(std::string name);

private:
	std::map<std::string, std::shared_ptr<Type> > _variables;
	std::map<std::string, int > _nestLvls;
	int _nestLvl;
};

}

#endif // __LATTE_ENVIRONMENT__

