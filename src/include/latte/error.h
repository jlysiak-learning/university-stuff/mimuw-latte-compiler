/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Standard error codes
 */

#ifndef __LATTE_ERROR__
#define __LATTE_ERROR__

typedef enum {
	SUCCESS = 0,
	FAIL = 1
} err_t;

#endif
