/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * TypeChecker main module.
 */

#ifndef __LATTE_TYPECHECKER__
#define __LATTE_TYPECHECKER__ 

#include <memory>
#include <vector>
#include <string>

#include <latte/latte/Absyn.h>
#include <latte/common/ClassDefinition.h>
#include <latte/common/FunctionDefinition.h>
#include <latte/common/Environment.h>
#include <latte/common/LocalEnvironment.h>
#include <latte/common/Logger.h>
#include <latte/error.h>

namespace Frontend {

using namespace Latte;

/**
 */
class TypeChecker {
public:
	TypeChecker(Common::Environment &env, Common::Logger &logger);

	error_t check();

private:
	Common::Environment& _env;
	Common::Logger& _logger;

	error_t _check_function(const Common::LocalEnvironment& env, std::shared_ptr<Common::FunctionDefinition> fun);
};

}

#endif // __LATTE_TYPECHECKER__

