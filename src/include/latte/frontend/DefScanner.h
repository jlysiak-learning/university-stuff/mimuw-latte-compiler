/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Top definitions scanner.
 */

#ifndef __LATTE_DEFSCANNER__
#define __LATTE_DEFSCANNER__

#include <memory>
#include <vector>
#include <string>

#include <latte/latte/Absyn.h>
#include <latte/common/ClassDefinition.h>
#include <latte/common/FunctionDefinition.h>
#include <latte/common/Environment.h>
#include <latte/common/Logger.h>
#include <latte/error.h>

namespace Frontend {

using namespace Latte;

/**
 * Definitions scanner.
 * Visit AST and get information about functions and classes.
 */
class DefScanner : public Visitor {
private:
	Common::Environment& _env;
	Common::Logger& _logger;


	// Temporary store visiting object
	std::shared_ptr<Common::ClassDefinition> _inClass;
	std::shared_ptr<Common::Type> _inType;
	std::shared_ptr<Common::FunctionDefinition> _inFun;

	void _check_ext_classes();

public:
	DefScanner(Common::Environment &env, Common::Logger &logger);

	err_t scan_defs(Visitable *ast_root);

	/*============ VISITOR CLASS STUFF */
	void visitIdentP(IdentP *p);
	void visitProgram(Program *p);
	void visitTopDef(TopDef *p);
	void visitFunDef(FunDef *p);
	void visitArg(Arg *p);
	void visitClassDef(ClassDef *p);
	void visitClassBlock(ClassBlock *p);
	void visitClassBlockDef(ClassBlockDef *p);
	void visitClsFldDefItem(ClsFldDefItem *p);
	void visitBlock(Block *p);
	void visitStmt(Stmt *p);
	void visitDeclItem(DeclItem *p);
	void visitClassType(ClassType *p);
	void visitBasicType(BasicType *p);
	void visitNonVoidType(NonVoidType *p);
	void visitType(Type *p);
	void visitCastOn(CastOn *p);
	void visitExpr(Expr *p);
	void visitAddOp(AddOp *p);
	void visitMulOp(MulOp *p);
	void visitRelOp(RelOp *p);
	void visitIdentPos(IdentPos *p);
	void visitProg(Prog *p);
	void visitClsDefTop(ClsDefTop *p);
	void visitFnDefTop(FnDefTop *p);
	void visitFnDef(FnDef *p);
	void visitAr(Ar *p);
	void visitClsDef(ClsDef *p);
	void visitClsExtDef(ClsExtDef *p);
	void visitClsBlk(ClsBlk *p);
	void visitClsMthDef(ClsMthDef *p);
	void visitClsFldDef(ClsFldDef *p);
	void visitClsFldDefIt(ClsFldDefIt *p);
	void visitBlk(Blk *p);
	void visitDecl(Decl *p);
	void visitNoInit(NoInit *p);
	void visitInit(Init *p);
	void visitAssign(Assign *p);
	void visitIncr(Incr *p);
	void visitDecr(Decr *p);
	void visitRet(Ret *p);
	void visitVRet(VRet *p);
	void visitWhile(While *p);
	void visitFor(For *p);
	void visitIf(If *p);
	void visitIfElse(IfElse *p);
	void visitBStmt(BStmt *p);
	void visitEmpty(Empty *p);
	void visitExp(Exp *p);
	void visitTClass(TClass *p);
	void visitTInt(TInt *p);
	void visitTStr(TStr *p);
	void visitTBool(TBool *p);
	void visitTNVC(TNVC *p);
	void visitTNVB(TNVB *p);
	void visitTArray(TArray *p);
	void visitTSingle(TSingle *p);
	void visitTVoid(TVoid *p);
	void visitCastOnClass(CastOnClass *p);
	void visitCastOnArr(CastOnArr *p);
	void visitCastOnBasic(CastOnBasic *p);
	void visitENewArr(ENewArr *p);
	void visitENewCls(ENewCls *p);
	void visitELitInt(ELitInt *p);
	void visitELitTrue(ELitTrue *p);
	void visitELitFalse(ELitFalse *p);
	void visitEString(EString *p);
	void visitEVar(EVar *p);
	void visitENull(ENull *p);
	void visitEMemb(EMemb *p);
	void visitEFunCall(EFunCall *p);
	void visitEMembCall(EMembCall *p);
	void visitEAt(EAt *p);
	void visitECast(ECast *p);
	void visitNeg(Neg *p);
	void visitNot(Not *p);
	void visitEMul(EMul *p);
	void visitEAdd(EAdd *p);
	void visitERel(ERel *p);
	void visitEAnd(EAnd *p);
	void visitEOr(EOr *p);
	void visitPlus(Plus *p);
	void visitMinus(Minus *p);
	void visitTimes(Times *p);
	void visitDiv(Div *p);
	void visitMod(Mod *p);
	void visitLTH(LTH *p);
	void visitLE(LE *p);
	void visitGTH(GTH *p);
	void visitGE(GE *p);
	void visitEQU(EQU *p);
	void visitNE(NE *p);
	void visitListTopDef(ListTopDef *p);
	void visitListArg(ListArg *p);
	void visitListClsFldDefItem(ListClsFldDefItem *p);
	void visitListClassBlockDef(ListClassBlockDef *p);
	void visitListStmt(ListStmt *p);
	void visitListDeclItem(ListDeclItem *p);
	void visitListExpr(ListExpr *p);

	void visitInteger(Integer x);
	void visitChar(Char x);
	void visitDouble(Double x);
	void visitString(String x);
	void visitIdent(Ident x);

};

}

#endif // __LATTE_DEFSCANNER__

