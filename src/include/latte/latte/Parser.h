#ifndef LATTE_PARSER_HEADER_FILE
#define LATTE_PARSER_HEADER_FILE

#include<vector>
#include<string>

namespace Latte
{
class IdentP;
class Program;
class ListTopDef;
class TopDef;
class FunDef;
class Arg;
class ListArg;
class ClassDef;
class ClassBlock;
class ClassBlockDef;
class ClsFldDefItem;
class ListClsFldDefItem;
class ListClassBlockDef;
class Block;
class ListStmt;
class Stmt;
class DeclItem;
class ListDeclItem;
class ClassType;
class BasicType;
class NonVoidType;
class Type;
class CastOn;
class Expr;
class ListExpr;
class AddOp;
class MulOp;
class RelOp;

typedef union
{
  int int_;
  char char_;
  double double_;
  char* string_;
  IdentP* identp_;
  Program* program_;
  ListTopDef* listtopdef_;
  TopDef* topdef_;
  FunDef* fundef_;
  Arg* arg_;
  ListArg* listarg_;
  ClassDef* classdef_;
  ClassBlock* classblock_;
  ClassBlockDef* classblockdef_;
  ClsFldDefItem* clsflddefitem_;
  ListClsFldDefItem* listclsflddefitem_;
  ListClassBlockDef* listclassblockdef_;
  Block* block_;
  ListStmt* liststmt_;
  Stmt* stmt_;
  DeclItem* declitem_;
  ListDeclItem* listdeclitem_;
  ClassType* classtype_;
  BasicType* basictype_;
  NonVoidType* nonvoidtype_;
  Type* type_;
  CastOn* caston_;
  Expr* expr_;
  ListExpr* listexpr_;
  AddOp* addop_;
  MulOp* mulop_;
  RelOp* relop_;
} YYSTYPE;

Program* pProgram(FILE *inp);
Program* pProgram(const char *str);

}

#define LATTE__ERROR_ 258
#define LATTE__SYMB_0 259
#define LATTE__SYMB_1 260
#define LATTE__SYMB_2 261
#define LATTE__SYMB_3 262
#define LATTE__SYMB_4 263
#define LATTE__SYMB_5 264
#define LATTE__SYMB_6 265
#define LATTE__SYMB_7 266
#define LATTE__SYMB_8 267
#define LATTE__SYMB_9 268
#define LATTE__SYMB_10 269
#define LATTE__SYMB_11 270
#define LATTE__SYMB_12 271
#define LATTE__SYMB_13 272
#define LATTE__SYMB_14 273
#define LATTE__SYMB_15 274
#define LATTE__SYMB_16 275
#define LATTE__SYMB_17 276
#define LATTE__SYMB_18 277
#define LATTE__SYMB_19 278
#define LATTE__SYMB_20 279
#define LATTE__SYMB_21 280
#define LATTE__SYMB_22 281
#define LATTE__SYMB_23 282
#define LATTE__SYMB_24 283
#define LATTE__SYMB_25 284
#define LATTE__SYMB_26 285
#define LATTE__SYMB_27 286
#define LATTE__SYMB_28 287
#define LATTE__SYMB_29 288
#define LATTE__SYMB_30 289
#define LATTE__SYMB_31 290
#define LATTE__SYMB_32 291
#define LATTE__SYMB_33 292
#define LATTE__SYMB_34 293
#define LATTE__SYMB_35 294
#define LATTE__SYMB_36 295
#define LATTE__SYMB_37 296
#define LATTE__SYMB_38 297
#define LATTE__SYMB_39 298
#define LATTE__SYMB_40 299
#define LATTE__SYMB_41 300
#define LATTE__SYMB_42 301
#define LATTE__STRING_ 302
#define LATTE__INTEGER_ 303
#define LATTE__IDENT_ 304

extern Latte::YYSTYPE Latteyylval;

#endif
