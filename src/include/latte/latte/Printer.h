#ifndef LATTE_PRINTER_HEADER
#define LATTE_PRINTER_HEADER

#include <latte/latte/Absyn.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

namespace Latte
{
/* Certain applications may improve performance by changing the buffer size */
#define LATTE_BUFFER_INITIAL 2000
/* You may wish to change _L_PAREN or _R_PAREN */
#define LATTE__L_PAREN '('
#define LATTE__R_PAREN ')'

class PrintAbsyn : public Visitor
{
 protected:
  int _n_, _i_;
  /* The following are simple heuristics for rendering terminals */
  /* You may wish to change them */
  void render(Char c);
  void render(String s);
  void render(const char *s);
  void indent(void);
  void backup(void);
 public:
  PrintAbsyn(void);
  ~PrintAbsyn(void);
  char *print(Visitable *v);

  void visitIdentP(IdentP *p); /* abstract class */
  void visitIdentPos(IdentPos *p);
  void visitProgram(Program *p); /* abstract class */
  void visitProg(Prog *p);
  void visitListTopDef(ListTopDef *p);
  void visitTopDef(TopDef *p); /* abstract class */
  void visitClsDefTop(ClsDefTop *p);
  void visitFnDefTop(FnDefTop *p);
  void visitFunDef(FunDef *p); /* abstract class */
  void visitFnDef(FnDef *p);
  void visitArg(Arg *p); /* abstract class */
  void visitAr(Ar *p);
  void visitListArg(ListArg *p);
  void visitClassDef(ClassDef *p); /* abstract class */
  void visitClsDef(ClsDef *p);
  void visitClsExtDef(ClsExtDef *p);
  void visitClassBlock(ClassBlock *p); /* abstract class */
  void visitClsBlk(ClsBlk *p);
  void visitClassBlockDef(ClassBlockDef *p); /* abstract class */
  void visitClsMthDef(ClsMthDef *p);
  void visitClsFldDef(ClsFldDef *p);
  void visitClsFldDefItem(ClsFldDefItem *p); /* abstract class */
  void visitClsFldDefIt(ClsFldDefIt *p);
  void visitListClsFldDefItem(ListClsFldDefItem *p);
  void visitListClassBlockDef(ListClassBlockDef *p);
  void visitBlock(Block *p); /* abstract class */
  void visitBlk(Blk *p);
  void visitListStmt(ListStmt *p);
  void visitStmt(Stmt *p); /* abstract class */
  void visitDecl(Decl *p);
  void visitAssign(Assign *p);
  void visitIncr(Incr *p);
  void visitDecr(Decr *p);
  void visitRet(Ret *p);
  void visitVRet(VRet *p);
  void visitWhile(While *p);
  void visitFor(For *p);
  void visitIf(If *p);
  void visitIfElse(IfElse *p);
  void visitBStmt(BStmt *p);
  void visitEmpty(Empty *p);
  void visitExp(Exp *p);
  void visitDeclItem(DeclItem *p); /* abstract class */
  void visitNoInit(NoInit *p);
  void visitInit(Init *p);
  void visitListDeclItem(ListDeclItem *p);
  void visitClassType(ClassType *p); /* abstract class */
  void visitTClass(TClass *p);
  void visitBasicType(BasicType *p); /* abstract class */
  void visitTInt(TInt *p);
  void visitTStr(TStr *p);
  void visitTBool(TBool *p);
  void visitNonVoidType(NonVoidType *p); /* abstract class */
  void visitTNVC(TNVC *p);
  void visitTNVB(TNVB *p);
  void visitType(Type *p); /* abstract class */
  void visitTArray(TArray *p);
  void visitTSingle(TSingle *p);
  void visitTVoid(TVoid *p);
  void visitCastOn(CastOn *p); /* abstract class */
  void visitCastOnClass(CastOnClass *p);
  void visitCastOnArr(CastOnArr *p);
  void visitCastOnBasic(CastOnBasic *p);
  void visitExpr(Expr *p); /* abstract class */
  void visitENewArr(ENewArr *p);
  void visitENewCls(ENewCls *p);
  void visitELitInt(ELitInt *p);
  void visitELitTrue(ELitTrue *p);
  void visitELitFalse(ELitFalse *p);
  void visitEString(EString *p);
  void visitEVar(EVar *p);
  void visitENull(ENull *p);
  void visitEMemb(EMemb *p);
  void visitEFunCall(EFunCall *p);
  void visitEMembCall(EMembCall *p);
  void visitEAt(EAt *p);
  void visitECast(ECast *p);
  void visitNeg(Neg *p);
  void visitNot(Not *p);
  void visitEMul(EMul *p);
  void visitEAdd(EAdd *p);
  void visitERel(ERel *p);
  void visitEAnd(EAnd *p);
  void visitEOr(EOr *p);
  void visitListExpr(ListExpr *p);
  void visitAddOp(AddOp *p); /* abstract class */
  void visitPlus(Plus *p);
  void visitMinus(Minus *p);
  void visitMulOp(MulOp *p); /* abstract class */
  void visitTimes(Times *p);
  void visitDiv(Div *p);
  void visitMod(Mod *p);
  void visitRelOp(RelOp *p); /* abstract class */
  void visitLTH(LTH *p);
  void visitLE(LE *p);
  void visitGTH(GTH *p);
  void visitGE(GE *p);
  void visitEQU(EQU *p);
  void visitNE(NE *p);

  void visitInteger(Integer i);
  void visitDouble(Double d);
  void visitChar(Char c);
  void visitString(String s);
  void visitIdent(String s);
 protected:
  char *buf_;
  int cur_, buf_size;

  void inline bufAppend(const char *s)
  {
    int len = strlen(s);
    while (cur_ + len >= buf_size)
    {
      buf_size *= 2; /* Double the buffer size */
    }
    resizeBuffer();
    for(int n = 0; n < len; n++)
    {
      buf_[cur_ + n] = s[n];
    }
    cur_ += len;
    buf_[cur_] = 0;
  }

  void inline bufAppend(const char c)
  {
    if (cur_ >= buf_size)
    {
      buf_size *= 2; /* Double the buffer size */
      resizeBuffer();
    }
    buf_[cur_] = c;
    cur_++;
    buf_[cur_] = 0;
  }

  void inline bufAppend(String str)
  {
    const char *s = str.c_str();
    bufAppend(s);
  }
  void inline bufReset(void)
  {
    if (buf_) free(buf_);
    buf_size = LATTE_BUFFER_INITIAL;
    buf_ = (char *) malloc(buf_size);
    if (!buf_) {
      fprintf(stderr, "Error: Out of memory while allocating buffer!\n");
      exit(1);
    }
    memset(buf_, 0, buf_size);
    cur_ = 0;
  }

  void inline resizeBuffer(void)
  {
    char *temp = (char *) malloc(buf_size);
    if (!temp)
    {
      fprintf(stderr, "Error: Out of memory while attempting to grow buffer!\n");
      exit(1);
    }
    if (buf_)
    {
      strcpy(temp, buf_);
      free(buf_);
    }
    buf_ = temp;
  }
};



class ShowAbsyn : public Visitor
{
 public:
  ShowAbsyn(void);
  ~ShowAbsyn(void);
  char *show(Visitable *v);

  void visitIdentP(IdentP *p); /* abstract class */
  void visitIdentPos(IdentPos *p);
  void visitProgram(Program *p); /* abstract class */
  void visitProg(Prog *p);
  void visitListTopDef(ListTopDef *p);
  void visitTopDef(TopDef *p); /* abstract class */
  void visitClsDefTop(ClsDefTop *p);
  void visitFnDefTop(FnDefTop *p);
  void visitFunDef(FunDef *p); /* abstract class */
  void visitFnDef(FnDef *p);
  void visitArg(Arg *p); /* abstract class */
  void visitAr(Ar *p);
  void visitListArg(ListArg *p);
  void visitClassDef(ClassDef *p); /* abstract class */
  void visitClsDef(ClsDef *p);
  void visitClsExtDef(ClsExtDef *p);
  void visitClassBlock(ClassBlock *p); /* abstract class */
  void visitClsBlk(ClsBlk *p);
  void visitClassBlockDef(ClassBlockDef *p); /* abstract class */
  void visitClsMthDef(ClsMthDef *p);
  void visitClsFldDef(ClsFldDef *p);
  void visitClsFldDefItem(ClsFldDefItem *p); /* abstract class */
  void visitClsFldDefIt(ClsFldDefIt *p);
  void visitListClsFldDefItem(ListClsFldDefItem *p);
  void visitListClassBlockDef(ListClassBlockDef *p);
  void visitBlock(Block *p); /* abstract class */
  void visitBlk(Blk *p);
  void visitListStmt(ListStmt *p);
  void visitStmt(Stmt *p); /* abstract class */
  void visitDecl(Decl *p);
  void visitAssign(Assign *p);
  void visitIncr(Incr *p);
  void visitDecr(Decr *p);
  void visitRet(Ret *p);
  void visitVRet(VRet *p);
  void visitWhile(While *p);
  void visitFor(For *p);
  void visitIf(If *p);
  void visitIfElse(IfElse *p);
  void visitBStmt(BStmt *p);
  void visitEmpty(Empty *p);
  void visitExp(Exp *p);
  void visitDeclItem(DeclItem *p); /* abstract class */
  void visitNoInit(NoInit *p);
  void visitInit(Init *p);
  void visitListDeclItem(ListDeclItem *p);
  void visitClassType(ClassType *p); /* abstract class */
  void visitTClass(TClass *p);
  void visitBasicType(BasicType *p); /* abstract class */
  void visitTInt(TInt *p);
  void visitTStr(TStr *p);
  void visitTBool(TBool *p);
  void visitNonVoidType(NonVoidType *p); /* abstract class */
  void visitTNVC(TNVC *p);
  void visitTNVB(TNVB *p);
  void visitType(Type *p); /* abstract class */
  void visitTArray(TArray *p);
  void visitTSingle(TSingle *p);
  void visitTVoid(TVoid *p);
  void visitCastOn(CastOn *p); /* abstract class */
  void visitCastOnClass(CastOnClass *p);
  void visitCastOnArr(CastOnArr *p);
  void visitCastOnBasic(CastOnBasic *p);
  void visitExpr(Expr *p); /* abstract class */
  void visitENewArr(ENewArr *p);
  void visitENewCls(ENewCls *p);
  void visitELitInt(ELitInt *p);
  void visitELitTrue(ELitTrue *p);
  void visitELitFalse(ELitFalse *p);
  void visitEString(EString *p);
  void visitEVar(EVar *p);
  void visitENull(ENull *p);
  void visitEMemb(EMemb *p);
  void visitEFunCall(EFunCall *p);
  void visitEMembCall(EMembCall *p);
  void visitEAt(EAt *p);
  void visitECast(ECast *p);
  void visitNeg(Neg *p);
  void visitNot(Not *p);
  void visitEMul(EMul *p);
  void visitEAdd(EAdd *p);
  void visitERel(ERel *p);
  void visitEAnd(EAnd *p);
  void visitEOr(EOr *p);
  void visitListExpr(ListExpr *p);
  void visitAddOp(AddOp *p); /* abstract class */
  void visitPlus(Plus *p);
  void visitMinus(Minus *p);
  void visitMulOp(MulOp *p); /* abstract class */
  void visitTimes(Times *p);
  void visitDiv(Div *p);
  void visitMod(Mod *p);
  void visitRelOp(RelOp *p); /* abstract class */
  void visitLTH(LTH *p);
  void visitLE(LE *p);
  void visitGTH(GTH *p);
  void visitGE(GE *p);
  void visitEQU(EQU *p);
  void visitNE(NE *p);

  void visitInteger(Integer i);
  void visitDouble(Double d);
  void visitChar(Char c);
  void visitString(String s);
  void visitIdent(String s);
 protected:
  char *buf_;
  int cur_, buf_size;

  void inline bufAppend(const char *s)
  {
    int len = strlen(s);
    while (cur_ + len >= buf_size)
    {
      buf_size *= 2; /* Double the buffer size */
    }
    resizeBuffer();
    for(int n = 0; n < len; n++)
    {
      buf_[cur_ + n] = s[n];
    }
    cur_ += len;
    buf_[cur_] = 0;
  }

  void inline bufAppend(const char c)
  {
    if (cur_ >= buf_size)
    {
      buf_size *= 2; /* Double the buffer size */
      resizeBuffer();
    }
    buf_[cur_] = c;
    cur_++;
    buf_[cur_] = 0;
  }

  void inline bufAppend(String str)
  {
    const char *s = str.c_str();
    bufAppend(s);
  }
  void inline bufReset(void)
  {
    if (buf_) free(buf_);
    buf_size = LATTE_BUFFER_INITIAL;
    buf_ = (char *) malloc(buf_size);
    if (!buf_) {
      fprintf(stderr, "Error: Out of memory while allocating buffer!\n");
      exit(1);
    }
    memset(buf_, 0, buf_size);
    cur_ = 0;
  }

  void inline resizeBuffer(void)
  {
    char *temp = (char *) malloc(buf_size);
    if (!temp)
    {
      fprintf(stderr, "Error: Out of memory while attempting to grow buffer!\n");
      exit(1);
    }
    if (buf_)
    {
      strcpy(temp, buf_);
      free(buf_);
    }
    buf_ = temp;
  }
};


}

#endif

