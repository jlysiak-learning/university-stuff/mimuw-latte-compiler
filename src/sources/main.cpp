/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Latte compiler wrapper.
 *
 * INPUT:
 * 	- STDIN if argc == 1
 * 	- FILE if argc > 1
 *
 * OUTPUT:
 * 	Compilation result is printed on STDOUT.
 * 	Use redirection operator to save into file.
 *
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <latte/Compiler.h>
#include <latte/common/Logger.h>


int main(int argc, char ** argv)
{
	FILE *input;

	if (argc < 2) {
		std::cerr << "Usage: LatteCompiler loglevel <x64 | llvm> [file]\n";
		exit(1);
	}

	int lvl = std::stoi(std::string(argv[1]));
	std::string type = argv[2];

	if (argc > 2) {
		input = fopen(argv[2], "r");
		if (!input) {
			fprintf(stderr, "Error opening input file.\n");
			exit(1);
		}
	} else
		input = stdin;

	Common::log_lvl_t loglvl = Common::LOG_LVL_ERR;
	if (lvl < 1)
		loglvl = Common::LOG_LVL_WARN;
	else if (lvl < 2) 
		loglvl = Common::LOG_LVL_INFO;
	else
		loglvl = Common::LOG_LVL_DEBUG;

	Common::Logger logger(std::cerr, loglvl);
	Compiler compiler(input, std::cout, logger);
	compiler.compile();

	if (argc > 1)
		fclose(input);
	if (logger.has_errors()) {
		std::cerr << "Compilation: FAIL!\n";
		return 1;
	}
	else {
		std::cerr << "Compilation: SUCCESS!\n";
		return 0;
	}
}

