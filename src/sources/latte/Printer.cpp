/*** BNFC-Generated Pretty Printer and Abstract Syntax Viewer ***/

#include <string>
#include <latte/latte/Printer.h>
#define INDENT_WIDTH 2

namespace Latte
{
//You may wish to change render
void PrintAbsyn::render(Char c)
{
  if (c == '{')
  {
     bufAppend('\n');
     indent();
     bufAppend(c);
     _n_ = _n_ + INDENT_WIDTH;
     bufAppend('\n');
     indent();
  }
  else if (c == '(' || c == '[')
     bufAppend(c);
  else if (c == ')' || c == ']')
  {
     backup();
     bufAppend(c);
  }
  else if (c == '}')
  {
     int t;
     _n_ = _n_ - INDENT_WIDTH;
     for (t=0; t<INDENT_WIDTH; t++) {
       backup();
     }
     bufAppend(c);
     bufAppend('\n');
     indent();
  }
  else if (c == ',')
  {
     backup();
     bufAppend(c);
     bufAppend(' ');
  }
  else if (c == ';')
  {
     backup();
     bufAppend(c);
     bufAppend('\n');
     indent();
  }
  else if (c == 0) return;
  else
  {
     bufAppend(' ');
     bufAppend(c);
     bufAppend(' ');
  }
}

void PrintAbsyn::render(String s_)
{
  const char *s = s_.c_str() ;
  if(strlen(s) > 0)
  {
    bufAppend(s);
    bufAppend(' ');
  }
}
void PrintAbsyn::render(const char *s)
{
  if(strlen(s) > 0)
  {
    bufAppend(s);
    bufAppend(' ');
  }
}

void PrintAbsyn::indent()
{
  int n = _n_;
  while (n > 0)
  {
    bufAppend(' ');
    n--;
  }
}

void PrintAbsyn::backup()
{
  if (buf_[cur_ - 1] == ' ')
  {
    buf_[cur_ - 1] = 0;
    cur_--;
  }
}

PrintAbsyn::PrintAbsyn(void)
{
  _i_ = 0; _n_ = 0;
  buf_ = 0;
  bufReset();
}

PrintAbsyn::~PrintAbsyn(void)
{
}

char *PrintAbsyn::print(Visitable *v)
{
  _i_ = 0; _n_ = 0;
  bufReset();
  v->accept(this);
  return buf_;
}

void PrintAbsyn::visitIdentP(IdentP *p) {} //abstract class

void PrintAbsyn::visitIdentPos(IdentPos *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  visitIdent(p->ident_);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitProgram(Program *p) {} //abstract class

void PrintAbsyn::visitProg(Prog *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  if(p->listtopdef_) {_i_ = 0; p->listtopdef_->accept(this);}

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListTopDef(ListTopDef *listtopdef)
{
  for (ListTopDef::const_iterator i = listtopdef->begin() ; i != listtopdef->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listtopdef->end() - 1) render("");
  }
}void PrintAbsyn::visitTopDef(TopDef *p) {} //abstract class

void PrintAbsyn::visitClsDefTop(ClsDefTop *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->classdef_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFnDefTop(FnDefTop *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->fundef_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFunDef(FunDef *p) {} //abstract class

void PrintAbsyn::visitFnDef(FnDef *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->type_->accept(this);
  _i_ = 0; p->identp_->accept(this);
  render('(');
  if(p->listarg_) {_i_ = 0; p->listarg_->accept(this);}
  render(')');
  _i_ = 0; p->block_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitArg(Arg *p) {} //abstract class

void PrintAbsyn::visitAr(Ar *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->type_->accept(this);
  _i_ = 0; p->identp_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListArg(ListArg *listarg)
{
  for (ListArg::const_iterator i = listarg->begin() ; i != listarg->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listarg->end() - 1) render(',');
  }
}void PrintAbsyn::visitClassDef(ClassDef *p) {} //abstract class

void PrintAbsyn::visitClsDef(ClsDef *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("class");
  _i_ = 0; p->identp_->accept(this);
  _i_ = 0; p->classblock_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitClsExtDef(ClsExtDef *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("class");
  _i_ = 0; p->identp_->accept(this);
  render("extends");
  visitIdent(p->ident_);
  _i_ = 0; p->classblock_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitClassBlock(ClassBlock *p) {} //abstract class

void PrintAbsyn::visitClsBlk(ClsBlk *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('{');
  if(p->listclassblockdef_) {_i_ = 0; p->listclassblockdef_->accept(this);}
  render('}');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitClassBlockDef(ClassBlockDef *p) {} //abstract class

void PrintAbsyn::visitClsMthDef(ClsMthDef *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->fundef_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitClsFldDef(ClsFldDef *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->type_->accept(this);
  if(p->listclsflddefitem_) {_i_ = 0; p->listclsflddefitem_->accept(this);}
  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitClsFldDefItem(ClsFldDefItem *p) {} //abstract class

void PrintAbsyn::visitClsFldDefIt(ClsFldDefIt *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->identp_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListClsFldDefItem(ListClsFldDefItem *listclsflddefitem)
{
  for (ListClsFldDefItem::const_iterator i = listclsflddefitem->begin() ; i != listclsflddefitem->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listclsflddefitem->end() - 1) render(',');
  }
}void PrintAbsyn::visitListClassBlockDef(ListClassBlockDef *listclassblockdef)
{
  for (ListClassBlockDef::const_iterator i = listclassblockdef->begin() ; i != listclassblockdef->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listclassblockdef->end() - 1) render("");
  }
}void PrintAbsyn::visitBlock(Block *p) {} //abstract class

void PrintAbsyn::visitBlk(Blk *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('{');
  if(p->liststmt_) {_i_ = 0; p->liststmt_->accept(this);}
  render('}');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListStmt(ListStmt *liststmt)
{
  for (ListStmt::const_iterator i = liststmt->begin() ; i != liststmt->end() ; ++i)
  {
    (*i)->accept(this);
    render("");
  }
}void PrintAbsyn::visitStmt(Stmt *p) {} //abstract class

void PrintAbsyn::visitDecl(Decl *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->type_->accept(this);
  if(p->listdeclitem_) {_i_ = 0; p->listdeclitem_->accept(this);}
  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitAssign(Assign *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->expr_1->accept(this);
  render('=');
  _i_ = 0; p->expr_2->accept(this);
  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitIncr(Incr *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->expr_->accept(this);
  render("++");
  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitDecr(Decr *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->expr_->accept(this);
  render("--");
  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitRet(Ret *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("return");
  _i_ = 0; p->expr_->accept(this);
  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitVRet(VRet *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("return");
  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitWhile(While *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("while");
  render('(');
  _i_ = 0; p->expr_->accept(this);
  render(')');
  _i_ = 0; p->stmt_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitFor(For *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("for");
  render('(');
  _i_ = 0; p->type_->accept(this);
  visitIdent(p->ident_);
  render(':');
  _i_ = 0; p->expr_->accept(this);
  render(')');
  _i_ = 0; p->stmt_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitIf(If *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("if");
  render('(');
  _i_ = 0; p->expr_->accept(this);
  render(')');
  _i_ = 0; p->stmt_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitIfElse(IfElse *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("if");
  render('(');
  _i_ = 0; p->expr_->accept(this);
  render(')');
  _i_ = 0; p->stmt_1->accept(this);
  render("else");
  _i_ = 0; p->stmt_2->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitBStmt(BStmt *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->block_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEmpty(Empty *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitExp(Exp *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->expr_->accept(this);
  render(';');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitDeclItem(DeclItem *p) {} //abstract class

void PrintAbsyn::visitNoInit(NoInit *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  visitIdent(p->ident_);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitInit(Init *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  visitIdent(p->ident_);
  render('=');
  _i_ = 0; p->expr_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListDeclItem(ListDeclItem *listdeclitem)
{
  for (ListDeclItem::const_iterator i = listdeclitem->begin() ; i != listdeclitem->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listdeclitem->end() - 1) render(',');
  }
}void PrintAbsyn::visitClassType(ClassType *p) {} //abstract class

void PrintAbsyn::visitTClass(TClass *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  visitIdent(p->ident_);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitBasicType(BasicType *p) {} //abstract class

void PrintAbsyn::visitTInt(TInt *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("int");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitTStr(TStr *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("string");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitTBool(TBool *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("boolean");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitNonVoidType(NonVoidType *p) {} //abstract class

void PrintAbsyn::visitTNVC(TNVC *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->classtype_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitTNVB(TNVB *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->basictype_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitType(Type *p) {} //abstract class

void PrintAbsyn::visitTArray(TArray *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->nonvoidtype_->accept(this);
  render("[]");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitTSingle(TSingle *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 0; p->nonvoidtype_->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitTVoid(TVoid *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("void");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitCastOn(CastOn *p) {} //abstract class

void PrintAbsyn::visitCastOnClass(CastOnClass *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('(');
  visitIdent(p->ident_);
  render(')');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitCastOnArr(CastOnArr *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('(');
  _i_ = 0; p->nonvoidtype_->accept(this);
  render("[]");
  render(')');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitCastOnBasic(CastOnBasic *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('(');
  _i_ = 0; p->basictype_->accept(this);
  render(')');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitExpr(Expr *p) {} //abstract class

void PrintAbsyn::visitENewArr(ENewArr *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  render("new");
  _i_ = 0; p->nonvoidtype_->accept(this);
  render('[');
  _i_ = 0; p->expr_->accept(this);
  render(']');

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitENewCls(ENewCls *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  render("new");
  visitIdent(p->ident_);

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitELitInt(ELitInt *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  visitInteger(p->integer_);

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitELitTrue(ELitTrue *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  render("true");

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitELitFalse(ELitFalse *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  render("false");

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEString(EString *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  visitString(p->string_);

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEVar(EVar *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  visitIdent(p->ident_);

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitENull(ENull *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  render("null");

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEMemb(EMemb *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  _i_ = 6; p->expr_->accept(this);
  render('.');
  visitIdent(p->ident_);

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEFunCall(EFunCall *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  visitIdent(p->ident_);
  render('(');
  if(p->listexpr_) {_i_ = 0; p->listexpr_->accept(this);}
  render(')');

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEMembCall(EMembCall *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  _i_ = 6; p->expr_->accept(this);
  render('.');
  visitIdent(p->ident_);
  render('(');
  if(p->listexpr_) {_i_ = 0; p->listexpr_->accept(this);}
  render(')');

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEAt(EAt *p)
{
  int oldi = _i_;
  if (oldi > 6) render(LATTE__L_PAREN);

  _i_ = 6; p->expr_1->accept(this);
  render('[');
  _i_ = 0; p->expr_2->accept(this);
  render(']');

  if (oldi > 6) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitECast(ECast *p)
{
  int oldi = _i_;
  if (oldi > 5) render(LATTE__L_PAREN);

  _i_ = 0; p->caston_->accept(this);
  _i_ = 5; p->expr_->accept(this);

  if (oldi > 5) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitNeg(Neg *p)
{
  int oldi = _i_;
  if (oldi > 5) render(LATTE__L_PAREN);

  render('-');
  _i_ = 5; p->expr_->accept(this);

  if (oldi > 5) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitNot(Not *p)
{
  int oldi = _i_;
  if (oldi > 5) render(LATTE__L_PAREN);

  render('!');
  _i_ = 5; p->expr_->accept(this);

  if (oldi > 5) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEMul(EMul *p)
{
  int oldi = _i_;
  if (oldi > 4) render(LATTE__L_PAREN);

  _i_ = 4; p->expr_1->accept(this);
  _i_ = 0; p->mulop_->accept(this);
  _i_ = 5; p->expr_2->accept(this);

  if (oldi > 4) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEAdd(EAdd *p)
{
  int oldi = _i_;
  if (oldi > 3) render(LATTE__L_PAREN);

  _i_ = 3; p->expr_1->accept(this);
  _i_ = 0; p->addop_->accept(this);
  _i_ = 4; p->expr_2->accept(this);

  if (oldi > 3) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitERel(ERel *p)
{
  int oldi = _i_;
  if (oldi > 2) render(LATTE__L_PAREN);

  _i_ = 2; p->expr_1->accept(this);
  _i_ = 0; p->relop_->accept(this);
  _i_ = 3; p->expr_2->accept(this);

  if (oldi > 2) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEAnd(EAnd *p)
{
  int oldi = _i_;
  if (oldi > 1) render(LATTE__L_PAREN);

  _i_ = 2; p->expr_1->accept(this);
  render("&&");
  _i_ = 1; p->expr_2->accept(this);

  if (oldi > 1) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEOr(EOr *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  _i_ = 1; p->expr_1->accept(this);
  render("||");
  _i_ = 0; p->expr_2->accept(this);

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitListExpr(ListExpr *listexpr)
{
  for (ListExpr::const_iterator i = listexpr->begin() ; i != listexpr->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listexpr->end() - 1) render(',');
  }
}void PrintAbsyn::visitAddOp(AddOp *p) {} //abstract class

void PrintAbsyn::visitPlus(Plus *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('+');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitMinus(Minus *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('-');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitMulOp(MulOp *p) {} //abstract class

void PrintAbsyn::visitTimes(Times *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('*');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitDiv(Div *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('/');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitMod(Mod *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('%');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitRelOp(RelOp *p) {} //abstract class

void PrintAbsyn::visitLTH(LTH *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('<');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitLE(LE *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("<=");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitGTH(GTH *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render('>');

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitGE(GE *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render(">=");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitEQU(EQU *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("==");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitNE(NE *p)
{
  int oldi = _i_;
  if (oldi > 0) render(LATTE__L_PAREN);

  render("!=");

  if (oldi > 0) render(LATTE__R_PAREN);

  _i_ = oldi;
}

void PrintAbsyn::visitInteger(Integer i)
{
  char tmp[16];
  sprintf(tmp, "%d", i);
  bufAppend(tmp);
}

void PrintAbsyn::visitDouble(Double d)
{
  char tmp[16];
  sprintf(tmp, "%g", d);
  bufAppend(tmp);
}

void PrintAbsyn::visitChar(Char c)
{
  bufAppend('\'');
  bufAppend(c);
  bufAppend('\'');
}

void PrintAbsyn::visitString(String s)
{
  bufAppend('\"');
  bufAppend(s);
  bufAppend('\"');
}

void PrintAbsyn::visitIdent(String s)
{
  render(s);
}

ShowAbsyn::ShowAbsyn(void)
{
  buf_ = 0;
  bufReset();
}

ShowAbsyn::~ShowAbsyn(void)
{
}

char *ShowAbsyn::show(Visitable *v)
{
  bufReset();
  v->accept(this);
  return buf_;
}

void ShowAbsyn::visitIdentP(IdentP *p) {} //abstract class

void ShowAbsyn::visitIdentPos(IdentPos *p)
{
  bufAppend('(');
  bufAppend("IdentPos");
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(')');
}
void ShowAbsyn::visitProgram(Program *p) {} //abstract class

void ShowAbsyn::visitProg(Prog *p)
{
  bufAppend('(');
  bufAppend("Prog");
  bufAppend(' ');
  bufAppend('[');
  if (p->listtopdef_)  p->listtopdef_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitListTopDef(ListTopDef *listtopdef)
{
  for (ListTopDef::const_iterator i = listtopdef->begin() ; i != listtopdef->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listtopdef->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitTopDef(TopDef *p) {} //abstract class

void ShowAbsyn::visitClsDefTop(ClsDefTop *p)
{
  bufAppend('(');
  bufAppend("ClsDefTop");
  bufAppend(' ');
  bufAppend('[');
  if (p->classdef_)  p->classdef_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitFnDefTop(FnDefTop *p)
{
  bufAppend('(');
  bufAppend("FnDefTop");
  bufAppend(' ');
  bufAppend('[');
  if (p->fundef_)  p->fundef_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitFunDef(FunDef *p) {} //abstract class

void ShowAbsyn::visitFnDef(FnDef *p)
{
  bufAppend('(');
  bufAppend("FnDef");
  bufAppend(' ');
  bufAppend('[');
  if (p->type_)  p->type_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->identp_)  p->identp_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->listarg_)  p->listarg_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->block_)  p->block_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitArg(Arg *p) {} //abstract class

void ShowAbsyn::visitAr(Ar *p)
{
  bufAppend('(');
  bufAppend("Ar");
  bufAppend(' ');
  bufAppend('[');
  if (p->type_)  p->type_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->identp_)  p->identp_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitListArg(ListArg *listarg)
{
  for (ListArg::const_iterator i = listarg->begin() ; i != listarg->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listarg->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitClassDef(ClassDef *p) {} //abstract class

void ShowAbsyn::visitClsDef(ClsDef *p)
{
  bufAppend('(');
  bufAppend("ClsDef");
  bufAppend(' ');
  bufAppend('[');
  if (p->identp_)  p->identp_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->classblock_)  p->classblock_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitClsExtDef(ClsExtDef *p)
{
  bufAppend('(');
  bufAppend("ClsExtDef");
  bufAppend(' ');
  bufAppend('[');
  if (p->identp_)  p->identp_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(' ');
  bufAppend('[');
  if (p->classblock_)  p->classblock_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitClassBlock(ClassBlock *p) {} //abstract class

void ShowAbsyn::visitClsBlk(ClsBlk *p)
{
  bufAppend('(');
  bufAppend("ClsBlk");
  bufAppend(' ');
  bufAppend('[');
  if (p->listclassblockdef_)  p->listclassblockdef_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitClassBlockDef(ClassBlockDef *p) {} //abstract class

void ShowAbsyn::visitClsMthDef(ClsMthDef *p)
{
  bufAppend('(');
  bufAppend("ClsMthDef");
  bufAppend(' ');
  bufAppend('[');
  if (p->fundef_)  p->fundef_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitClsFldDef(ClsFldDef *p)
{
  bufAppend('(');
  bufAppend("ClsFldDef");
  bufAppend(' ');
  bufAppend('[');
  if (p->type_)  p->type_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->listclsflddefitem_)  p->listclsflddefitem_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitClsFldDefItem(ClsFldDefItem *p) {} //abstract class

void ShowAbsyn::visitClsFldDefIt(ClsFldDefIt *p)
{
  bufAppend('(');
  bufAppend("ClsFldDefIt");
  bufAppend(' ');
  bufAppend('[');
  if (p->identp_)  p->identp_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitListClsFldDefItem(ListClsFldDefItem *listclsflddefitem)
{
  for (ListClsFldDefItem::const_iterator i = listclsflddefitem->begin() ; i != listclsflddefitem->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listclsflddefitem->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitListClassBlockDef(ListClassBlockDef *listclassblockdef)
{
  for (ListClassBlockDef::const_iterator i = listclassblockdef->begin() ; i != listclassblockdef->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listclassblockdef->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitBlock(Block *p) {} //abstract class

void ShowAbsyn::visitBlk(Blk *p)
{
  bufAppend('(');
  bufAppend("Blk");
  bufAppend(' ');
  bufAppend('[');
  if (p->liststmt_)  p->liststmt_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitListStmt(ListStmt *liststmt)
{
  for (ListStmt::const_iterator i = liststmt->begin() ; i != liststmt->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != liststmt->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitStmt(Stmt *p) {} //abstract class

void ShowAbsyn::visitDecl(Decl *p)
{
  bufAppend('(');
  bufAppend("Decl");
  bufAppend(' ');
  bufAppend('[');
  if (p->type_)  p->type_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->listdeclitem_)  p->listdeclitem_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitAssign(Assign *p)
{
  bufAppend('(');
  bufAppend("Assign");
  bufAppend(' ');
  p->expr_1->accept(this);
  bufAppend(' ');
  p->expr_2->accept(this);
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitIncr(Incr *p)
{
  bufAppend('(');
  bufAppend("Incr");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitDecr(Decr *p)
{
  bufAppend('(');
  bufAppend("Decr");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitRet(Ret *p)
{
  bufAppend('(');
  bufAppend("Ret");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitVRet(VRet *p)
{
  bufAppend("VRet");
}
void ShowAbsyn::visitWhile(While *p)
{
  bufAppend('(');
  bufAppend("While");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->stmt_)  p->stmt_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitFor(For *p)
{
  bufAppend('(');
  bufAppend("For");
  bufAppend(' ');
  bufAppend('[');
  if (p->type_)  p->type_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->stmt_)  p->stmt_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitIf(If *p)
{
  bufAppend('(');
  bufAppend("If");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->stmt_)  p->stmt_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitIfElse(IfElse *p)
{
  bufAppend('(');
  bufAppend("IfElse");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  p->stmt_1->accept(this);
  bufAppend(' ');
  p->stmt_2->accept(this);
  bufAppend(')');
}
void ShowAbsyn::visitBStmt(BStmt *p)
{
  bufAppend('(');
  bufAppend("BStmt");
  bufAppend(' ');
  bufAppend('[');
  if (p->block_)  p->block_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitEmpty(Empty *p)
{
  bufAppend("Empty");
}
void ShowAbsyn::visitExp(Exp *p)
{
  bufAppend('(');
  bufAppend("Exp");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitDeclItem(DeclItem *p) {} //abstract class

void ShowAbsyn::visitNoInit(NoInit *p)
{
  bufAppend('(');
  bufAppend("NoInit");
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(')');
}
void ShowAbsyn::visitInit(Init *p)
{
  bufAppend('(');
  bufAppend("Init");
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitListDeclItem(ListDeclItem *listdeclitem)
{
  for (ListDeclItem::const_iterator i = listdeclitem->begin() ; i != listdeclitem->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listdeclitem->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitClassType(ClassType *p) {} //abstract class

void ShowAbsyn::visitTClass(TClass *p)
{
  bufAppend('(');
  bufAppend("TClass");
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(')');
}
void ShowAbsyn::visitBasicType(BasicType *p) {} //abstract class

void ShowAbsyn::visitTInt(TInt *p)
{
  bufAppend("TInt");
}
void ShowAbsyn::visitTStr(TStr *p)
{
  bufAppend("TStr");
}
void ShowAbsyn::visitTBool(TBool *p)
{
  bufAppend("TBool");
}
void ShowAbsyn::visitNonVoidType(NonVoidType *p) {} //abstract class

void ShowAbsyn::visitTNVC(TNVC *p)
{
  bufAppend('(');
  bufAppend("TNVC");
  bufAppend(' ');
  bufAppend('[');
  if (p->classtype_)  p->classtype_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitTNVB(TNVB *p)
{
  bufAppend('(');
  bufAppend("TNVB");
  bufAppend(' ');
  bufAppend('[');
  if (p->basictype_)  p->basictype_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitType(Type *p) {} //abstract class

void ShowAbsyn::visitTArray(TArray *p)
{
  bufAppend('(');
  bufAppend("TArray");
  bufAppend(' ');
  bufAppend('[');
  if (p->nonvoidtype_)  p->nonvoidtype_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitTSingle(TSingle *p)
{
  bufAppend('(');
  bufAppend("TSingle");
  bufAppend(' ');
  bufAppend('[');
  if (p->nonvoidtype_)  p->nonvoidtype_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitTVoid(TVoid *p)
{
  bufAppend("TVoid");
}
void ShowAbsyn::visitCastOn(CastOn *p) {} //abstract class

void ShowAbsyn::visitCastOnClass(CastOnClass *p)
{
  bufAppend('(');
  bufAppend("CastOnClass");
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitCastOnArr(CastOnArr *p)
{
  bufAppend('(');
  bufAppend("CastOnArr");
  bufAppend(' ');
  bufAppend('[');
  if (p->nonvoidtype_)  p->nonvoidtype_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitCastOnBasic(CastOnBasic *p)
{
  bufAppend('(');
  bufAppend("CastOnBasic");
  bufAppend(' ');
  bufAppend('[');
  if (p->basictype_)  p->basictype_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitExpr(Expr *p) {} //abstract class

void ShowAbsyn::visitENewArr(ENewArr *p)
{
  bufAppend('(');
  bufAppend("ENewArr");
  bufAppend(' ');
  bufAppend('[');
  if (p->nonvoidtype_)  p->nonvoidtype_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitENewCls(ENewCls *p)
{
  bufAppend('(');
  bufAppend("ENewCls");
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(')');
}
void ShowAbsyn::visitELitInt(ELitInt *p)
{
  bufAppend('(');
  bufAppend("ELitInt");
  bufAppend(' ');
  visitInteger(p->integer_);
  bufAppend(')');
}
void ShowAbsyn::visitELitTrue(ELitTrue *p)
{
  bufAppend("ELitTrue");
}
void ShowAbsyn::visitELitFalse(ELitFalse *p)
{
  bufAppend("ELitFalse");
}
void ShowAbsyn::visitEString(EString *p)
{
  bufAppend('(');
  bufAppend("EString");
  bufAppend(' ');
  visitString(p->string_);
  bufAppend(')');
}
void ShowAbsyn::visitEVar(EVar *p)
{
  bufAppend('(');
  bufAppend("EVar");
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(')');
}
void ShowAbsyn::visitENull(ENull *p)
{
  bufAppend("ENull");
}
void ShowAbsyn::visitEMemb(EMemb *p)
{
  bufAppend('(');
  bufAppend("EMemb");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(')');
}
void ShowAbsyn::visitEFunCall(EFunCall *p)
{
  bufAppend('(');
  bufAppend("EFunCall");
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(' ');
  bufAppend('[');
  if (p->listexpr_)  p->listexpr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitEMembCall(EMembCall *p)
{
  bufAppend('(');
  bufAppend("EMembCall");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  visitIdent(p->ident_);
  bufAppend(' ');
  bufAppend('[');
  if (p->listexpr_)  p->listexpr_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitEAt(EAt *p)
{
  bufAppend('(');
  bufAppend("EAt");
  bufAppend(' ');
  p->expr_1->accept(this);
  bufAppend(' ');
  p->expr_2->accept(this);
  bufAppend(' ');
  bufAppend(')');
}
void ShowAbsyn::visitECast(ECast *p)
{
  bufAppend('(');
  bufAppend("ECast");
  bufAppend(' ');
  bufAppend('[');
  if (p->caston_)  p->caston_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitNeg(Neg *p)
{
  bufAppend('(');
  bufAppend("Neg");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitNot(Not *p)
{
  bufAppend('(');
  bufAppend("Not");
  bufAppend(' ');
  bufAppend('[');
  if (p->expr_)  p->expr_->accept(this);
  bufAppend(']');
  bufAppend(')');
}
void ShowAbsyn::visitEMul(EMul *p)
{
  bufAppend('(');
  bufAppend("EMul");
  bufAppend(' ');
  p->expr_1->accept(this);
  bufAppend(' ');
  bufAppend('[');
  if (p->mulop_)  p->mulop_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  p->expr_2->accept(this);
  bufAppend(')');
}
void ShowAbsyn::visitEAdd(EAdd *p)
{
  bufAppend('(');
  bufAppend("EAdd");
  bufAppend(' ');
  p->expr_1->accept(this);
  bufAppend(' ');
  bufAppend('[');
  if (p->addop_)  p->addop_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  p->expr_2->accept(this);
  bufAppend(')');
}
void ShowAbsyn::visitERel(ERel *p)
{
  bufAppend('(');
  bufAppend("ERel");
  bufAppend(' ');
  p->expr_1->accept(this);
  bufAppend(' ');
  bufAppend('[');
  if (p->relop_)  p->relop_->accept(this);
  bufAppend(']');
  bufAppend(' ');
  p->expr_2->accept(this);
  bufAppend(')');
}
void ShowAbsyn::visitEAnd(EAnd *p)
{
  bufAppend('(');
  bufAppend("EAnd");
  bufAppend(' ');
  p->expr_1->accept(this);
  bufAppend(' ');
  p->expr_2->accept(this);
  bufAppend(')');
}
void ShowAbsyn::visitEOr(EOr *p)
{
  bufAppend('(');
  bufAppend("EOr");
  bufAppend(' ');
  p->expr_1->accept(this);
  bufAppend(' ');
  p->expr_2->accept(this);
  bufAppend(')');
}
void ShowAbsyn::visitListExpr(ListExpr *listexpr)
{
  for (ListExpr::const_iterator i = listexpr->begin() ; i != listexpr->end() ; ++i)
  {
    (*i)->accept(this);
    if (i != listexpr->end() - 1) bufAppend(", ");
  }
}

void ShowAbsyn::visitAddOp(AddOp *p) {} //abstract class

void ShowAbsyn::visitPlus(Plus *p)
{
  bufAppend("Plus");
}
void ShowAbsyn::visitMinus(Minus *p)
{
  bufAppend("Minus");
}
void ShowAbsyn::visitMulOp(MulOp *p) {} //abstract class

void ShowAbsyn::visitTimes(Times *p)
{
  bufAppend("Times");
}
void ShowAbsyn::visitDiv(Div *p)
{
  bufAppend("Div");
}
void ShowAbsyn::visitMod(Mod *p)
{
  bufAppend("Mod");
}
void ShowAbsyn::visitRelOp(RelOp *p) {} //abstract class

void ShowAbsyn::visitLTH(LTH *p)
{
  bufAppend("LTH");
}
void ShowAbsyn::visitLE(LE *p)
{
  bufAppend("LE");
}
void ShowAbsyn::visitGTH(GTH *p)
{
  bufAppend("GTH");
}
void ShowAbsyn::visitGE(GE *p)
{
  bufAppend("GE");
}
void ShowAbsyn::visitEQU(EQU *p)
{
  bufAppend("EQU");
}
void ShowAbsyn::visitNE(NE *p)
{
  bufAppend("NE");
}
void ShowAbsyn::visitInteger(Integer i)
{
  char tmp[16];
  sprintf(tmp, "%d", i);
  bufAppend(tmp);
}
void ShowAbsyn::visitDouble(Double d)
{
  char tmp[16];
  sprintf(tmp, "%g", d);
  bufAppend(tmp);
}
void ShowAbsyn::visitChar(Char c)
{
  bufAppend('\'');
  bufAppend(c);
  bufAppend('\'');
}
void ShowAbsyn::visitString(String s)
{
  bufAppend('\"');
  bufAppend(s);
  bufAppend('\"');
}
void ShowAbsyn::visitIdent(String s)
{
  bufAppend('\"');
  bufAppend(s);
  bufAppend('\"');
}

}
