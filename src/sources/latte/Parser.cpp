/* A Bison parser, made by GNU Bison 3.2.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Substitute the type names.  */
#define YYSTYPE         LATTEYYSTYPE
/* Substitute the variable and function names.  */
#define yyparse         Latteyyparse
#define yylex           Latteyylex
#define yyerror         Latteyyerror
#define yydebug         Latteyydebug
#define yynerrs         Latteyynerrs

#define yylval          Latteyylval
#define yychar          Latteyychar

/* First part of user prologue.  */
#line 2 "Latte.y" /* yacc.c:338  */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <latte/latte/Absyn.h>
typedef struct yy_buffer_state *YY_BUFFER_STATE;
int yyparse(void);
int yylex(void);
YY_BUFFER_STATE Latteyy_scan_string(const char *str);
void Latteyy_delete_buffer(YY_BUFFER_STATE buf);
int Latteyy_mylinenumber;
int Latteinitialize_lexer(FILE * inp);
int Latteyywrap(void)
{
  return 1;
}
void Latteyyerror(const char *str)
{
  extern char *Latteyytext;
  fprintf(stderr,"error: line %d: %s at %s\n", 
    Latteyy_mylinenumber, str, Latteyytext);
}


namespace Latte
{
static IdentP* YY_RESULT_IdentP_ = 0;
IdentP* pIdentP(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_IdentP_;
  }
}
IdentP* pIdentP(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_IdentP_;
  }
}

static Program* YY_RESULT_Program_ = 0;
Program* pProgram(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Program_;
  }
}
Program* pProgram(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Program_;
  }
}

static ListTopDef* YY_RESULT_ListTopDef_ = 0;
ListTopDef* pListTopDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListTopDef_;
  }
}
ListTopDef* pListTopDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListTopDef_;
  }
}

static TopDef* YY_RESULT_TopDef_ = 0;
TopDef* pTopDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_TopDef_;
  }
}
TopDef* pTopDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_TopDef_;
  }
}

static FunDef* YY_RESULT_FunDef_ = 0;
FunDef* pFunDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_FunDef_;
  }
}
FunDef* pFunDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_FunDef_;
  }
}

static Arg* YY_RESULT_Arg_ = 0;
Arg* pArg(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Arg_;
  }
}
Arg* pArg(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Arg_;
  }
}

static ListArg* YY_RESULT_ListArg_ = 0;
ListArg* pListArg(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListArg_;
  }
}
ListArg* pListArg(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListArg_;
  }
}

static ClassDef* YY_RESULT_ClassDef_ = 0;
ClassDef* pClassDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassDef_;
  }
}
ClassDef* pClassDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassDef_;
  }
}

static ClassBlock* YY_RESULT_ClassBlock_ = 0;
ClassBlock* pClassBlock(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassBlock_;
  }
}
ClassBlock* pClassBlock(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassBlock_;
  }
}

static ClassBlockDef* YY_RESULT_ClassBlockDef_ = 0;
ClassBlockDef* pClassBlockDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassBlockDef_;
  }
}
ClassBlockDef* pClassBlockDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassBlockDef_;
  }
}

static ClsFldDefItem* YY_RESULT_ClsFldDefItem_ = 0;
ClsFldDefItem* pClsFldDefItem(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClsFldDefItem_;
  }
}
ClsFldDefItem* pClsFldDefItem(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClsFldDefItem_;
  }
}

static ListClsFldDefItem* YY_RESULT_ListClsFldDefItem_ = 0;
ListClsFldDefItem* pListClsFldDefItem(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListClsFldDefItem_;
  }
}
ListClsFldDefItem* pListClsFldDefItem(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListClsFldDefItem_;
  }
}

static ListClassBlockDef* YY_RESULT_ListClassBlockDef_ = 0;
ListClassBlockDef* pListClassBlockDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListClassBlockDef_;
  }
}
ListClassBlockDef* pListClassBlockDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListClassBlockDef_;
  }
}

static Block* YY_RESULT_Block_ = 0;
Block* pBlock(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Block_;
  }
}
Block* pBlock(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Block_;
  }
}

static ListStmt* YY_RESULT_ListStmt_ = 0;
ListStmt* pListStmt(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListStmt_;
  }
}
ListStmt* pListStmt(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListStmt_;
  }
}

static Stmt* YY_RESULT_Stmt_ = 0;
Stmt* pStmt(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Stmt_;
  }
}
Stmt* pStmt(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Stmt_;
  }
}

static DeclItem* YY_RESULT_DeclItem_ = 0;
DeclItem* pDeclItem(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_DeclItem_;
  }
}
DeclItem* pDeclItem(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_DeclItem_;
  }
}

static ListDeclItem* YY_RESULT_ListDeclItem_ = 0;
ListDeclItem* pListDeclItem(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListDeclItem_;
  }
}
ListDeclItem* pListDeclItem(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListDeclItem_;
  }
}

static ClassType* YY_RESULT_ClassType_ = 0;
ClassType* pClassType(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassType_;
  }
}
ClassType* pClassType(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassType_;
  }
}

static BasicType* YY_RESULT_BasicType_ = 0;
BasicType* pBasicType(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_BasicType_;
  }
}
BasicType* pBasicType(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_BasicType_;
  }
}

static NonVoidType* YY_RESULT_NonVoidType_ = 0;
NonVoidType* pNonVoidType(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_NonVoidType_;
  }
}
NonVoidType* pNonVoidType(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_NonVoidType_;
  }
}

static Type* YY_RESULT_Type_ = 0;
Type* pType(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Type_;
  }
}
Type* pType(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Type_;
  }
}

static CastOn* YY_RESULT_CastOn_ = 0;
CastOn* pCastOn(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_CastOn_;
  }
}
CastOn* pCastOn(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_CastOn_;
  }
}

static Expr* YY_RESULT_Expr_ = 0;
Expr* pExpr(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Expr_;
  }
}
Expr* pExpr(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Expr_;
  }
}

static ListExpr* YY_RESULT_ListExpr_ = 0;
ListExpr* pListExpr(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListExpr_;
  }
}
ListExpr* pListExpr(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListExpr_;
  }
}

static AddOp* YY_RESULT_AddOp_ = 0;
AddOp* pAddOp(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_AddOp_;
  }
}
AddOp* pAddOp(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_AddOp_;
  }
}

static MulOp* YY_RESULT_MulOp_ = 0;
MulOp* pMulOp(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_MulOp_;
  }
}
MulOp* pMulOp(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_MulOp_;
  }
}

static RelOp* YY_RESULT_RelOp_ = 0;
RelOp* pRelOp(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_RelOp_;
  }
}
RelOp* pRelOp(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_RelOp_;
  }
}


}

#line 1033 "Parser.C" /* yacc.c:338  */
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef LATTEYYDEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define LATTEYYDEBUG 1
#  else
#   define LATTEYYDEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define LATTEYYDEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined LATTEYYDEBUG */
#if LATTEYYDEBUG
extern int Latteyydebug;
#endif

/* Token type.  */
#ifndef LATTEYYTOKENTYPE
# define LATTEYYTOKENTYPE
  enum Latteyytokentype
  {
    _ERROR_ = 258,
    LATTE__SYMB_0 = 259,
    LATTE__SYMB_1 = 260,
    LATTE__SYMB_2 = 261,
    LATTE__SYMB_3 = 262,
    LATTE__SYMB_4 = 263,
    LATTE__SYMB_5 = 264,
    LATTE__SYMB_6 = 265,
    LATTE__SYMB_7 = 266,
    LATTE__SYMB_8 = 267,
    LATTE__SYMB_9 = 268,
    LATTE__SYMB_10 = 269,
    LATTE__SYMB_11 = 270,
    LATTE__SYMB_12 = 271,
    LATTE__SYMB_13 = 272,
    LATTE__SYMB_14 = 273,
    LATTE__SYMB_15 = 274,
    LATTE__SYMB_16 = 275,
    LATTE__SYMB_17 = 276,
    LATTE__SYMB_18 = 277,
    LATTE__SYMB_19 = 278,
    LATTE__SYMB_20 = 279,
    LATTE__SYMB_21 = 280,
    LATTE__SYMB_22 = 281,
    LATTE__SYMB_23 = 282,
    LATTE__SYMB_24 = 283,
    LATTE__SYMB_25 = 284,
    LATTE__SYMB_26 = 285,
    LATTE__SYMB_27 = 286,
    LATTE__SYMB_28 = 287,
    LATTE__SYMB_29 = 288,
    LATTE__SYMB_30 = 289,
    LATTE__SYMB_31 = 290,
    LATTE__SYMB_32 = 291,
    LATTE__SYMB_33 = 292,
    LATTE__SYMB_34 = 293,
    LATTE__SYMB_35 = 294,
    LATTE__SYMB_36 = 295,
    LATTE__SYMB_37 = 296,
    LATTE__SYMB_38 = 297,
    LATTE__SYMB_39 = 298,
    LATTE__SYMB_40 = 299,
    LATTE__SYMB_41 = 300,
    LATTE__SYMB_42 = 301,
    _STRING_ = 302,
    _INTEGER_ = 303,
    _IDENT_ = 304
  };
#endif

/* Value type.  */
#if ! defined LATTEYYSTYPE && ! defined LATTEYYSTYPE_IS_DECLARED

union LATTEYYSTYPE
{
#line 958 "Latte.y" /* yacc.c:353  */

  int int_;
  char char_;
  double double_;
  char* string_;
  Latte::IdentP* identp_;
  Latte::Program* program_;
  Latte::ListTopDef* listtopdef_;
  Latte::TopDef* topdef_;
  Latte::FunDef* fundef_;
  Latte::Arg* arg_;
  Latte::ListArg* listarg_;
  Latte::ClassDef* classdef_;
  Latte::ClassBlock* classblock_;
  Latte::ClassBlockDef* classblockdef_;
  Latte::ClsFldDefItem* clsflddefitem_;
  Latte::ListClsFldDefItem* listclsflddefitem_;
  Latte::ListClassBlockDef* listclassblockdef_;
  Latte::Block* block_;
  Latte::ListStmt* liststmt_;
  Latte::Stmt* stmt_;
  Latte::DeclItem* declitem_;
  Latte::ListDeclItem* listdeclitem_;
  Latte::ClassType* classtype_;
  Latte::BasicType* basictype_;
  Latte::NonVoidType* nonvoidtype_;
  Latte::Type* type_;
  Latte::CastOn* caston_;
  Latte::Expr* expr_;
  Latte::ListExpr* listexpr_;
  Latte::AddOp* addop_;
  Latte::MulOp* mulop_;
  Latte::RelOp* relop_;

#line 1166 "Parser.C" /* yacc.c:353  */
};

typedef union LATTEYYSTYPE LATTEYYSTYPE;
# define LATTEYYSTYPE_IS_TRIVIAL 1
# define LATTEYYSTYPE_IS_DECLARED 1
#endif


extern LATTEYYSTYPE Latteyylval;

int Latteyyparse (void);





#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined LATTEYYSTYPE_IS_TRIVIAL && LATTEYYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  18
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   224

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  50
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  35
/* YYNRULES -- Number of rules.  */
#define YYNRULES  95
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  167

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   304

#define YYTRANSLATE(YYX)                                                \
  ((unsigned) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49
};

#if LATTEYYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,  1079,  1079,  1081,  1083,  1084,  1086,  1087,  1089,  1091,
    1093,  1094,  1095,  1097,  1098,  1100,  1102,  1103,  1105,  1107,
    1108,  1110,  1111,  1113,  1115,  1116,  1118,  1119,  1120,  1121,
    1122,  1123,  1124,  1125,  1126,  1127,  1128,  1129,  1130,  1132,
    1133,  1135,  1136,  1138,  1140,  1141,  1142,  1144,  1145,  1147,
    1148,  1149,  1151,  1152,  1153,  1155,  1156,  1157,  1158,  1159,
    1160,  1161,  1162,  1163,  1164,  1165,  1166,  1167,  1169,  1170,
    1171,  1172,  1174,  1175,  1177,  1178,  1180,  1181,  1183,  1184,
    1186,  1187,  1189,  1190,  1191,  1193,  1194,  1196,  1197,  1198,
    1200,  1201,  1202,  1203,  1204,  1205
};
#endif

#if LATTEYYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "_ERROR_", "LATTE__SYMB_0",
  "LATTE__SYMB_1", "LATTE__SYMB_2", "LATTE__SYMB_3", "LATTE__SYMB_4",
  "LATTE__SYMB_5", "LATTE__SYMB_6", "LATTE__SYMB_7", "LATTE__SYMB_8",
  "LATTE__SYMB_9", "LATTE__SYMB_10", "LATTE__SYMB_11", "LATTE__SYMB_12",
  "LATTE__SYMB_13", "LATTE__SYMB_14", "LATTE__SYMB_15", "LATTE__SYMB_16",
  "LATTE__SYMB_17", "LATTE__SYMB_18", "LATTE__SYMB_19", "LATTE__SYMB_20",
  "LATTE__SYMB_21", "LATTE__SYMB_22", "LATTE__SYMB_23", "LATTE__SYMB_24",
  "LATTE__SYMB_25", "LATTE__SYMB_26", "LATTE__SYMB_27", "LATTE__SYMB_28",
  "LATTE__SYMB_29", "LATTE__SYMB_30", "LATTE__SYMB_31", "LATTE__SYMB_32",
  "LATTE__SYMB_33", "LATTE__SYMB_34", "LATTE__SYMB_35", "LATTE__SYMB_36",
  "LATTE__SYMB_37", "LATTE__SYMB_38", "LATTE__SYMB_39", "LATTE__SYMB_40",
  "LATTE__SYMB_41", "LATTE__SYMB_42", "_STRING_", "_INTEGER_", "_IDENT_",
  "$accept", "IdentP", "Program", "ListTopDef", "TopDef", "FunDef", "Arg",
  "ListArg", "ClassDef", "ClassBlock", "ClassBlockDef", "ClsFldDefItem",
  "ListClsFldDefItem", "ListClassBlockDef", "Block", "ListStmt", "Stmt",
  "DeclItem", "ListDeclItem", "ClassType", "BasicType", "NonVoidType",
  "Type", "CastOn", "Expr6", "Expr5", "Expr4", "Expr3", "Expr2", "Expr1",
  "Expr", "ListExpr", "AddOp", "MulOp", "RelOp", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304
};
# endif

#define YYPACT_NINF -140

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-140)))

#define YYTABLE_NINF -44

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      51,  -140,   -48,  -140,  -140,  -140,  -140,     9,  -140,    51,
    -140,  -140,  -140,  -140,     0,   -48,  -140,     8,  -140,  -140,
    -140,    35,    27,   -16,  -140,    27,  -140,    27,    42,   -48,
      48,    50,    53,   -48,  -140,  -140,    35,    58,    62,  -140,
      27,    66,  -140,   -48,  -140,  -140,  -140,  -140,  -140,  -140,
      73,   165,  -140,  -140,   175,   175,  -140,    71,    82,    46,
    -140,    13,  -140,    84,  -140,  -140,    -1,  -140,  -140,    49,
     175,    21,  -140,    44,    29,   104,    72,   130,    14,    94,
      87,    98,   102,  -140,  -140,    27,   175,    92,    93,  -140,
     116,   175,   175,   117,   123,   127,  -140,   175,    95,  -140,
    -140,  -140,   175,  -140,  -140,   175,   175,  -140,  -140,  -140,
    -140,  -140,  -140,   175,   175,  -140,   175,   134,   136,  -140,
    -140,   141,  -140,    99,   142,   175,  -140,   144,   146,   145,
     175,    49,  -140,   137,   150,  -140,    44,  -140,    29,  -140,
     161,  -140,  -140,  -140,   158,   119,   156,   119,   175,  -140,
    -140,  -140,  -140,   175,  -140,   175,   139,  -140,  -140,  -140,
     169,   170,   119,  -140,   119,  -140,  -140
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    46,     0,    44,    45,    51,    43,     0,     3,     4,
       7,     6,    47,    48,    50,     0,     2,     0,     1,     5,
      49,     0,     0,     0,    13,    10,    16,    21,     0,     0,
       0,    11,     0,     0,    22,    15,    18,    19,     0,    14,
      10,     0,     9,     0,    17,    12,    24,     8,    18,    20,
       0,     0,    23,    37,     0,     0,    59,     0,     0,     0,
      62,     0,    58,     0,    60,    57,    61,    36,    25,     0,
       0,    71,    73,    75,    77,    79,    81,     0,    61,    48,
       0,     0,    61,    69,    70,     0,     0,    56,     0,    31,
       0,     0,    82,    39,    41,     0,    68,     0,     0,    87,
      88,    89,     0,    86,    85,     0,     0,    90,    91,    92,
      93,    94,    95,     0,     0,    38,     0,     0,     0,    52,
      54,     0,    67,     0,     0,     0,    30,     0,    83,     0,
       0,     0,    26,     0,    63,    72,    74,    78,    76,    80,
       0,    28,    29,    53,     0,     0,     0,     0,    82,    64,
      40,    42,    66,    82,    27,     0,    34,    55,    32,    84,
       0,     0,     0,    65,     0,    35,    33
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -140,     1,  -140,   167,  -140,   -15,  -140,   138,  -140,   147,
    -140,  -140,   143,   153,   140,  -140,  -139,  -140,    54,  -140,
     131,   -14,     2,  -140,  -140,   -50,    83,    74,  -140,    85,
     -51,  -127,  -140,  -140,  -140
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    17,     7,     8,     9,    10,    31,    32,    11,    24,
      27,    37,    38,    28,    67,    50,    68,    94,    95,    12,
      13,    14,    69,    70,    71,    72,    73,    74,    75,    76,
      77,   129,   105,   102,   113
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      81,    16,    15,    92,    83,    84,   156,    26,   158,    18,
      90,    15,    26,   -43,    20,    22,    21,    51,    92,   119,
      96,   159,    89,   165,    29,   166,   160,    33,   -43,    29,
      36,    54,    55,    30,    42,   124,    97,    80,    98,    25,
     127,   128,    33,    23,    48,    88,   133,   103,   -43,    56,
      35,   104,   135,    59,    60,    22,    40,    62,    41,     1,
      64,    65,    82,   139,    43,   140,     3,    99,   100,   101,
       4,    44,     5,    46,   146,    85,     6,    51,     1,   150,
      46,    52,    53,     1,     2,     3,    86,   123,    91,     4,
       3,    54,    55,   114,     4,    87,     5,   128,    93,   120,
       6,   121,   128,   122,   161,     1,    92,   -43,   125,    56,
      57,    58,     3,    59,    60,    61,     4,    62,     5,    63,
      64,    65,    66,    51,   106,   126,    46,   130,    53,   131,
     107,   108,   109,   110,   111,   112,   132,    54,    55,   115,
     116,   117,   118,   141,   134,   142,   143,   145,   144,   147,
     149,     1,   148,   152,   153,    56,    57,    58,     3,    59,
      60,    61,     4,    62,     5,    63,    64,    65,    66,    51,
     154,   155,   157,   162,   163,   164,    19,    39,    45,    51,
      34,    47,    79,    54,    55,   151,    49,   138,   136,     0,
       0,   137,     0,    54,    55,     0,     0,     1,     0,     0,
       0,    56,     0,     0,     3,    59,    60,     0,     4,    62,
       0,    56,    64,    65,    78,    59,    60,     0,     0,    62,
       0,     0,    64,    65,    82
};

static const yytype_int16 yycheck[] =
{
      51,    49,     0,     4,    54,    55,   145,    22,   147,     0,
      61,     9,    27,    14,    14,     7,    15,     4,     4,     5,
      70,   148,     9,   162,    22,   164,   153,    25,    14,    27,
      29,    18,    19,    49,    33,    86,    15,    51,    17,     4,
      91,    92,    40,    35,    43,    59,    97,    18,    49,    36,
       8,    22,   102,    40,    41,     7,     6,    44,     5,    32,
      47,    48,    49,   114,     6,   116,    39,    23,    24,    25,
      43,     9,    45,     7,   125,     4,    49,     4,    32,   130,
       7,     8,     9,    32,    33,    39,     4,    85,     4,    43,
      39,    18,    19,    21,    43,    49,    45,   148,    49,     5,
      49,    14,   153,     5,   155,    32,     4,    15,    15,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,     4,    20,     9,     7,    10,     9,     6,
      26,    27,    28,    29,    30,    31,     9,    18,    19,     9,
      10,    11,    12,     9,    49,     9,     5,     5,    49,     5,
       5,    32,     6,    16,     4,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,     4,
       9,    13,    16,    34,     5,     5,     9,    30,    40,     4,
      27,    41,    51,    18,    19,   131,    43,   113,   105,    -1,
      -1,   106,    -1,    18,    19,    -1,    -1,    32,    -1,    -1,
      -1,    36,    -1,    -1,    39,    40,    41,    -1,    43,    44,
      -1,    36,    47,    48,    49,    40,    41,    -1,    -1,    44,
      -1,    -1,    47,    48,    49
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    32,    33,    39,    43,    45,    49,    52,    53,    54,
      55,    58,    69,    70,    71,    72,    49,    51,     0,    53,
      14,    51,     7,    35,    59,     4,    55,    60,    63,    72,
      49,    56,    57,    72,    63,     8,    51,    61,    62,    59,
       6,     5,    51,     6,     9,    57,     7,    64,    51,    62,
      65,     4,     8,     9,    18,    19,    36,    37,    38,    40,
      41,    42,    44,    46,    47,    48,    49,    64,    66,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    49,    70,
      71,    80,    49,    75,    75,     4,     4,    49,    71,     9,
      80,     4,     4,    49,    67,    68,    75,    15,    17,    23,
      24,    25,    83,    18,    22,    82,    20,    26,    27,    28,
      29,    30,    31,    84,    21,     9,    10,    11,    12,     5,
       5,    14,     5,    72,    80,    15,     9,    80,    80,    81,
      10,     6,     9,    80,    49,    75,    76,    79,    77,    80,
      80,     9,     9,     5,    49,     5,    80,     5,     6,     5,
      80,    68,    16,     4,     9,    13,    66,    16,    66,    81,
      81,    80,    34,     5,     5,    66,    66
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    50,    51,    52,    53,    53,    54,    54,    55,    56,
      57,    57,    57,    58,    58,    59,    60,    60,    61,    62,
      62,    63,    63,    64,    65,    65,    66,    66,    66,    66,
      66,    66,    66,    66,    66,    66,    66,    66,    66,    67,
      67,    68,    68,    69,    70,    70,    70,    71,    71,    72,
      72,    72,    73,    73,    73,    74,    74,    74,    74,    74,
      74,    74,    74,    74,    74,    74,    74,    74,    75,    75,
      75,    75,    76,    76,    77,    77,    78,    78,    79,    79,
      80,    80,    81,    81,    81,    82,    82,    83,    83,    83,
      84,    84,    84,    84,    84,    84
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     1,     2,     1,     1,     6,     2,
       0,     1,     3,     3,     5,     3,     1,     3,     1,     1,
       3,     1,     2,     3,     0,     2,     3,     4,     3,     3,
       3,     2,     5,     8,     5,     7,     1,     1,     2,     1,
       3,     1,     3,     1,     1,     1,     1,     1,     1,     2,
       1,     1,     3,     4,     3,     5,     2,     1,     1,     1,
       1,     1,     1,     3,     4,     6,     4,     3,     2,     2,
       2,     1,     3,     1,     3,     1,     3,     1,     3,     1,
       3,     1,     0,     1,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if LATTEYYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !LATTEYYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !LATTEYYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return (YYSIZE_T) (yystpcpy (yyres, yystr) - yyres);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = (yytype_int16) yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = (YYSIZE_T) (yyssp - yyss + 1);

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 1079 "Latte.y" /* yacc.c:1645  */
    {  (yyval.identp_) = new Latte::IdentPos((yyvsp[0].string_)); (yyval.identp_)->line_number = Latteyy_mylinenumber;  }
#line 2387 "Parser.C" /* yacc.c:1645  */
    break;

  case 3:
#line 1081 "Latte.y" /* yacc.c:1645  */
    {  std::reverse((yyvsp[0].listtopdef_)->begin(),(yyvsp[0].listtopdef_)->end()) ;(yyval.program_) = new Latte::Prog((yyvsp[0].listtopdef_)); (yyval.program_)->line_number = Latteyy_mylinenumber; Latte::YY_RESULT_Program_= (yyval.program_); }
#line 2393 "Parser.C" /* yacc.c:1645  */
    break;

  case 4:
#line 1083 "Latte.y" /* yacc.c:1645  */
    {  (yyval.listtopdef_) = new Latte::ListTopDef() ; (yyval.listtopdef_)->push_back((yyvsp[0].topdef_));  }
#line 2399 "Parser.C" /* yacc.c:1645  */
    break;

  case 5:
#line 1084 "Latte.y" /* yacc.c:1645  */
    {  (yyvsp[0].listtopdef_)->push_back((yyvsp[-1].topdef_)) ; (yyval.listtopdef_) = (yyvsp[0].listtopdef_) ;  }
#line 2405 "Parser.C" /* yacc.c:1645  */
    break;

  case 6:
#line 1086 "Latte.y" /* yacc.c:1645  */
    {  (yyval.topdef_) = new Latte::ClsDefTop((yyvsp[0].classdef_)); (yyval.topdef_)->line_number = Latteyy_mylinenumber;  }
#line 2411 "Parser.C" /* yacc.c:1645  */
    break;

  case 7:
#line 1087 "Latte.y" /* yacc.c:1645  */
    {  (yyval.topdef_) = new Latte::FnDefTop((yyvsp[0].fundef_)); (yyval.topdef_)->line_number = Latteyy_mylinenumber;  }
#line 2417 "Parser.C" /* yacc.c:1645  */
    break;

  case 8:
#line 1089 "Latte.y" /* yacc.c:1645  */
    {  std::reverse((yyvsp[-2].listarg_)->begin(),(yyvsp[-2].listarg_)->end()) ;(yyval.fundef_) = new Latte::FnDef((yyvsp[-5].type_), (yyvsp[-4].identp_), (yyvsp[-2].listarg_), (yyvsp[0].block_)); (yyval.fundef_)->line_number = Latteyy_mylinenumber;  }
#line 2423 "Parser.C" /* yacc.c:1645  */
    break;

  case 9:
#line 1091 "Latte.y" /* yacc.c:1645  */
    {  (yyval.arg_) = new Latte::Ar((yyvsp[-1].type_), (yyvsp[0].identp_)); (yyval.arg_)->line_number = Latteyy_mylinenumber;  }
#line 2429 "Parser.C" /* yacc.c:1645  */
    break;

  case 10:
#line 1093 "Latte.y" /* yacc.c:1645  */
    {  (yyval.listarg_) = new Latte::ListArg();  }
#line 2435 "Parser.C" /* yacc.c:1645  */
    break;

  case 11:
#line 1094 "Latte.y" /* yacc.c:1645  */
    {  (yyval.listarg_) = new Latte::ListArg() ; (yyval.listarg_)->push_back((yyvsp[0].arg_));  }
#line 2441 "Parser.C" /* yacc.c:1645  */
    break;

  case 12:
#line 1095 "Latte.y" /* yacc.c:1645  */
    {  (yyvsp[0].listarg_)->push_back((yyvsp[-2].arg_)) ; (yyval.listarg_) = (yyvsp[0].listarg_) ;  }
#line 2447 "Parser.C" /* yacc.c:1645  */
    break;

  case 13:
#line 1097 "Latte.y" /* yacc.c:1645  */
    {  (yyval.classdef_) = new Latte::ClsDef((yyvsp[-1].identp_), (yyvsp[0].classblock_)); (yyval.classdef_)->line_number = Latteyy_mylinenumber;  }
#line 2453 "Parser.C" /* yacc.c:1645  */
    break;

  case 14:
#line 1098 "Latte.y" /* yacc.c:1645  */
    {  (yyval.classdef_) = new Latte::ClsExtDef((yyvsp[-3].identp_), (yyvsp[-1].string_), (yyvsp[0].classblock_)); (yyval.classdef_)->line_number = Latteyy_mylinenumber;  }
#line 2459 "Parser.C" /* yacc.c:1645  */
    break;

  case 15:
#line 1100 "Latte.y" /* yacc.c:1645  */
    {  std::reverse((yyvsp[-1].listclassblockdef_)->begin(),(yyvsp[-1].listclassblockdef_)->end()) ;(yyval.classblock_) = new Latte::ClsBlk((yyvsp[-1].listclassblockdef_)); (yyval.classblock_)->line_number = Latteyy_mylinenumber;  }
#line 2465 "Parser.C" /* yacc.c:1645  */
    break;

  case 16:
#line 1102 "Latte.y" /* yacc.c:1645  */
    {  (yyval.classblockdef_) = new Latte::ClsMthDef((yyvsp[0].fundef_)); (yyval.classblockdef_)->line_number = Latteyy_mylinenumber;  }
#line 2471 "Parser.C" /* yacc.c:1645  */
    break;

  case 17:
#line 1103 "Latte.y" /* yacc.c:1645  */
    {  std::reverse((yyvsp[-1].listclsflddefitem_)->begin(),(yyvsp[-1].listclsflddefitem_)->end()) ;(yyval.classblockdef_) = new Latte::ClsFldDef((yyvsp[-2].type_), (yyvsp[-1].listclsflddefitem_)); (yyval.classblockdef_)->line_number = Latteyy_mylinenumber;  }
#line 2477 "Parser.C" /* yacc.c:1645  */
    break;

  case 18:
#line 1105 "Latte.y" /* yacc.c:1645  */
    {  (yyval.clsflddefitem_) = new Latte::ClsFldDefIt((yyvsp[0].identp_)); (yyval.clsflddefitem_)->line_number = Latteyy_mylinenumber;  }
#line 2483 "Parser.C" /* yacc.c:1645  */
    break;

  case 19:
#line 1107 "Latte.y" /* yacc.c:1645  */
    {  (yyval.listclsflddefitem_) = new Latte::ListClsFldDefItem() ; (yyval.listclsflddefitem_)->push_back((yyvsp[0].clsflddefitem_));  }
#line 2489 "Parser.C" /* yacc.c:1645  */
    break;

  case 20:
#line 1108 "Latte.y" /* yacc.c:1645  */
    {  (yyvsp[0].listclsflddefitem_)->push_back((yyvsp[-2].clsflddefitem_)) ; (yyval.listclsflddefitem_) = (yyvsp[0].listclsflddefitem_) ;  }
#line 2495 "Parser.C" /* yacc.c:1645  */
    break;

  case 21:
#line 1110 "Latte.y" /* yacc.c:1645  */
    {  (yyval.listclassblockdef_) = new Latte::ListClassBlockDef() ; (yyval.listclassblockdef_)->push_back((yyvsp[0].classblockdef_));  }
#line 2501 "Parser.C" /* yacc.c:1645  */
    break;

  case 22:
#line 1111 "Latte.y" /* yacc.c:1645  */
    {  (yyvsp[0].listclassblockdef_)->push_back((yyvsp[-1].classblockdef_)) ; (yyval.listclassblockdef_) = (yyvsp[0].listclassblockdef_) ;  }
#line 2507 "Parser.C" /* yacc.c:1645  */
    break;

  case 23:
#line 1113 "Latte.y" /* yacc.c:1645  */
    {  (yyval.block_) = new Latte::Blk((yyvsp[-1].liststmt_)); (yyval.block_)->line_number = Latteyy_mylinenumber;  }
#line 2513 "Parser.C" /* yacc.c:1645  */
    break;

  case 24:
#line 1115 "Latte.y" /* yacc.c:1645  */
    {  (yyval.liststmt_) = new Latte::ListStmt();  }
#line 2519 "Parser.C" /* yacc.c:1645  */
    break;

  case 25:
#line 1116 "Latte.y" /* yacc.c:1645  */
    {  (yyvsp[-1].liststmt_)->push_back((yyvsp[0].stmt_)) ; (yyval.liststmt_) = (yyvsp[-1].liststmt_) ;  }
#line 2525 "Parser.C" /* yacc.c:1645  */
    break;

  case 26:
#line 1118 "Latte.y" /* yacc.c:1645  */
    {  std::reverse((yyvsp[-1].listdeclitem_)->begin(),(yyvsp[-1].listdeclitem_)->end()) ;(yyval.stmt_) = new Latte::Decl((yyvsp[-2].type_), (yyvsp[-1].listdeclitem_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2531 "Parser.C" /* yacc.c:1645  */
    break;

  case 27:
#line 1119 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::Assign((yyvsp[-3].expr_), (yyvsp[-1].expr_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2537 "Parser.C" /* yacc.c:1645  */
    break;

  case 28:
#line 1120 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::Incr((yyvsp[-2].expr_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2543 "Parser.C" /* yacc.c:1645  */
    break;

  case 29:
#line 1121 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::Decr((yyvsp[-2].expr_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2549 "Parser.C" /* yacc.c:1645  */
    break;

  case 30:
#line 1122 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::Ret((yyvsp[-1].expr_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2555 "Parser.C" /* yacc.c:1645  */
    break;

  case 31:
#line 1123 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::VRet(); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2561 "Parser.C" /* yacc.c:1645  */
    break;

  case 32:
#line 1124 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::While((yyvsp[-2].expr_), (yyvsp[0].stmt_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2567 "Parser.C" /* yacc.c:1645  */
    break;

  case 33:
#line 1125 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::For((yyvsp[-5].type_), (yyvsp[-4].string_), (yyvsp[-2].expr_), (yyvsp[0].stmt_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2573 "Parser.C" /* yacc.c:1645  */
    break;

  case 34:
#line 1126 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::If((yyvsp[-2].expr_), (yyvsp[0].stmt_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2579 "Parser.C" /* yacc.c:1645  */
    break;

  case 35:
#line 1127 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::IfElse((yyvsp[-4].expr_), (yyvsp[-2].stmt_), (yyvsp[0].stmt_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2585 "Parser.C" /* yacc.c:1645  */
    break;

  case 36:
#line 1128 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::BStmt((yyvsp[0].block_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2591 "Parser.C" /* yacc.c:1645  */
    break;

  case 37:
#line 1129 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::Empty(); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2597 "Parser.C" /* yacc.c:1645  */
    break;

  case 38:
#line 1130 "Latte.y" /* yacc.c:1645  */
    {  (yyval.stmt_) = new Latte::Exp((yyvsp[-1].expr_)); (yyval.stmt_)->line_number = Latteyy_mylinenumber;  }
#line 2603 "Parser.C" /* yacc.c:1645  */
    break;

  case 39:
#line 1132 "Latte.y" /* yacc.c:1645  */
    {  (yyval.declitem_) = new Latte::NoInit((yyvsp[0].string_)); (yyval.declitem_)->line_number = Latteyy_mylinenumber;  }
#line 2609 "Parser.C" /* yacc.c:1645  */
    break;

  case 40:
#line 1133 "Latte.y" /* yacc.c:1645  */
    {  (yyval.declitem_) = new Latte::Init((yyvsp[-2].string_), (yyvsp[0].expr_)); (yyval.declitem_)->line_number = Latteyy_mylinenumber;  }
#line 2615 "Parser.C" /* yacc.c:1645  */
    break;

  case 41:
#line 1135 "Latte.y" /* yacc.c:1645  */
    {  (yyval.listdeclitem_) = new Latte::ListDeclItem() ; (yyval.listdeclitem_)->push_back((yyvsp[0].declitem_));  }
#line 2621 "Parser.C" /* yacc.c:1645  */
    break;

  case 42:
#line 1136 "Latte.y" /* yacc.c:1645  */
    {  (yyvsp[0].listdeclitem_)->push_back((yyvsp[-2].declitem_)) ; (yyval.listdeclitem_) = (yyvsp[0].listdeclitem_) ;  }
#line 2627 "Parser.C" /* yacc.c:1645  */
    break;

  case 43:
#line 1138 "Latte.y" /* yacc.c:1645  */
    {  (yyval.classtype_) = new Latte::TClass((yyvsp[0].string_)); (yyval.classtype_)->line_number = Latteyy_mylinenumber;  }
#line 2633 "Parser.C" /* yacc.c:1645  */
    break;

  case 44:
#line 1140 "Latte.y" /* yacc.c:1645  */
    {  (yyval.basictype_) = new Latte::TInt(); (yyval.basictype_)->line_number = Latteyy_mylinenumber;  }
#line 2639 "Parser.C" /* yacc.c:1645  */
    break;

  case 45:
#line 1141 "Latte.y" /* yacc.c:1645  */
    {  (yyval.basictype_) = new Latte::TStr(); (yyval.basictype_)->line_number = Latteyy_mylinenumber;  }
#line 2645 "Parser.C" /* yacc.c:1645  */
    break;

  case 46:
#line 1142 "Latte.y" /* yacc.c:1645  */
    {  (yyval.basictype_) = new Latte::TBool(); (yyval.basictype_)->line_number = Latteyy_mylinenumber;  }
#line 2651 "Parser.C" /* yacc.c:1645  */
    break;

  case 47:
#line 1144 "Latte.y" /* yacc.c:1645  */
    {  (yyval.nonvoidtype_) = new Latte::TNVC((yyvsp[0].classtype_)); (yyval.nonvoidtype_)->line_number = Latteyy_mylinenumber;  }
#line 2657 "Parser.C" /* yacc.c:1645  */
    break;

  case 48:
#line 1145 "Latte.y" /* yacc.c:1645  */
    {  (yyval.nonvoidtype_) = new Latte::TNVB((yyvsp[0].basictype_)); (yyval.nonvoidtype_)->line_number = Latteyy_mylinenumber;  }
#line 2663 "Parser.C" /* yacc.c:1645  */
    break;

  case 49:
#line 1147 "Latte.y" /* yacc.c:1645  */
    {  (yyval.type_) = new Latte::TArray((yyvsp[-1].nonvoidtype_)); (yyval.type_)->line_number = Latteyy_mylinenumber;  }
#line 2669 "Parser.C" /* yacc.c:1645  */
    break;

  case 50:
#line 1148 "Latte.y" /* yacc.c:1645  */
    {  (yyval.type_) = new Latte::TSingle((yyvsp[0].nonvoidtype_)); (yyval.type_)->line_number = Latteyy_mylinenumber;  }
#line 2675 "Parser.C" /* yacc.c:1645  */
    break;

  case 51:
#line 1149 "Latte.y" /* yacc.c:1645  */
    {  (yyval.type_) = new Latte::TVoid(); (yyval.type_)->line_number = Latteyy_mylinenumber;  }
#line 2681 "Parser.C" /* yacc.c:1645  */
    break;

  case 52:
#line 1151 "Latte.y" /* yacc.c:1645  */
    {  (yyval.caston_) = new Latte::CastOnClass((yyvsp[-1].string_)); (yyval.caston_)->line_number = Latteyy_mylinenumber;  }
#line 2687 "Parser.C" /* yacc.c:1645  */
    break;

  case 53:
#line 1152 "Latte.y" /* yacc.c:1645  */
    {  (yyval.caston_) = new Latte::CastOnArr((yyvsp[-2].nonvoidtype_)); (yyval.caston_)->line_number = Latteyy_mylinenumber;  }
#line 2693 "Parser.C" /* yacc.c:1645  */
    break;

  case 54:
#line 1153 "Latte.y" /* yacc.c:1645  */
    {  (yyval.caston_) = new Latte::CastOnBasic((yyvsp[-1].basictype_)); (yyval.caston_)->line_number = Latteyy_mylinenumber;  }
#line 2699 "Parser.C" /* yacc.c:1645  */
    break;

  case 55:
#line 1155 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::ENewArr((yyvsp[-3].nonvoidtype_), (yyvsp[-1].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2705 "Parser.C" /* yacc.c:1645  */
    break;

  case 56:
#line 1156 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::ENewCls((yyvsp[0].string_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2711 "Parser.C" /* yacc.c:1645  */
    break;

  case 57:
#line 1157 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::ELitInt((yyvsp[0].int_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2717 "Parser.C" /* yacc.c:1645  */
    break;

  case 58:
#line 1158 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::ELitTrue(); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2723 "Parser.C" /* yacc.c:1645  */
    break;

  case 59:
#line 1159 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::ELitFalse(); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2729 "Parser.C" /* yacc.c:1645  */
    break;

  case 60:
#line 1160 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::EString((yyvsp[0].string_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2735 "Parser.C" /* yacc.c:1645  */
    break;

  case 61:
#line 1161 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::EVar((yyvsp[0].string_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2741 "Parser.C" /* yacc.c:1645  */
    break;

  case 62:
#line 1162 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::ENull(); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2747 "Parser.C" /* yacc.c:1645  */
    break;

  case 63:
#line 1163 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::EMemb((yyvsp[-2].expr_), (yyvsp[0].string_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2753 "Parser.C" /* yacc.c:1645  */
    break;

  case 64:
#line 1164 "Latte.y" /* yacc.c:1645  */
    {  std::reverse((yyvsp[-1].listexpr_)->begin(),(yyvsp[-1].listexpr_)->end()) ;(yyval.expr_) = new Latte::EFunCall((yyvsp[-3].string_), (yyvsp[-1].listexpr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2759 "Parser.C" /* yacc.c:1645  */
    break;

  case 65:
#line 1165 "Latte.y" /* yacc.c:1645  */
    {  std::reverse((yyvsp[-1].listexpr_)->begin(),(yyvsp[-1].listexpr_)->end()) ;(yyval.expr_) = new Latte::EMembCall((yyvsp[-5].expr_), (yyvsp[-3].string_), (yyvsp[-1].listexpr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2765 "Parser.C" /* yacc.c:1645  */
    break;

  case 66:
#line 1166 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::EAt((yyvsp[-3].expr_), (yyvsp[-1].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2771 "Parser.C" /* yacc.c:1645  */
    break;

  case 67:
#line 1167 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = (yyvsp[-1].expr_);  }
#line 2777 "Parser.C" /* yacc.c:1645  */
    break;

  case 68:
#line 1169 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::ECast((yyvsp[-1].caston_), (yyvsp[0].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2783 "Parser.C" /* yacc.c:1645  */
    break;

  case 69:
#line 1170 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::Neg((yyvsp[0].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2789 "Parser.C" /* yacc.c:1645  */
    break;

  case 70:
#line 1171 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::Not((yyvsp[0].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2795 "Parser.C" /* yacc.c:1645  */
    break;

  case 71:
#line 1172 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = (yyvsp[0].expr_);  }
#line 2801 "Parser.C" /* yacc.c:1645  */
    break;

  case 72:
#line 1174 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::EMul((yyvsp[-2].expr_), (yyvsp[-1].mulop_), (yyvsp[0].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2807 "Parser.C" /* yacc.c:1645  */
    break;

  case 73:
#line 1175 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = (yyvsp[0].expr_);  }
#line 2813 "Parser.C" /* yacc.c:1645  */
    break;

  case 74:
#line 1177 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::EAdd((yyvsp[-2].expr_), (yyvsp[-1].addop_), (yyvsp[0].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2819 "Parser.C" /* yacc.c:1645  */
    break;

  case 75:
#line 1178 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = (yyvsp[0].expr_);  }
#line 2825 "Parser.C" /* yacc.c:1645  */
    break;

  case 76:
#line 1180 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::ERel((yyvsp[-2].expr_), (yyvsp[-1].relop_), (yyvsp[0].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2831 "Parser.C" /* yacc.c:1645  */
    break;

  case 77:
#line 1181 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = (yyvsp[0].expr_);  }
#line 2837 "Parser.C" /* yacc.c:1645  */
    break;

  case 78:
#line 1183 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::EAnd((yyvsp[-2].expr_), (yyvsp[0].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2843 "Parser.C" /* yacc.c:1645  */
    break;

  case 79:
#line 1184 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = (yyvsp[0].expr_);  }
#line 2849 "Parser.C" /* yacc.c:1645  */
    break;

  case 80:
#line 1186 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = new Latte::EOr((yyvsp[-2].expr_), (yyvsp[0].expr_)); (yyval.expr_)->line_number = Latteyy_mylinenumber;  }
#line 2855 "Parser.C" /* yacc.c:1645  */
    break;

  case 81:
#line 1187 "Latte.y" /* yacc.c:1645  */
    {  (yyval.expr_) = (yyvsp[0].expr_);  }
#line 2861 "Parser.C" /* yacc.c:1645  */
    break;

  case 82:
#line 1189 "Latte.y" /* yacc.c:1645  */
    {  (yyval.listexpr_) = new Latte::ListExpr();  }
#line 2867 "Parser.C" /* yacc.c:1645  */
    break;

  case 83:
#line 1190 "Latte.y" /* yacc.c:1645  */
    {  (yyval.listexpr_) = new Latte::ListExpr() ; (yyval.listexpr_)->push_back((yyvsp[0].expr_));  }
#line 2873 "Parser.C" /* yacc.c:1645  */
    break;

  case 84:
#line 1191 "Latte.y" /* yacc.c:1645  */
    {  (yyvsp[0].listexpr_)->push_back((yyvsp[-2].expr_)) ; (yyval.listexpr_) = (yyvsp[0].listexpr_) ;  }
#line 2879 "Parser.C" /* yacc.c:1645  */
    break;

  case 85:
#line 1193 "Latte.y" /* yacc.c:1645  */
    {  (yyval.addop_) = new Latte::Plus(); (yyval.addop_)->line_number = Latteyy_mylinenumber;  }
#line 2885 "Parser.C" /* yacc.c:1645  */
    break;

  case 86:
#line 1194 "Latte.y" /* yacc.c:1645  */
    {  (yyval.addop_) = new Latte::Minus(); (yyval.addop_)->line_number = Latteyy_mylinenumber;  }
#line 2891 "Parser.C" /* yacc.c:1645  */
    break;

  case 87:
#line 1196 "Latte.y" /* yacc.c:1645  */
    {  (yyval.mulop_) = new Latte::Times(); (yyval.mulop_)->line_number = Latteyy_mylinenumber;  }
#line 2897 "Parser.C" /* yacc.c:1645  */
    break;

  case 88:
#line 1197 "Latte.y" /* yacc.c:1645  */
    {  (yyval.mulop_) = new Latte::Div(); (yyval.mulop_)->line_number = Latteyy_mylinenumber;  }
#line 2903 "Parser.C" /* yacc.c:1645  */
    break;

  case 89:
#line 1198 "Latte.y" /* yacc.c:1645  */
    {  (yyval.mulop_) = new Latte::Mod(); (yyval.mulop_)->line_number = Latteyy_mylinenumber;  }
#line 2909 "Parser.C" /* yacc.c:1645  */
    break;

  case 90:
#line 1200 "Latte.y" /* yacc.c:1645  */
    {  (yyval.relop_) = new Latte::LTH(); (yyval.relop_)->line_number = Latteyy_mylinenumber;  }
#line 2915 "Parser.C" /* yacc.c:1645  */
    break;

  case 91:
#line 1201 "Latte.y" /* yacc.c:1645  */
    {  (yyval.relop_) = new Latte::LE(); (yyval.relop_)->line_number = Latteyy_mylinenumber;  }
#line 2921 "Parser.C" /* yacc.c:1645  */
    break;

  case 92:
#line 1202 "Latte.y" /* yacc.c:1645  */
    {  (yyval.relop_) = new Latte::GTH(); (yyval.relop_)->line_number = Latteyy_mylinenumber;  }
#line 2927 "Parser.C" /* yacc.c:1645  */
    break;

  case 93:
#line 1203 "Latte.y" /* yacc.c:1645  */
    {  (yyval.relop_) = new Latte::GE(); (yyval.relop_)->line_number = Latteyy_mylinenumber;  }
#line 2933 "Parser.C" /* yacc.c:1645  */
    break;

  case 94:
#line 1204 "Latte.y" /* yacc.c:1645  */
    {  (yyval.relop_) = new Latte::EQU(); (yyval.relop_)->line_number = Latteyy_mylinenumber;  }
#line 2939 "Parser.C" /* yacc.c:1645  */
    break;

  case 95:
#line 1205 "Latte.y" /* yacc.c:1645  */
    {  (yyval.relop_) = new Latte::NE(); (yyval.relop_)->line_number = Latteyy_mylinenumber;  }
#line 2945 "Parser.C" /* yacc.c:1645  */
    break;


#line 2949 "Parser.C" /* yacc.c:1645  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
