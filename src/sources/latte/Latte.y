/* This Bison file was machine-generated by BNFC */
%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include "Absyn.h"
typedef struct yy_buffer_state *YY_BUFFER_STATE;
int yyparse(void);
int yylex(void);
YY_BUFFER_STATE Latteyy_scan_string(const char *str);
void Latteyy_delete_buffer(YY_BUFFER_STATE buf);
int Latteyy_mylinenumber;
int Latteinitialize_lexer(FILE * inp);
int Latteyywrap(void)
{
  return 1;
}
void Latteyyerror(const char *str)
{
  extern char *Latteyytext;
  fprintf(stderr,"error: line %d: %s at %s\n", 
    Latteyy_mylinenumber, str, Latteyytext);
}


namespace Latte
{
static IdentP* YY_RESULT_IdentP_ = 0;
IdentP* pIdentP(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_IdentP_;
  }
}
IdentP* pIdentP(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_IdentP_;
  }
}

static Program* YY_RESULT_Program_ = 0;
Program* pProgram(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Program_;
  }
}
Program* pProgram(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Program_;
  }
}

static ListTopDef* YY_RESULT_ListTopDef_ = 0;
ListTopDef* pListTopDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListTopDef_;
  }
}
ListTopDef* pListTopDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListTopDef_;
  }
}

static TopDef* YY_RESULT_TopDef_ = 0;
TopDef* pTopDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_TopDef_;
  }
}
TopDef* pTopDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_TopDef_;
  }
}

static FunDef* YY_RESULT_FunDef_ = 0;
FunDef* pFunDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_FunDef_;
  }
}
FunDef* pFunDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_FunDef_;
  }
}

static Arg* YY_RESULT_Arg_ = 0;
Arg* pArg(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Arg_;
  }
}
Arg* pArg(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Arg_;
  }
}

static ListArg* YY_RESULT_ListArg_ = 0;
ListArg* pListArg(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListArg_;
  }
}
ListArg* pListArg(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListArg_;
  }
}

static ClassDef* YY_RESULT_ClassDef_ = 0;
ClassDef* pClassDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassDef_;
  }
}
ClassDef* pClassDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassDef_;
  }
}

static ClassBlock* YY_RESULT_ClassBlock_ = 0;
ClassBlock* pClassBlock(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassBlock_;
  }
}
ClassBlock* pClassBlock(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassBlock_;
  }
}

static ClassBlockDef* YY_RESULT_ClassBlockDef_ = 0;
ClassBlockDef* pClassBlockDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassBlockDef_;
  }
}
ClassBlockDef* pClassBlockDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassBlockDef_;
  }
}

static ClsFldDefItem* YY_RESULT_ClsFldDefItem_ = 0;
ClsFldDefItem* pClsFldDefItem(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClsFldDefItem_;
  }
}
ClsFldDefItem* pClsFldDefItem(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClsFldDefItem_;
  }
}

static ListClsFldDefItem* YY_RESULT_ListClsFldDefItem_ = 0;
ListClsFldDefItem* pListClsFldDefItem(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListClsFldDefItem_;
  }
}
ListClsFldDefItem* pListClsFldDefItem(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListClsFldDefItem_;
  }
}

static ListClassBlockDef* YY_RESULT_ListClassBlockDef_ = 0;
ListClassBlockDef* pListClassBlockDef(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListClassBlockDef_;
  }
}
ListClassBlockDef* pListClassBlockDef(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListClassBlockDef_;
  }
}

static Block* YY_RESULT_Block_ = 0;
Block* pBlock(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Block_;
  }
}
Block* pBlock(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Block_;
  }
}

static ListStmt* YY_RESULT_ListStmt_ = 0;
ListStmt* pListStmt(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListStmt_;
  }
}
ListStmt* pListStmt(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListStmt_;
  }
}

static Stmt* YY_RESULT_Stmt_ = 0;
Stmt* pStmt(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Stmt_;
  }
}
Stmt* pStmt(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Stmt_;
  }
}

static DeclItem* YY_RESULT_DeclItem_ = 0;
DeclItem* pDeclItem(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_DeclItem_;
  }
}
DeclItem* pDeclItem(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_DeclItem_;
  }
}

static ListDeclItem* YY_RESULT_ListDeclItem_ = 0;
ListDeclItem* pListDeclItem(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListDeclItem_;
  }
}
ListDeclItem* pListDeclItem(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListDeclItem_;
  }
}

static ClassType* YY_RESULT_ClassType_ = 0;
ClassType* pClassType(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassType_;
  }
}
ClassType* pClassType(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ClassType_;
  }
}

static BasicType* YY_RESULT_BasicType_ = 0;
BasicType* pBasicType(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_BasicType_;
  }
}
BasicType* pBasicType(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_BasicType_;
  }
}

static NonVoidType* YY_RESULT_NonVoidType_ = 0;
NonVoidType* pNonVoidType(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_NonVoidType_;
  }
}
NonVoidType* pNonVoidType(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_NonVoidType_;
  }
}

static Type* YY_RESULT_Type_ = 0;
Type* pType(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Type_;
  }
}
Type* pType(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Type_;
  }
}

static CastOn* YY_RESULT_CastOn_ = 0;
CastOn* pCastOn(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_CastOn_;
  }
}
CastOn* pCastOn(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_CastOn_;
  }
}

static Expr* YY_RESULT_Expr_ = 0;
Expr* pExpr(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Expr_;
  }
}
Expr* pExpr(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_Expr_;
  }
}

static ListExpr* YY_RESULT_ListExpr_ = 0;
ListExpr* pListExpr(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListExpr_;
  }
}
ListExpr* pListExpr(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_ListExpr_;
  }
}

static AddOp* YY_RESULT_AddOp_ = 0;
AddOp* pAddOp(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_AddOp_;
  }
}
AddOp* pAddOp(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_AddOp_;
  }
}

static MulOp* YY_RESULT_MulOp_ = 0;
MulOp* pMulOp(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_MulOp_;
  }
}
MulOp* pMulOp(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_MulOp_;
  }
}

static RelOp* YY_RESULT_RelOp_ = 0;
RelOp* pRelOp(FILE *inp)
{
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(inp);
  if (yyparse())
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_RelOp_;
  }
}
RelOp* pRelOp(const char *str)
{
  YY_BUFFER_STATE buf;
  int result;
  Latteyy_mylinenumber = 1;
  Latteinitialize_lexer(0);
  buf = Latteyy_scan_string(str);
  result = yyparse();
  Latteyy_delete_buffer(buf);
  if (result)
  { /* Failure */
    return 0;
  }
  else
  { /* Success */
    return YY_RESULT_RelOp_;
  }
}


}
%}

%union
{
  int int_;
  char char_;
  double double_;
  char* string_;
  Latte::IdentP* identp_;
  Latte::Program* program_;
  Latte::ListTopDef* listtopdef_;
  Latte::TopDef* topdef_;
  Latte::FunDef* fundef_;
  Latte::Arg* arg_;
  Latte::ListArg* listarg_;
  Latte::ClassDef* classdef_;
  Latte::ClassBlock* classblock_;
  Latte::ClassBlockDef* classblockdef_;
  Latte::ClsFldDefItem* clsflddefitem_;
  Latte::ListClsFldDefItem* listclsflddefitem_;
  Latte::ListClassBlockDef* listclassblockdef_;
  Latte::Block* block_;
  Latte::ListStmt* liststmt_;
  Latte::Stmt* stmt_;
  Latte::DeclItem* declitem_;
  Latte::ListDeclItem* listdeclitem_;
  Latte::ClassType* classtype_;
  Latte::BasicType* basictype_;
  Latte::NonVoidType* nonvoidtype_;
  Latte::Type* type_;
  Latte::CastOn* caston_;
  Latte::Expr* expr_;
  Latte::ListExpr* listexpr_;
  Latte::AddOp* addop_;
  Latte::MulOp* mulop_;
  Latte::RelOp* relop_;
}
%define api.prefix {Latteyy}
%token _ERROR_
%token LATTE__SYMB_0    //   (
%token LATTE__SYMB_1    //   )
%token LATTE__SYMB_2    //   ,
%token LATTE__SYMB_3    //   {
%token LATTE__SYMB_4    //   }
%token LATTE__SYMB_5    //   ;
%token LATTE__SYMB_6    //   =
%token LATTE__SYMB_7    //   ++
%token LATTE__SYMB_8    //   --
%token LATTE__SYMB_9    //   :
%token LATTE__SYMB_10    //   []
%token LATTE__SYMB_11    //   [
%token LATTE__SYMB_12    //   ]
%token LATTE__SYMB_13    //   .
%token LATTE__SYMB_14    //   -
%token LATTE__SYMB_15    //   !
%token LATTE__SYMB_16    //   &&
%token LATTE__SYMB_17    //   ||
%token LATTE__SYMB_18    //   +
%token LATTE__SYMB_19    //   *
%token LATTE__SYMB_20    //   /
%token LATTE__SYMB_21    //   %
%token LATTE__SYMB_22    //   <
%token LATTE__SYMB_23    //   <=
%token LATTE__SYMB_24    //   >
%token LATTE__SYMB_25    //   >=
%token LATTE__SYMB_26    //   ==
%token LATTE__SYMB_27    //   !=
%token LATTE__SYMB_28    //   boolean
%token LATTE__SYMB_29    //   class
%token LATTE__SYMB_30    //   else
%token LATTE__SYMB_31    //   extends
%token LATTE__SYMB_32    //   false
%token LATTE__SYMB_33    //   for
%token LATTE__SYMB_34    //   if
%token LATTE__SYMB_35    //   int
%token LATTE__SYMB_36    //   new
%token LATTE__SYMB_37    //   null
%token LATTE__SYMB_38    //   return
%token LATTE__SYMB_39    //   string
%token LATTE__SYMB_40    //   true
%token LATTE__SYMB_41    //   void
%token LATTE__SYMB_42    //   while

%type <identp_> IdentP
%type <program_> Program
%type <listtopdef_> ListTopDef
%type <topdef_> TopDef
%type <fundef_> FunDef
%type <arg_> Arg
%type <listarg_> ListArg
%type <classdef_> ClassDef
%type <classblock_> ClassBlock
%type <classblockdef_> ClassBlockDef
%type <clsflddefitem_> ClsFldDefItem
%type <listclsflddefitem_> ListClsFldDefItem
%type <listclassblockdef_> ListClassBlockDef
%type <block_> Block
%type <liststmt_> ListStmt
%type <stmt_> Stmt
%type <declitem_> DeclItem
%type <listdeclitem_> ListDeclItem
%type <classtype_> ClassType
%type <basictype_> BasicType
%type <nonvoidtype_> NonVoidType
%type <type_> Type
%type <caston_> CastOn
%type <expr_> Expr6
%type <expr_> Expr5
%type <expr_> Expr4
%type <expr_> Expr3
%type <expr_> Expr2
%type <expr_> Expr1
%type <expr_> Expr
%type <listexpr_> ListExpr
%type <addop_> AddOp
%type <mulop_> MulOp
%type <relop_> RelOp

%start Program
%token<string_> _STRING_
%token<int_> _INTEGER_
%token<string_> _IDENT_

%%
IdentP : _IDENT_ {  $$ = new Latte::IdentPos($1); $$->line_number = Latteyy_mylinenumber;  } 
;
Program : ListTopDef {  std::reverse($1->begin(),$1->end()) ;$$ = new Latte::Prog($1); $$->line_number = Latteyy_mylinenumber; Latte::YY_RESULT_Program_= $$; } 
;
ListTopDef : TopDef {  $$ = new Latte::ListTopDef() ; $$->push_back($1);  } 
  | TopDef ListTopDef {  $2->push_back($1) ; $$ = $2 ;  }
;
TopDef : ClassDef {  $$ = new Latte::ClsDefTop($1); $$->line_number = Latteyy_mylinenumber;  } 
  | FunDef {  $$ = new Latte::FnDefTop($1); $$->line_number = Latteyy_mylinenumber;  }
;
FunDef : Type IdentP LATTE__SYMB_0 ListArg LATTE__SYMB_1 Block {  std::reverse($4->begin(),$4->end()) ;$$ = new Latte::FnDef($1, $2, $4, $6); $$->line_number = Latteyy_mylinenumber;  } 
;
Arg : Type IdentP {  $$ = new Latte::Ar($1, $2); $$->line_number = Latteyy_mylinenumber;  } 
;
ListArg : /* empty */ {  $$ = new Latte::ListArg();  } 
  | Arg {  $$ = new Latte::ListArg() ; $$->push_back($1);  }
  | Arg LATTE__SYMB_2 ListArg {  $3->push_back($1) ; $$ = $3 ;  }
;
ClassDef : LATTE__SYMB_29 IdentP ClassBlock {  $$ = new Latte::ClsDef($2, $3); $$->line_number = Latteyy_mylinenumber;  } 
  | LATTE__SYMB_29 IdentP LATTE__SYMB_31 _IDENT_ ClassBlock {  $$ = new Latte::ClsExtDef($2, $4, $5); $$->line_number = Latteyy_mylinenumber;  }
;
ClassBlock : LATTE__SYMB_3 ListClassBlockDef LATTE__SYMB_4 {  std::reverse($2->begin(),$2->end()) ;$$ = new Latte::ClsBlk($2); $$->line_number = Latteyy_mylinenumber;  } 
;
ClassBlockDef : FunDef {  $$ = new Latte::ClsMthDef($1); $$->line_number = Latteyy_mylinenumber;  } 
  | Type ListClsFldDefItem LATTE__SYMB_5 {  std::reverse($2->begin(),$2->end()) ;$$ = new Latte::ClsFldDef($1, $2); $$->line_number = Latteyy_mylinenumber;  }
;
ClsFldDefItem : IdentP {  $$ = new Latte::ClsFldDefIt($1); $$->line_number = Latteyy_mylinenumber;  } 
;
ListClsFldDefItem : ClsFldDefItem {  $$ = new Latte::ListClsFldDefItem() ; $$->push_back($1);  } 
  | ClsFldDefItem LATTE__SYMB_2 ListClsFldDefItem {  $3->push_back($1) ; $$ = $3 ;  }
;
ListClassBlockDef : ClassBlockDef {  $$ = new Latte::ListClassBlockDef() ; $$->push_back($1);  } 
  | ClassBlockDef ListClassBlockDef {  $2->push_back($1) ; $$ = $2 ;  }
;
Block : LATTE__SYMB_3 ListStmt LATTE__SYMB_4 {  $$ = new Latte::Blk($2); $$->line_number = Latteyy_mylinenumber;  } 
;
ListStmt : /* empty */ {  $$ = new Latte::ListStmt();  } 
  | ListStmt Stmt {  $1->push_back($2) ; $$ = $1 ;  }
;
Stmt : Type ListDeclItem LATTE__SYMB_5 {  std::reverse($2->begin(),$2->end()) ;$$ = new Latte::Decl($1, $2); $$->line_number = Latteyy_mylinenumber;  } 
  | Expr LATTE__SYMB_6 Expr LATTE__SYMB_5 {  $$ = new Latte::Assign($1, $3); $$->line_number = Latteyy_mylinenumber;  }
  | Expr LATTE__SYMB_7 LATTE__SYMB_5 {  $$ = new Latte::Incr($1); $$->line_number = Latteyy_mylinenumber;  }
  | Expr LATTE__SYMB_8 LATTE__SYMB_5 {  $$ = new Latte::Decr($1); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_38 Expr LATTE__SYMB_5 {  $$ = new Latte::Ret($2); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_38 LATTE__SYMB_5 {  $$ = new Latte::VRet(); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_42 LATTE__SYMB_0 Expr LATTE__SYMB_1 Stmt {  $$ = new Latte::While($3, $5); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_33 LATTE__SYMB_0 Type _IDENT_ LATTE__SYMB_9 Expr LATTE__SYMB_1 Stmt {  $$ = new Latte::For($3, $4, $6, $8); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_34 LATTE__SYMB_0 Expr LATTE__SYMB_1 Stmt {  $$ = new Latte::If($3, $5); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_34 LATTE__SYMB_0 Expr LATTE__SYMB_1 Stmt LATTE__SYMB_30 Stmt {  $$ = new Latte::IfElse($3, $5, $7); $$->line_number = Latteyy_mylinenumber;  }
  | Block {  $$ = new Latte::BStmt($1); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_5 {  $$ = new Latte::Empty(); $$->line_number = Latteyy_mylinenumber;  }
  | Expr LATTE__SYMB_5 {  $$ = new Latte::Exp($1); $$->line_number = Latteyy_mylinenumber;  }
;
DeclItem : _IDENT_ {  $$ = new Latte::NoInit($1); $$->line_number = Latteyy_mylinenumber;  } 
  | _IDENT_ LATTE__SYMB_6 Expr {  $$ = new Latte::Init($1, $3); $$->line_number = Latteyy_mylinenumber;  }
;
ListDeclItem : DeclItem {  $$ = new Latte::ListDeclItem() ; $$->push_back($1);  } 
  | DeclItem LATTE__SYMB_2 ListDeclItem {  $3->push_back($1) ; $$ = $3 ;  }
;
ClassType : _IDENT_ {  $$ = new Latte::TClass($1); $$->line_number = Latteyy_mylinenumber;  } 
;
BasicType : LATTE__SYMB_35 {  $$ = new Latte::TInt(); $$->line_number = Latteyy_mylinenumber;  } 
  | LATTE__SYMB_39 {  $$ = new Latte::TStr(); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_28 {  $$ = new Latte::TBool(); $$->line_number = Latteyy_mylinenumber;  }
;
NonVoidType : ClassType {  $$ = new Latte::TNVC($1); $$->line_number = Latteyy_mylinenumber;  } 
  | BasicType {  $$ = new Latte::TNVB($1); $$->line_number = Latteyy_mylinenumber;  }
;
Type : NonVoidType LATTE__SYMB_10 {  $$ = new Latte::TArray($1); $$->line_number = Latteyy_mylinenumber;  } 
  | NonVoidType {  $$ = new Latte::TSingle($1); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_41 {  $$ = new Latte::TVoid(); $$->line_number = Latteyy_mylinenumber;  }
;
CastOn : LATTE__SYMB_0 _IDENT_ LATTE__SYMB_1 {  $$ = new Latte::CastOnClass($2); $$->line_number = Latteyy_mylinenumber;  } 
  | LATTE__SYMB_0 NonVoidType LATTE__SYMB_10 LATTE__SYMB_1 {  $$ = new Latte::CastOnArr($2); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_0 BasicType LATTE__SYMB_1 {  $$ = new Latte::CastOnBasic($2); $$->line_number = Latteyy_mylinenumber;  }
;
Expr6 : LATTE__SYMB_36 NonVoidType LATTE__SYMB_11 Expr LATTE__SYMB_12 {  $$ = new Latte::ENewArr($2, $4); $$->line_number = Latteyy_mylinenumber;  } 
  | LATTE__SYMB_36 _IDENT_ {  $$ = new Latte::ENewCls($2); $$->line_number = Latteyy_mylinenumber;  }
  | _INTEGER_ {  $$ = new Latte::ELitInt($1); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_40 {  $$ = new Latte::ELitTrue(); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_32 {  $$ = new Latte::ELitFalse(); $$->line_number = Latteyy_mylinenumber;  }
  | _STRING_ {  $$ = new Latte::EString($1); $$->line_number = Latteyy_mylinenumber;  }
  | _IDENT_ {  $$ = new Latte::EVar($1); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_37 {  $$ = new Latte::ENull(); $$->line_number = Latteyy_mylinenumber;  }
  | Expr6 LATTE__SYMB_13 _IDENT_ {  $$ = new Latte::EMemb($1, $3); $$->line_number = Latteyy_mylinenumber;  }
  | _IDENT_ LATTE__SYMB_0 ListExpr LATTE__SYMB_1 {  std::reverse($3->begin(),$3->end()) ;$$ = new Latte::EFunCall($1, $3); $$->line_number = Latteyy_mylinenumber;  }
  | Expr6 LATTE__SYMB_13 _IDENT_ LATTE__SYMB_0 ListExpr LATTE__SYMB_1 {  std::reverse($5->begin(),$5->end()) ;$$ = new Latte::EMembCall($1, $3, $5); $$->line_number = Latteyy_mylinenumber;  }
  | Expr6 LATTE__SYMB_11 Expr LATTE__SYMB_12 {  $$ = new Latte::EAt($1, $3); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_0 Expr LATTE__SYMB_1 {  $$ = $2;  }
;
Expr5 : CastOn Expr5 {  $$ = new Latte::ECast($1, $2); $$->line_number = Latteyy_mylinenumber;  } 
  | LATTE__SYMB_14 Expr5 {  $$ = new Latte::Neg($2); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_15 Expr5 {  $$ = new Latte::Not($2); $$->line_number = Latteyy_mylinenumber;  }
  | Expr6 {  $$ = $1;  }
;
Expr4 : Expr4 MulOp Expr5 {  $$ = new Latte::EMul($1, $2, $3); $$->line_number = Latteyy_mylinenumber;  } 
  | Expr5 {  $$ = $1;  }
;
Expr3 : Expr3 AddOp Expr4 {  $$ = new Latte::EAdd($1, $2, $3); $$->line_number = Latteyy_mylinenumber;  } 
  | Expr4 {  $$ = $1;  }
;
Expr2 : Expr2 RelOp Expr3 {  $$ = new Latte::ERel($1, $2, $3); $$->line_number = Latteyy_mylinenumber;  } 
  | Expr3 {  $$ = $1;  }
;
Expr1 : Expr2 LATTE__SYMB_16 Expr1 {  $$ = new Latte::EAnd($1, $3); $$->line_number = Latteyy_mylinenumber;  } 
  | Expr2 {  $$ = $1;  }
;
Expr : Expr1 LATTE__SYMB_17 Expr {  $$ = new Latte::EOr($1, $3); $$->line_number = Latteyy_mylinenumber;  } 
  | Expr1 {  $$ = $1;  }
;
ListExpr : /* empty */ {  $$ = new Latte::ListExpr();  } 
  | Expr {  $$ = new Latte::ListExpr() ; $$->push_back($1);  }
  | Expr LATTE__SYMB_2 ListExpr {  $3->push_back($1) ; $$ = $3 ;  }
;
AddOp : LATTE__SYMB_18 {  $$ = new Latte::Plus(); $$->line_number = Latteyy_mylinenumber;  } 
  | LATTE__SYMB_14 {  $$ = new Latte::Minus(); $$->line_number = Latteyy_mylinenumber;  }
;
MulOp : LATTE__SYMB_19 {  $$ = new Latte::Times(); $$->line_number = Latteyy_mylinenumber;  } 
  | LATTE__SYMB_20 {  $$ = new Latte::Div(); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_21 {  $$ = new Latte::Mod(); $$->line_number = Latteyy_mylinenumber;  }
;
RelOp : LATTE__SYMB_22 {  $$ = new Latte::LTH(); $$->line_number = Latteyy_mylinenumber;  } 
  | LATTE__SYMB_23 {  $$ = new Latte::LE(); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_24 {  $$ = new Latte::GTH(); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_25 {  $$ = new Latte::GE(); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_26 {  $$ = new Latte::EQU(); $$->line_number = Latteyy_mylinenumber;  }
  | LATTE__SYMB_27 {  $$ = new Latte::NE(); $$->line_number = Latteyy_mylinenumber;  }
;

