/*
 * Jacek Łysiak
 *
 * Latte types
 */

#include <latte/common/Type.h>
#include <latte/common/RelOp.h>

#include <cassert>

namespace TypeHelper {

using namespace Latte;

class TypeExtractor : public Visitor
{
public:
	TypeExtractor() : _result(nullptr) {}

	std::shared_ptr<Common::Type> get_type(Type *p);
	std::shared_ptr<Common::Type> get_cast(CastOn *p);

	void visitType(Type *p) {}
	void visitCastOn(CastOn *p) {}

	void visitClassType(ClassType *p) {}
	void visitBasicType(BasicType *p) {}
	void visitNonVoidType(NonVoidType *p) {}

	void visitTClass(TClass *p);
	void visitTInt(TInt *p);
	void visitTStr(TStr *p);
	void visitTBool(TBool *p);
	void visitTNVC(TNVC *p);
	void visitTNVB(TNVB *p);
	void visitTArray(TArray *p);
	void visitTSingle(TSingle *p);
	void visitTVoid(TVoid *p);

	void visitCastOnClass(CastOnClass *p);
	void visitCastOnArr(CastOnArr *p);
	void visitCastOnBasic(CastOnBasic *p);

	void visitIdentP(IdentP *p) {} 
	void visitProgram(Program *p) {}
	void visitTopDef(TopDef *p) {}
	void visitFunDef(FunDef *p){}
	void visitArg(Arg *p){}
	void visitClassDef(ClassDef *p){}
	void visitClassBlock(ClassBlock *p){}
	void visitClassBlockDef(ClassBlockDef *p){}
	void visitClsFldDefItem(ClsFldDefItem *p){}
	void visitBlock(Block *p){}
	void visitStmt(Stmt *p){}
	void visitDeclItem(DeclItem *p){}
	void visitExpr(Expr *p){}
	void visitAddOp(AddOp *p){}
	void visitMulOp(MulOp *p){}
	void visitRelOp(RelOp *p){}
	void visitIdentPos(IdentPos *p){}
	void visitProg(Prog *p){}
	void visitClsDefTop(ClsDefTop *p){}
	void visitFnDefTop(FnDefTop *p){}
	void visitFnDef(FnDef *p){}
	void visitAr(Ar *p){}
	void visitClsDef(ClsDef *p){}
	void visitClsExtDef(ClsExtDef *p){}
	void visitClsBlk(ClsBlk *p){}
	void visitClsMthDef(ClsMthDef *p){}
	void visitClsFldDef(ClsFldDef *p){}
	void visitClsFldDefIt(ClsFldDefIt *p){}
	void visitBlk(Blk *p){}
	void visitDecl(Decl *p){}
	void visitNoInit(NoInit *p){}
	void visitInit(Init *p){}
	void visitAssign(Assign *p){}
	void visitIncr(Incr *p){}
	void visitDecr(Decr *p){}
	void visitRet(Ret *p){}
	void visitVRet(VRet *p){}
	void visitWhile(While *p){}
	void visitFor(For *p){}
	void visitIf(If *p){}
	void visitIfElse(IfElse *p){}
	void visitBStmt(BStmt *p){}
	void visitEmpty(Empty *p){}
	void visitExp(Exp *p){}
	void visitENewArr(ENewArr *p){}
	void visitENewCls(ENewCls *p){}
	void visitELitInt(ELitInt *p){}
	void visitELitTrue(ELitTrue *p){}
	void visitELitFalse(ELitFalse *p){}
	void visitEString(EString *p){}
	void visitEVar(EVar *p){}
	void visitENull(ENull *p){}
	void visitEMemb(EMemb *p){}
	void visitEFunCall(EFunCall *p){}
	void visitEMembCall(EMembCall *p){}
	void visitEAt(EAt *p){}
	void visitECast(ECast *p){}
	void visitNeg(Neg *p){}
	void visitNot(Not *p){}
	void visitEMul(EMul *p){}
	void visitEAdd(EAdd *p){}
	void visitERel(ERel *p){}
	void visitEAnd(EAnd *p){}
	void visitEOr(EOr *p){}
	void visitPlus(Plus *p){}
	void visitMinus(Minus *p) {}
	void visitTimes(Times *p){}
	void visitDiv(Div *p) {}
	void visitMod(Mod *p) {}
	void visitLTH(LTH *p) {} 
	void visitLE(LE *p) {}
	void visitGTH(GTH *p) {}
	void visitGE(GE *p) {}
	void visitEQU(EQU *p) {}
	void visitNE(NE *p) {}
	void visitListTopDef(ListTopDef *p) {} 
	void visitListArg(ListArg *p) {}
	void visitListClsFldDefItem(ListClsFldDefItem *p) {}
	void visitListClassBlockDef(ListClassBlockDef *p) {}
	void visitListStmt(ListStmt *p){}
	void visitListDeclItem(ListDeclItem *p) {}
	void visitListExpr(ListExpr *p) {}
	void visitInteger(Integer x) {}
	void visitChar(Char x) {}
	void visitDouble(Double x) {}
	void visitString(String x) {}
	void visitIdent(Ident x) {}
private:
	std::shared_ptr<Common::Type> _result;
};


std::shared_ptr<Common::Type> TypeExtractor::get_type(Type *p) 
{
	p->accept(this);
	assert(_result != nullptr);
	return _result;
}

std::shared_ptr<Common::Type> TypeExtractor::get_cast(CastOn *p)
{
	p->accept(this);
	assert(_result != nullptr);
	return _result;
}

void TypeExtractor::visitTClass(TClass *t_class)
{
	auto t = std::make_shared<Common::Class>(t_class->ident_);

	if (_result == nullptr)
		_result = t;
	else
		std::dynamic_pointer_cast<Common::Array>(_result)->set_elem_type(t);
}

void TypeExtractor::visitTInt(TInt *t_int)
{
	auto t = std::make_shared<Common::Int>();
	if (_result == nullptr)
		_result = t;
	else
		std::dynamic_pointer_cast<Common::Array>(_result)->set_elem_type(t);
}

void TypeExtractor::visitTStr(TStr *t_str)
{
	auto t = std::make_shared<Common::String>();
	if (_result == nullptr)
		_result = t;
	else
		std::dynamic_pointer_cast<Common::Array>(_result)->set_elem_type(t);
}

void TypeExtractor::visitTBool(TBool *t_bool)
{
	auto t = std::make_shared<Common::Bool>();
	if (_result == nullptr)
		_result = t;
	else
		std::dynamic_pointer_cast<Common::Array>(_result)->set_elem_type(t);
}

void TypeExtractor::visitTNVC(TNVC *tnvc)
{
	tnvc->classtype_->accept(this);
}

void TypeExtractor::visitTNVB(TNVB *tnvb)
{
	tnvb->basictype_->accept(this);
}

void TypeExtractor::visitTArray(TArray *t_array)
{
	_result = std::make_shared<Common::Array>();
	t_array->nonvoidtype_->accept(this);
}

void TypeExtractor::visitTSingle(TSingle *t_single)
{
	t_single->nonvoidtype_->accept(this);
}

void TypeExtractor::visitTVoid(TVoid *t_void)
{
	_result = std::make_shared<Common::Void>();
}

/*============ CASTING =======================================================*/

void TypeExtractor::visitCastOnClass(CastOnClass *cast_on_class)
{
	_result = std::make_shared<Common::Class>(cast_on_class->ident_);
}

void TypeExtractor::visitCastOnArr(CastOnArr *cast_on_arr)
{
	_result = std::make_shared<Common::Array>();
	cast_on_arr->nonvoidtype_->accept(this);
}

void TypeExtractor::visitCastOnBasic(CastOnBasic *cast_on_basic)
{
	cast_on_basic->basictype_->accept(this);
}

} // TYPE HELPER

namespace Common
{

std::shared_ptr<Type> Type::from_type_AST(Latte::Type * typeAstPtr)
{
	TypeHelper::TypeExtractor ext;
	return ext.get_type(typeAstPtr);
}

std::shared_ptr<Type> Type::from_cast_AST(Latte::CastOn * castAstPtr)
{
	TypeHelper::TypeExtractor ext;
	return ext.get_cast(castAstPtr);
}

bool Type::operator!= (const Type& other) 
{
	return !(*this == other);
}

bool Type::operator== (const Type& other) 
{
	if (this->type() != other.type())
		return false;
	if (this->type() == TYPE_ARRAY) {
		auto mytype = ((Array *) this)->elem_type();
		auto ottype = ((Array *) &other)->elem_type();
		return *mytype == *ottype;
	}
	if (this->type() == TYPE_CLASS) {
		auto myname = ((Class *) this)->name();
		auto ottype = ((Class *) &other)->name();
		return myname == ottype;
	}
	return true;
}

bool Type::is_cast_possible(std::shared_ptr<Type> onType)
{
	type_t my = type();
	type_t on = onType->type();

	switch(my) {
		case TYPE_NULL:
			if (on == TYPE_CLASS || on == TYPE_ARRAY)
				return true;
			break;

		case TYPE_INT:
			if (on == TYPE_INT || on == TYPE_BOOL)
				return true;
			break;

		case TYPE_BOOL:
			if (on == TYPE_INT || on == TYPE_BOOL)
				return true;
			break;
		default:
			break;
	}
	return false;
}

bool Type::is_relop_compatible(relop_t op) 
{
	type_t my = type();

	switch(my) {
		case TYPE_NULL:
		case TYPE_BOOL:
		case TYPE_STRING:
		case TYPE_ARRAY:
		case TYPE_CLASS:
			if (op == RELOP_EQU || op == RELOP_NE)
				return true;
			break;

		case TYPE_INT:
			return true;

		default:
			break;
	}
	return false;

}

}
