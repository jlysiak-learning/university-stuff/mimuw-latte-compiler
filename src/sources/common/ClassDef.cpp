
#include <latte/common/ClassDefinition.h>

namespace Common 
{

ClassDefinition::ClassDefinition(int line, std::string name) :
	_line(line), _name(name), _parentName(""), _parent(nullptr) {
	_type = std::make_shared<Class>(name);
}

ClassDefinition::ClassDefinition(
		int line, 
		std::string name, 
		std::string parent_name) :
	_line(line), _name(name), _parentName(parent_name),  _parent(nullptr) {
	_type = std::make_shared<Class>(name);
}

bool ClassDefinition::add_field(std::shared_ptr<Type> type, std::string name) {
	if (has_field_visible(name))
		return false;
	_fields[name] = type;
	return true;
}

void ClassDefinition::set_parent(std::shared_ptr<ClassDefinition> parent)
{
	_parent = parent;
}

bool ClassDefinition::add_method(std::shared_ptr<FunctionDefinition> funPtr){
	if (has_method_visible(funPtr->get_name())) {
		auto m = get_method(funPtr->get_name());
		if (*m != *funPtr) // compare signature
			return false;
	}
	_methods[funPtr->get_name()] = funPtr;
	return true;
}

bool ClassDefinition::has_field_visible(std::string name) {
	if (_fields.end() != _fields.find(name))
		return true;
	if (_parent != nullptr)
		return _parent->has_field(name);
	return false;
}

bool ClassDefinition::has_method_visible(std::string name) {
	if (_methods.end() != _methods.find(name))
		return true;
	if (_parent != nullptr)
		return _parent->has_method(name);
	return false;
}

bool ClassDefinition::has_field(std::string name) {
	return _fields.end() != _fields.find(name);
}

bool ClassDefinition::has_method(std::string name) {
	return _methods.end() != _methods.find(name);
}

bool ClassDefinition::has_ident(std::string name) {
	return has_method(name) || has_method(name);
}

std::shared_ptr<Type> ClassDefinition::get_field_type(std::string name) {
	if (has_field(name))
		return _fields[name];
	if (_parent != nullptr)
		return _parent->get_field_type(name);
	return nullptr;
}

std::shared_ptr<FunctionDefinition> ClassDefinition::get_method(std::string name) {
	if (has_method(name))
		return _methods[name];
	if (_parent != nullptr)
		return _parent->get_method(name);
	return nullptr;
}

bool ClassDefinition::valid() {
	if (_parent == nullptr)
		return true;
	for (auto el : _fields) {
		if (_parent->has_field_visible(el.first))
			return false;
	}
	for (auto el : _methods) {
		auto mth = _parent->get_method(el.first);
		if (mth == nullptr)
			continue;
		if (*mth != *el.second)
			return false;
	}
	return true;
}

}
