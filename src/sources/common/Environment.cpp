/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Compiler environment.
 */

#include <latte/common/Environment.h>

namespace Common 
{

Environment::Environment() {
	_reserved_idents.push_back("printInt");
	_reserved_idents.push_back("printString");
	_reserved_idents.push_back("error");
	_reserved_idents.push_back("readInt");
	_reserved_idents.push_back("readString");
	_reserved_idents.push_back("self");
	
	auto funPtr = std::make_shared<Common::FunctionDefinition>(std::make_shared<Void>(), "printInt");
	funPtr->add_arg(std::make_shared<Int>(), "x");
	_functionDefs["printInt"] = funPtr;

	funPtr = std::make_shared<Common::FunctionDefinition>(std::make_shared<Void>(), "printString");
	funPtr->add_arg(std::make_shared<String>(), "x");
	_functionDefs["printString"] = funPtr;
	
	funPtr = std::make_shared<Common::FunctionDefinition>(std::make_shared<Void>(), "error");
	_functionDefs["error"] = funPtr;

	funPtr = std::make_shared<Common::FunctionDefinition>(std::make_shared<Int>(), "readInt");
	_functionDefs["readInt"] = funPtr;

	funPtr = std::make_shared<Common::FunctionDefinition>(std::make_shared<String>(), "readString");
	_functionDefs["readString"] = funPtr;
}

Environment::Environment(const Environment& env) :
	_classDefs(env._classDefs), _functionDefs(env._functionDefs),
	_reserved_idents(env._reserved_idents) {}

bool Environment::is_reserved_ident(std::string name) const {
	for (auto n : _reserved_idents)
		if (n == name)
			return true;
	return false;
}

bool Environment::entrypoint_defined() const {
	auto entry = _functionDefs.find("main");
	if (entry == _functionDefs.end())
		return false;
	if (entry->second->arg_num() > 0)
		return false;
	if (!entry->second->type()->is<Common::Int>())
		return false;
	return true;
}

bool Environment::is_class_defined(std::string name)
{
	auto iter = _classDefs.find(name);
	return (iter != _classDefs.end());
}

std::shared_ptr<Common::ClassDefinition> Environment::get_class_def(std::string name)
{
	auto iter = _classDefs.find(name);
	if (iter == _classDefs.end())
		return nullptr;
	return iter->second;
}

std::shared_ptr<Common::FunctionDefinition> Environment::get_fun_def(std::string name)
{
	auto iter = _functionDefs.find(name);
	if (iter == _functionDefs.end())
		return nullptr;
	return iter->second;
}

void Environment::add_class_def(std::shared_ptr<Common::ClassDefinition> clsPtr)
{
	_classDefs[clsPtr->get_name()] = clsPtr;
}

void Environment::add_fun_def(std::shared_ptr<Common::FunctionDefinition> funPtr)
{
	_functionDefs[funPtr->get_name()] = funPtr;
}

bool Environment::is_child_of(std::shared_ptr<Type> child, std::shared_ptr<Type> base)
{
	if (!(child->is<Common::Class>() && base->is<Common::Class>()))
		return false;
	auto baseCls = std::dynamic_pointer_cast<Common::Class>(base);
	auto baseName = baseCls->name();
	auto childCls = std::dynamic_pointer_cast<Common::Class>(child);
	auto cname = childCls->name();
	auto p = _classDefs[cname]->get_parent();
	while (p != nullptr) {
		if (baseName == p->get_name())
			return true;
		p = p->get_parent();
	}
	return false;
}

} // end of namespace Common

