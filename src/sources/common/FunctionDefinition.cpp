
#include <latte/common/FunctionDefinition.h>

namespace Common 
{

FunctionDefinition::FunctionDefinition(int line, std::shared_ptr<Type> type, std::string name) :
	_predefined(false), _line(line), _type(type), _name(name) {}

FunctionDefinition::FunctionDefinition(std::shared_ptr<Type> type, std::string name) :
	 _predefined(true), _line(-1), _type(type), _name(name) {}


void FunctionDefinition::add_arg(std::shared_ptr<Type> type, std::string name) {
	_args.push_back(
		std::make_pair(type, name)
		);
}

bool FunctionDefinition::is_arg_name_defined(std::string name) {
	for (auto el : _args) {
		if (el.second == name)
			return true;
	}
	return false;
}

void FunctionDefinition::add_body(Latte::Blk *ast_tree) {
	_fun_body = ast_tree->clone();
}

std::string FunctionDefinition::str() const {
	std::string s = _type->str() + " " + _name + "(";
	unsigned int i = 0;
	if (_args.size() > 0) {
		for (; i < _args.size() - 1; ++i) {
			s += _args[i].first->str() + " " + _args[i].second + ", ";
		}
		s += _args[i].first->str() + " " + _args[i].second;
	}
	return s + ")";
}


bool FunctionDefinition::operator!= (const FunctionDefinition& other) 
{
	return !(*this == other);
}

bool FunctionDefinition::operator== (const FunctionDefinition& other) 
{
	if (_name != other._name)
		return false;
	if (*_type != *other._type)
		return false;
	int sz1 = _args.size();
	int sz2 = other._args.size();
	if (sz1 != sz2)
		return false;
	for (int i = 0; i < sz1; i++)
		if (*_args[i].first != *other._args[i].first)
			return false;
	return true;
}

}

