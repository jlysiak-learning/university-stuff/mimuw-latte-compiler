#include <latte/common/AddOp.h>

namespace Common {

std::string addop_to_string(addop_t op)
{
	switch(op) {
		case ADDOP_ADD:
			return "+";
		case ADDOP_SUB:
			return "-";
	}
	return "ERROR";
}

}
