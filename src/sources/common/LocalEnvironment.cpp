/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 */

#include <latte/error.h>
#include <latte/common/LocalEnvironment.h>

namespace Common 
{

LocalEnvironment::LocalEnvironment(const Environment& env) :
	Environment(env), _nestLvl(0)
{ }

LocalEnvironment::LocalEnvironment(const LocalEnvironment& env) :
	Environment(env), _variables(env._variables), _nestLvls(env._nestLvls),
	_nestLvl(env._nestLvl+1)
{ }

bool LocalEnvironment::add_variable(std::shared_ptr<Type>& type, std::string name)
{
	if (_variables.find(name) != _variables.end()) {
		int lvl = _nestLvls[name];	
		if (lvl == _nestLvl)
			return false; // Cannot override variable in the same block
	}
	_variables[name] = type;
	_nestLvls[name] = _nestLvl;
	return true;
}

std::shared_ptr<Type> LocalEnvironment::get_variable_type(std::string name)
{
	if (_variables.find(name) != _variables.end())
		return _variables[name];
	return nullptr;
}

} // end of namespace Common

