/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Logs handler.
 */

#include <latte/common/Logger.h>

namespace Common {

/*======================================= LOG ENTRY */

std::ostream& operator<< (std::ostream &output, LogEntry const& entry) 
{
	output << entry._format_message() << std::endl;
	return output;
}

std::string LogEntry::_format_message() const
{
	std::string s("[");
	switch (_level) {
		case LOG_LVL_ERR:
			s += "\033[91mERROR\033[0m";
			break;
		case LOG_LVL_WARN:
			s += "\033[92mWARNING\033[0m";
			break;
		case LOG_LVL_INFO:
			s += "INFO";
			break;
		case LOG_LVL_DEBUG:
			s += "DEBUG";
			break;
	}
	s += "] ";
	if (_line != -1)
		s += "At line " + std::to_string(_line) + ": ";
	s += _msg;
	return s;
}

LogEntry::LogEntry(log_lvl_t lvl, std::string msg) :
	_msg(msg), _level(lvl), _time(std::chrono::system_clock::now()), _line(-1) {}

LogEntry::LogEntry(log_lvl_t lvl, std::string msg, int line) :
	_msg(msg), _level(lvl), _time(std::chrono::system_clock::now()), _line(line) {}

log_lvl_t LogEntry::level() const
{
	return _level;
}

/*======================================= LOGGER */

Logger::Logger(std::ostream &output) :
	_log_lvl(LOG_LVL_ERR), _output(output) {}

Logger::Logger(std::ostream &output, log_lvl_t lvl) :
	_output(output)
{
	set_level(lvl);
}

void Logger::set_level(log_lvl_t lvl)
{
	_log_lvl = lvl;
}

bool Logger::has_errors() 
{
	for (auto const &value : _logs)
		if (value.level() == LOG_LVL_ERR)
			return true;
	return false;
}

void Logger::error(std::string msg)
{
	_add_entry(LOG_LVL_ERR, msg);
}

void Logger::warn(std::string msg)
{
	_add_entry(LOG_LVL_WARN, msg);
}

void Logger::info(std::string msg)
{
	_add_entry(LOG_LVL_INFO, msg);
}

void Logger::debug(std::string msg)
{
	_add_entry(LOG_LVL_DEBUG, msg);
}

void Logger::error_at(int line, std::string msg)
{
	_add_entry(LOG_LVL_ERR, msg, line);
}

void Logger::warn_at(int line, std::string msg)
{
	_add_entry(LOG_LVL_WARN, msg, line);
}

void Logger::info_at(int line, std::string msg)
{
	_add_entry(LOG_LVL_INFO, msg, line);
}

void Logger::debug_at(int line, std::string msg)
{
	_add_entry(LOG_LVL_DEBUG, msg, line);
}

void Logger::_add_entry(log_lvl_t lvl, std::string msg)
{
	LogEntry log(lvl, msg);
	if (log.level() <= _log_lvl)
		_output << log;
	_logs.push_back(std::move(log));
}

void Logger::_add_entry(log_lvl_t lvl, std::string msg, int line)
{
	LogEntry log(lvl, msg, line);
	if (log.level() <= _log_lvl)
		_output << log;
	_logs.push_back(std::move(log));
}

}

