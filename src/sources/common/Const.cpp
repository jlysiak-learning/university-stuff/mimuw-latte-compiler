#include <cassert>
#include <latte/common/Const.h>

namespace Common {


std::string Const::to_string() const
{
	if (_type == TYPE_INT) 
		return "(INT) " + std::to_string(_i);
	if (_type == TYPE_STRING) 
		return "(STRING) \"" + _s + "\"";
	if (_type == TYPE_BOOL)  {
		if (_b)
			return "(BOOL) TRUE";
		else 
			return "(BOOL) FALSE";
	}
	assert(false);
	return "";
}

std::shared_ptr<Const> Const::mul(const std::shared_ptr<Const>& c1, const std::shared_ptr<Const>& c2, mulop_t op) 
{
	if (c1->_type != TYPE_INT || c2->_type != TYPE_INT)
		return nullptr;
	int r = 0;
	switch (op) {
		case MULOP_MUL:
			r = c1->_i * c2->_i;
			break;

		case MULOP_DIV:
			r = c1->_i / c2->_i;
			break;

		case MULOP_MOD:
			r = c1->_i % c2->_i;
			break;
		default:
			assert(false);
	}
	return std::make_shared<Const>(r);
}

std::shared_ptr<Const> Const::add(const std::shared_ptr<Const>& c1, const std::shared_ptr<Const>& c2, addop_t op) 
{
	if (c1->_type == TYPE_INT && c2->_type == TYPE_INT) {
		int r = 0;
		switch (op) {
			case ADDOP_ADD:
				r = c1->_i + c2->_i;
				break;

			case ADDOP_SUB:
				r = c1->_i - c2->_i;
				break;
			default:
				assert(false);
		}
		return std::make_shared<Const>(r);
	} else if (c1->_type == TYPE_STRING && c2->_type == TYPE_STRING) {
		if (op == ADDOP_ADD)
			return std::make_shared<Const>(c1->_s + c2->_s);
		assert(false);
	}
	assert(false);
	return nullptr;
}

std::shared_ptr<Const> Const::rel(const std::shared_ptr<Const>& c1, const std::shared_ptr<Const>& c2, relop_t op) 
{
	if (c1->_type == TYPE_INT && c2->_type == TYPE_INT) {
		int i = c1->_i, j = c2->_i;
		bool r = false;
		switch(op) {
			case RELOP_EQU:
				r = i == j;
				break;
			case RELOP_NE:
				r = i != j;
				break;
			case RELOP_GE:
				r = i >= j;
				break;
			case RELOP_LE:
				r = i <= j;
				break;
			case RELOP_GTH:
				r = i > j;
				break;
			case RELOP_LTH:
				r = i < j;
				break;
			default:
				assert(false);
		}
		return std::make_shared<Const>(r);
	} else if (c1->_type == TYPE_STRING && c2->_type == TYPE_STRING) {
		std::string i = c1->_s, j = c2->_s;
		bool r = false;
		switch(op) {
			case RELOP_EQU:
				r = i == j;
				break;
			case RELOP_NE:
				r = i != j;
				break;
			default:
				assert(false);
		}
		return std::make_shared<Const>(r);
	} else if (c1->_type == TYPE_BOOL && c2->_type == TYPE_BOOL) {
		bool i = c1->_b, j = c2->_b;
		bool r = false;
		switch(op) {
			case RELOP_EQU:
				r = i == j;
				break;
			case RELOP_NE:
				r = i != j;
				break;
			default:
				assert(false);
		}
		return std::make_shared<Const>(r);
	}
	assert(false);
	return nullptr;
}

std::shared_ptr<Const> Const::cast_on(std::shared_ptr<Type> t)
{
	switch(this->_type) {
		case TYPE_BOOL:
		case TYPE_INT:
			if (t->is<Bool>())
				return std::make_shared<Const>(_b);
			else if (t->is<Int>())
				return std::make_shared<Const>(_i);
			break;
		default:
			break;
	}
	return nullptr;
}

std::shared_ptr<Const> operator&& (const std::shared_ptr<Const>& c1, const std::shared_ptr<Const>& c2)
{
	if (c1->_type == TYPE_BOOL && c2->_type == TYPE_BOOL) {
		bool i = c1->_b, j = c2->_b;
		return std::make_shared<Const>(i && j);
	}
	return nullptr;
}

std::shared_ptr<Const> operator|| (const std::shared_ptr<Const>& c1, const std::shared_ptr<Const>& c2)
{
	if (c1->_type == TYPE_BOOL && c2->_type == TYPE_BOOL) {
		bool i = c1->_b, j = c2->_b;
		return std::make_shared<Const>(i || j);
	}
	return nullptr;
}

std::shared_ptr<Const> operator! (const std::shared_ptr<Const>& c)
{
	if (c->_type == TYPE_BOOL) {
		bool i = c->_b;
		return std::make_shared<Const>(!i);
	}
	
	return nullptr;
}

std::shared_ptr<Const> operator- (const std::shared_ptr<Const>& c)
{
	if (c->_type == TYPE_INT){
		int i = c->_i;
		return std::make_shared<Const>(-i);
	}
	return nullptr;
}

}
