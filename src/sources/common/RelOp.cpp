#include <latte/common/RelOp.h>

namespace Common {

std::string relop_to_string(relop_t op)
{
	switch(op) {
		case RELOP_EQU:
			return "==";
		case RELOP_NE:
			return "!=";
		case RELOP_GE:
			return ">=";
		case RELOP_LE:
			return "<=";
		case RELOP_GTH:
			return ">";
		case RELOP_LTH:
			return "<";
	}
	return "ERROR";
}

}
