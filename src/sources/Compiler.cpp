/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Latte compiler.
 */

#include <iostream>
#include <latte/Compiler.h>
#include <latte/latte/Parser.h> 

#include <latte/frontend/DefScanner.h>
#include <latte/frontend/TypeChecker.h>

#include <latte/backend/IRGenerator.h>



Compiler::Compiler(FILE *input, std::ostream& output, Common::Logger &logger) :
	_input(input), _output(output), _logger(logger) {}


void Compiler::compile() {
	_logger.info("Parsing program...");
	Frontend::DefScanner defScanner(_env, _logger);
	Frontend::TypeChecker typeChecker(_env, _logger);

	_ast_root = Latte::pProgram(_input);
	if (!_ast_root) {
		_logger.error("Compilation failed due to syntax errors!");
		_status = FAIL;
		return;
	}
	_logger.info("Parsed!");

	if (defScanner.scan_defs(_ast_root) != SUCCESS) {
		_logger.info("Definition loader failed!");
		_status = FAIL;
		return;
	}
	_logger.info("Definitions loaded!");

	if (!_env.entrypoint_defined()) {
		_logger.error("Entry point INT  main() not defined!");
		_status = FAIL;
		return;
	}
	_logger.info("OK - entry point found!");

	if (typeChecker.check() != SUCCESS) {
		_logger.error("TypeChecker errors occured!");
		_status = FAIL;
		return;
	}

	Backend::IRGenerator irgenerator(_env, _logger);
	irgenerator.generate();
	irgenerator.print(_output);
}

err_t Compiler::status() const
{
	return _status;
}
