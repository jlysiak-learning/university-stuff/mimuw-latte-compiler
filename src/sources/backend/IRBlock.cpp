#include <latte/backend/IRBlock.h>

namespace Backend
{

std::ostream& operator<< (std::ostream &output, IRBlock const& b) 
{
	output << b.str();
	return output;
}

IRBlock::IRBlock(std::string label)
{
	_code.push_back(std::make_shared<IRCodeLabel>(label));
}

std::string IRBlock::str() const
{
	std::string s = ""; 
	for (auto el : _code) {
		if (!el->is<IRCodeLabel>())
			s += "\t";
		s += el->code() + "\n";
	}

	return s;
}

std::string IRBlock::label()
{
	return std::dynamic_pointer_cast<IRCodeLabel>(_code[0])->label();
}

void IRBlock::add_code(std::shared_ptr<IRCode> code)
{
	_code.push_back(code);
}

bool IRBlock::ends_with_retvoid() const
{
	return _code[_code.size() - 1]->is<IRCodeRetV>();
}

bool IRBlock::ends_with_ret() const
{
	return _code[_code.size() - 1]->is<IRCodeRet>();
}

bool IRBlock::ends_with_goto() const
{
	return _code[_code.size() - 1]->is<IRCodeGoto>();
}

bool IRBlock::ends_with_if() const
{
	return _code[_code.size() - 1]->is<IRCodeIf>();
}

void IRBlock::add_entry_label(std::string label) 
{
	_entries.push_back(label);
}

}
