#include <latte/backend/IREnvironment.h>
#include <latte/backend/IRVar.h>
#include <latte/backend/IRType.h>

namespace Backend
{

std::map<std::string, int > IREnvironment::_counters;

IREnvironment::IREnvironment(
		const Environment& env,
		std::string scope,
		std::map<std::string, std::shared_ptr<IRGlobalString> >& strings,
		std::map<std::string, std::shared_ptr<IRDeclaration> >& declarations,
		std::map<std::string, std::shared_ptr<IRClassDef> >& classes,
		LabelGenerator& generator) :
	Environment(env), 
	_nestLvl(0),
	_scope(scope),
	_globalStrings(strings),
	_globalDeclarations(declarations),
	_classes(classes),
	_lgen(generator)
{ }

IREnvironment::IREnvironment(const IREnvironment& env) :
	Environment(env), 
	_functionArgs(env._functionArgs),
	_variables(env._variables), 
	_nestLvls(env._nestLvls),
	_nestLvl(env._nestLvl+1),
	_scope(env._scope),
	_globalStrings(env._globalStrings),
	_globalDeclarations(env._globalDeclarations),
	_classes(env._classes),
	_lgen(env._lgen)
{ }

std::shared_ptr<IRVar> IREnvironment::add_function_argument(std::shared_ptr<Common::Type> type, std::string name)
{
	auto v = std::make_shared<IRVar>(IRType::from_common_type(type), name);
	_functionArgs[name] = v;
	auto vlocal = std::make_shared<IRVar>(v, local_variable_label(name));
	vlocal->to_ptr();
	add_local_variable(vlocal, name);
	return v;
}

std::shared_ptr<IRVar> IREnvironment::add_local_variable(std::shared_ptr<Common::Type> t, std::string name)
{
	auto v = std::make_shared<IRVar>(IRType::from_common_type(t), local_variable_label(name));
	v->to_ptr();
	return add_local_variable(v, name);
}

std::shared_ptr<IRVar> IREnvironment::add_local_variable(std::shared_ptr<IRVar> var, std::string name)
{
	_variables[name] = var;
	_nestLvls[name] = _nestLvl;
	if (_counters.find(name) == _counters.end())
		_counters[name] = 1;
	else
		_counters[name] = _counters[name] + 1;
	return var;
}

std::shared_ptr<IRVar> IREnvironment::get_local_variable(std::string name) 
{ 
	if (_variables.find(name) == _variables.end())
		return nullptr;
	return _variables.at(name);
}

std::shared_ptr<IRVar> IREnvironment::get_function_arg(std::string name) 
{ 
	return _functionArgs.at(name);
}

std::string IREnvironment::local_variable_label(std::string name)
{
	return "_" + name + "_" + std::to_string(_counters[name]);
}

bool IREnvironment::in_runtime_lib(std::string name)
{
	for (auto el : functions()) {
		if (name != el.first)
			continue;
		if (el.second->isPredefined())
			return true;
	}
	if ( name == "_concat" ||
		name == "_strcmp" ||
		name == "_strcmpn" ||
		name == "_calloc")
			return true;
	return false;
}

void IREnvironment::add_runtime_function(std::string name, std::shared_ptr<IRDeclaration> decl)
{	
	if (!in_runtime_lib(name))
		return;
	if (_globalDeclarations.find(name) == _globalDeclarations.end()) {
		_globalDeclarations[name] = decl;
	}
}

std::shared_ptr<IRVar> IREnvironment::add_global_string(std::string name, std::string s)
{
	if (_globalStrings.find(name) == _globalStrings.end()) {
		auto gstr = std::make_shared<IRGlobalString>(name, s);	
		_globalStrings[name] = gstr;
	}
	return std::dynamic_pointer_cast<IRVar>(_globalStrings[name]);
}

std::shared_ptr<IRVar> IREnvironment::add_global_string(std::string s)
{
	std::string name = _lgen.next_glob_label();
	if (_globalStrings.find(name) == _globalStrings.end()) {
		auto gstr = std::make_shared<IRGlobalString>(name, s);	
		_globalStrings[name] = gstr;
	}
	return std::dynamic_pointer_cast<IRVar>(_globalStrings[name]);
}

std::shared_ptr<IRVar> IREnvironment::next_var(std::shared_ptr<IRVar> var)
{
	return std::make_shared<IRVar>(var, _lgen.next_temp_label());
}

std::shared_ptr<IRVar> IREnvironment::next_var(std::shared_ptr<IRType> type)
{
	return std::make_shared<IRVar>(type, _lgen.next_temp_label());
}

std::string IREnvironment::next_block_label() {
	return _lgen.next_block_label();
}

std::string IREnvironment::next_temp_label() {
	return _lgen.next_temp_label();
}
/*
std::shared_ptr<Type> IREnvironment::get_variable_type(std::string name)
{
	if (_variables.find(name) != _variables.end())
		return _variables[name];
	return nullptr;
}
*/


} 

