#include <latte/backend/IRGlobalString.h>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <string.h>

namespace Backend {

std::string IRGlobalString::code() const 
{
	char buff[3 * len()] = "";
	char c[16];
	for (auto i = 0; i < _content.length(); i++) {
		sprintf(c, "\\%02x", _content[i]);
		strcat(buff, c);
	}

	// Code generation
	std::string s = lstr();
	s += " = internal constant ";
	s += tstr() + " ";
	s += "c\"" + std::string(buff) + "\\00\", align 1";
	return s;
}

std::string IRGlobalString::lstr() const 
{ 
	return "@" + _id;
}


int IRGlobalString::len() const 
{ 
	return _content.length() + 1;
}


}

