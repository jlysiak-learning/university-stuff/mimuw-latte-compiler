#include <latte/backend/IRFunction.h>

namespace Backend
{

using namespace Latte;

IRFunction::IRFunction(
        std::string scopeName,
        std::string name,
        std::shared_ptr<IRType> t) :
	_name(name),
	_scopeName(scopeName),
	_type(t)
{}

void IRFunction::add_arg(std::shared_ptr<IRVar> var)
{
	_args.push_back(var);
}

void IRFunction::add_block(std::shared_ptr<IRBlock> block)
{
	auto label = block->label();
	_labels.push_back(label);
	_blocks[label] = block;
}

std::ostream& operator<< (std::ostream &output, IRFunction const& fun) 
{
	output << "define ";
	output << *fun._type;
	output << " ";
	output << "@" + fun.full_name();
	output << " (";

	if (fun._args.size()) {
		for (int i = 0; i < fun._args.size() - 1; i++)
			output << *fun._args.at(i) << ", ";
		output << *fun._args[fun._args.size() - 1] << ") {\n";
	} else 
		output << "){\n";

	for (auto label : fun._labels)
		output << *(fun._blocks.at(label));

	output << "}" << "\n";

	return output;
}

void IRFunction::register_function_call(std::string fullname)
{
	_functionsCalled.insert(fullname);
}

void IRFunction::register_global_usage(std::string fullname)
{
	_globalsUsed.insert(fullname);
}

std::string IRFunction::typestr() const {
	std::string s = _type->code() + " (";
	if (_args.size()) {
		for (int i = 0; i < _args.size() - 1; i++)
			s += _args.at(i)->tstr() + ", ";
		s += _args[_args.size() - 1]->tstr() + ")";
	} else 
		s += ")";
	s += "*";
	return s;
}

std::string IRFunction::varstr() const {
	std::string s = typestr() + " @" + full_name();
	return s;
}

}
