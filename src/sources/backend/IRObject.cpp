#include <latte/backend/IRObject.h>

namespace Backend {

std::ostream& operator<< (std::ostream& output, const IRObject& obj) {
	output << obj.code();
	return output;
}

}

