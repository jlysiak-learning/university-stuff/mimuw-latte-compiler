#include <latte/backend/IRDeclaration.h>


namespace Backend {

IRDeclaration::IRDeclaration(
			std::string name,
			std::shared_ptr<IRType> type) :
	_name(name), _type(type)
{
}

void IRDeclaration::add_arg(std::shared_ptr<IRType> t)
{
	_argstype.push_back(t);
}

std::string IRDeclaration::code() const
{
	std::string s = "declare ";
	s += _type->code() + " ";
	s += "@" + _name + " (";
	if (_argstype.size() != 0) {
		auto sz = _argstype.size();
		for (int i = 0; i < sz - 1; i++)
			s += _argstype[i]->code() + ", ";
		s += _argstype[sz-1]->code();
	}
	s += ")";
	return s;
}

}
