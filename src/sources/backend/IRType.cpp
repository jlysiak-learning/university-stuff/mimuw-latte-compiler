#include <cassert>
#include <latte/backend/IRType.h>

namespace Backend
{


std::shared_ptr<IRType> IRType::from_common_type(std::shared_ptr<Common::Type> ctype)
{
	switch (ctype->type()) {
		case Common::TYPE_VOID:
			return std::make_shared<IRVoid>();

		case Common::TYPE_BOOL:
			return std::make_shared<IRBool>();

		case Common::TYPE_INT:
			return std::make_shared<IRInt>();

		case Common::TYPE_STRING:
			return std::make_shared<IRString>();

		case Common::TYPE_ARRAY:
		case Common::TYPE_CLASS:
			return std::make_shared<IRString>();

		case Common::TYPE_NULL:
		default:
			assert(false);
			return nullptr;
	}
}

}
