#include <latte/backend/IRClassDef.h>
#include <algorithm>
#include <iostream>
#include <cassert>

namespace Backend {

IRClassDef::IRClassDef(std::shared_ptr<Common::ClassDefinition> def) 
{
	if (def->get_methods().size() > 0)
		add_property(std::make_shared<IRString>(), "_vtable");
	_add_from(def);
	_name = def->get_name();
}

void IRClassDef::_add_from(std::shared_ptr<Common::ClassDefinition> def)
{
	if (def->get_parent() != nullptr)
		_add_from(def->get_parent());
	for (auto fld : def->get_fields()) {
		auto type = IRType::from_common_type(fld.second);
		if (type->is<IRClass>())
			type = std::make_shared<IRString>();
		add_property(type, fld.first);
	}
	for (auto mth : def->get_methods()) {
		declare_method(mth.first, def->get_name());

		auto rettype = IRType::from_common_type(mth.second->type());
		if (rettype->is<IRClass>())
			rettype = std::make_shared<IRString>();
		auto irfun = std::make_shared<IRFunction>(
				def->get_name(),
				mth.first, 
				rettype);
		//_functions[irfun->full_name()] = irfun;

		auto v = std::make_shared<IRVar>(std::make_shared<IRString>(), "self");
		irfun->add_arg(v);
		// Get standard arguments
		for (auto arg : mth.second->get_args())  {
			// el <Common:Type, name string> 
			auto v = std::make_shared<IRVar>(IRType::from_common_type(arg.first), arg.second);
			irfun->add_arg(v);
		}
		add_method(irfun);
	}
}

void IRClassDef::add_property(std::shared_ptr<IRType> type, std::string id)
{
	int ord = _propertyOrder.size();
	_propertyOrder.push_back(id);
	_orderMap[id] = ord;
	_typeMap[id] = type;
	_update();
}

void IRClassDef::declare_method(std::string id, std::string scope) {
	auto it = std::find(_methodOrder.begin(), _methodOrder.end(), id);
	if (it == _methodOrder.end()) {
		_methodOrder.push_back(id);
	}
	_methodNameMap[id] = scope + "." + id;
}

void IRClassDef::add_method(std::shared_ptr<IRFunction> def)
{
	_methodMap[def->name()] = def;
}

void IRClassDef::_update()
{
	_align = 8;
	_size = 0;
	for (auto el : _typeMap) 
		_size += el.second->ptr() ? el.second->ptr_size() : el.second->type_size();
}

std::string IRClassDef::type_declaration() const
{
	std::string s = type_label() + " = type { ";

	int l = _propertyOrder.size();
	if (l == 0)
		s += "}";
	else {
		auto i = 0u;
		for (; i < l - 1; i++) {
			auto p = _propertyOrder[i];
			s += _typeMap.at(p)->code() + ", ";
		}
		auto p = _propertyOrder[i];
		s += _typeMap.at(p)->code() + " }";
	}
	return s;
}

std::string IRClassDef::type_label() const {
	std::string s = "%class." + _name;
	return s;
}

std::string IRClassDef::vtable_label() const {
	std::string s = "@vtable." + _name;
	return s;
}

std::string IRClassDef::vtable_type_label() const {
	std::string s = "%vtype." + _name;
	return s;
}

std::string IRClassDef::vtable_typedef() const {
	std::string s = vtable_type_label() + " = type {";
	int i = 0;
	int sz = _methodOrder.size();
	for (; i < sz - 1; i++) {
		s += _methodMap.at(_methodOrder[i])->typestr() + ", ";
	}
	s += _methodMap.at(_methodOrder[i])->typestr() + "}";
	return s;
}

std::string IRClassDef::vtable_definition() const
{
	std::string s = vtable_label() + " = global ";
	s += vtable_type_label() + " {";
	int i = 0;
	int sz = _methodOrder.size();
	for (; i < sz - 1; i++) {
		s += _methodMap.at(_methodOrder[i])->varstr() + ", ";
	}
	s += _methodMap.at(_methodOrder[i])->varstr() + "}";
	return s;
}


std::shared_ptr<IRType> IRClassDef::type_objptr() const 
{
	return std::make_shared<IRClass>(_name);
}

std::shared_ptr<IRType> IRClassDef::vtable_objptr() const 
{
	return std::make_shared<IRVTable>(vtable_type_label());
}

std::shared_ptr<IRVar> IRClassDef::vtable_var() const 
{
	return std::make_shared<IRConst>(std::make_shared<IRVTable>(vtable_type_label()), vtable_label());
}

int IRClassDef::get_method_idx(std::string name) 
{
	for (int i = 0; i < _methodOrder.size(); i++)
		if (_methodOrder[i] == name)
			return i;
	assert(false); //shouldnt happen
	return 0;
}

}
