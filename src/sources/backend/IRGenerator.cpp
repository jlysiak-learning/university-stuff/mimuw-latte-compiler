/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 */

#include <cassert>
#include <iostream>

#include <latte/common/LocalEnvironment.h>
#include <latte/backend/IRGenerator.h>
#include <latte/backend/FunctionDecomposer.h>

#include <latte/backend/IRType.h>
#include <latte/backend/IRClassDef.h>

namespace Backend
{

using namespace Latte;

IRGenerator::IRGenerator(Common::Environment &env, Common::Logger &logger):
	_env(env), _logger(logger) 
{ 

}

void IRGenerator::_create_class_hierarchy()
{
	const auto clsMap = _env.classes();
	for (auto el : clsMap) {
		auto ircls = std::make_shared<IRClassDef>(el.second);
		_irClasses[el.first] = ircls;
		for (auto mth : ircls->methods_map()) {
			_functions[mth.second] = ircls->get_method(mth.first);
		}
	}

	// Add internal ARRAY type representation 
	auto ircls = std::make_shared<IRClassDef>("_array");
	ircls->add_property(std::make_shared<IRInt>(), "length");
	ircls->add_property(std::make_shared<IRString>(), "ptr"); // As i8*, cast later
	_irClasses["_array"] = ircls;

	_logger.debug("Class declarations: ");
	for (auto el : _irClasses) 
		_logger.debug("\t" + el.second->type_declaration() + 
			" - size: " + std::to_string(el.second->size()));
}

void IRGenerator::generate()
{
	LabelGenerator labelGenerator;
	const auto clsMap = _env.classes();
	const auto funMap = _env.functions();

	_create_class_hierarchy();

	for (auto fun : funMap) {
		if (fun.second->isPredefined())
			continue;
		auto irfun = std::make_shared<IRFunction>(
				"",
				fun.first, 
				IRType::from_common_type(fun.second->type()));
		_functions[irfun->full_name()] = irfun;

		for (auto el : fun.second->get_args())  {
			// el <Common:Type, name string> 
			auto v = std::make_shared<IRVar>(IRType::from_common_type(el.first), el.second);
			irfun->add_arg(v);
		}
	}

	// Generate code of class methods
	for (auto el : clsMap) {
		for (auto fun : el.second->get_methods()) {
			// Create local Environment
			auto localEnv = std::make_shared<IREnvironment>(
						_env, 
						el.first,
						_irGlobalStrings,
						_irDeclarations,
						_irClasses,
						labelGenerator);

			_decompose_function(
				std::make_shared<IREnvironment>(*localEnv), 
				fun.second);
		}
	}

	// Generate code of global functions 
	for (auto fun : funMap) {
		if (fun.second->isPredefined())
			continue;
		// Create local Environment
		auto localEnv = std::make_shared<IREnvironment>(
					_env, 
					"",
					_irGlobalStrings,
					_irDeclarations,
					_irClasses,
					labelGenerator);
		_decompose_function(localEnv, fun.second);
	}
}

void IRGenerator::_decompose_function(std::shared_ptr<IREnvironment> irEnv, 
			std::shared_ptr<Common::FunctionDefinition> fun)
{
	FunctionDecomposer funDecomp(irEnv, _logger);
	auto fullname = irEnv->scope() != "" ? irEnv->scope() + "." + fun->get_name() : fun->get_name();
	auto irfun = _functions[fullname];
	assert(irfun != nullptr);
	funDecomp.generate_code(fun, irfun);

}

void IRGenerator::print(std::ostream& output)
{
	output << "; LLVM CODE \n\n";
	output << ";============ GLOBALS AND DECLARATIONS =============\n\n";
	
	for (auto el : _irDeclarations)
		output << *el.second << "\n";
	for (auto el : _irGlobalStrings)
		output << *el.second << "\n";
	for (auto el : _irClasses)
		output << el.second->type_declaration() << "\n";

	output << ";=================== VIRTUAL TABLES ================\n\n";
	for (auto el : _irClasses) {
		if (!el.second->has_vtable())
			continue;
		output << el.second->vtable_typedef() << "\n";
		output << el.second->vtable_definition() << "\n";
	}

	output << "\n;============ CODE =================================\n\n";
		
	for (auto el : _functions) {
		output << "; " << el.first << "\n";
		output << *el.second << "\n";
	}
}

}
