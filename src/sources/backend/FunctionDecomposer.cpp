/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 */

#include <iostream>
#include <sstream>
#include <iomanip>
#include <cassert>

#include <latte/backend/FunctionDecomposer.h>
#include <latte/common/Const.h>

namespace Backend
{

using namespace Latte;

FunctionDecomposer::FunctionDecomposer(
		std::shared_ptr<IREnvironment> env, 
		Common::Logger &logger) :
	_env(env), 
	_logger(logger), 
	_irfun(nullptr), 
	_activeBlock(nullptr),
	_thisClass(nullptr)
{ }

void FunctionDecomposer::generate_code(
			std::shared_ptr<Common::FunctionDefinition> fd,
			std::shared_ptr<IRFunction> irfun)
{
	_logger.debug("Generating code of function: " + _env->scope() + "." + fd->get_name());
	
	_in_loop = 0;

	_irfun = irfun;
	if (_env->scope() != "") 
		_thisClass = _env->get_irclass(_env->scope());
	// Get standard arguments
	for (auto el : fd->get_args()) 
		// el <Common:Type, name string> 
		auto v =_env->add_function_argument(el.first, el.second);
	//=== Create starting block and go...
	_activeBlock = std::make_shared<IRBlock>("entry");
	_irfun->add_block(_activeBlock);
	_decompose(fd->get_body());
}

void FunctionDecomposer::_decompose(Blk* blk)
{
	//=== Allocate mem for fun args
	for (auto arg : _env->function_arguments()) {
		// arg < string, IRVar >
		auto localVar = _env->get_local_variable(arg.first);
		_activeBlock->add_code(std::make_shared<IRCodeAlloc>(localVar, arg.second->type()));
		_activeBlock->add_code(std::make_shared<IRCodeStore>(localVar, arg.second));
	}
	
	// Iterate over statements in block
	// Starts with "ENTRY" block active
	auto oldEnv = _env;
	_env = std::make_shared<IREnvironment>(*_env);
	blk->liststmt_->accept(this);
	_env = oldEnv;
	
	// Add lacking `ret void`
	_logger.debug("Checking function `" + _irfun->full_name() + "` blocks:");
	for (auto pair : _irfun->blocks()) {
		bool x = (pair.second->ends_with_if() || pair.second->ends_with_goto());
		bool y = (pair.second->ends_with_ret() || pair.second->ends_with_retvoid());
		if ((x && !y) || (!x && y))
			_logger.debug("Block `" + pair.first + "` -- OK");
		else {
			_logger.debug("Block `" + pair.first + "` -- Adding `ret void`");
			pair.second->add_code(std::make_shared<IRCodeRetV>());
		}
	}
}

std::shared_ptr<IRVar> FunctionDecomposer::_load_if_needed(Expr *expr) {
	if (expr->var->ptr()) {  
		auto var = _env->next_var(expr->var);
		var->no_ptr();
		_activeBlock->add_code(std::make_shared<IRCodeLoad>(var, expr->var));
		return var;
	}
	return expr->var;
}

std::shared_ptr<IRVar> FunctionDecomposer::_generate_default_initializer(std::shared_ptr<IRVar> var)
{
	if (var->type()->is<IRInt>()) {
		return std::make_shared<IRConst>(0); 
	} else if (var->type()->is<IRBool>()) {
		return std::make_shared<IRConst>(false);
	} else if (var->type()->is<IRString>()) {
		auto globalVar = std::dynamic_pointer_cast<IRGlobalString>(_env->add_global_string("_defaultString", ""));
		auto v = _env->next_var(std::make_shared<IRString>());
		_activeBlock->add_code(std::make_shared<IRCodeGetGlobalStr>(v, globalVar));
		return v;
	}
	assert(false);
	return nullptr;
}

std::shared_ptr<IRVar> FunctionDecomposer::_alloca(std::shared_ptr<IRType> what_type)
{
	auto where_type = what_type->clone();
	where_type->set_ptr(true);
	auto var_where = _env->next_var(where_type);
	auto code_alloc = std::make_shared<IRCodeAlloc>(var_where, what_type);
	if (_in_loop > 0)
		_allocs.push_back(code_alloc);
	else
		_activeBlock->add_code(code_alloc);
	return var_where;
}

std::shared_ptr<IRVar> FunctionDecomposer::_malloc(int n)
{
	auto fname = "_calloc";
	auto fundecl = std::make_shared<IRDeclaration>(fname, std::make_shared<IRString>());
	auto r = _env->next_var(std::make_shared<IRString>());

	auto code = std::make_shared<IRCodeCall>(r, fname);
	
	auto var_sz = std::make_shared<IRConst>(n);
	code->add_arg(var_sz);
	fundecl->add_arg(var_sz->type());

	_activeBlock->add_code(code);
	_env->add_runtime_function(fname, fundecl);
	_irfun->register_function_call(fname);
	return r;
}

std::shared_ptr<IRVar> FunctionDecomposer::_malloc(std::shared_ptr<IRVar> var_n, int el_sz)
{
	auto fname = "_calloc";
	auto fundecl = std::make_shared<IRDeclaration>(fname, std::make_shared<IRString>());
	auto r = _env->next_var(std::make_shared<IRString>());
	auto code_call = std::make_shared<IRCodeCall>(r, fname);
	
	auto var_sz = _env->next_var(std::make_shared<IRInt>());
	auto var_el_sz =std::make_shared<IRConst>(el_sz);
	auto code_mul = std::make_shared<IRCodeMul>(var_sz, Common::MULOP_MUL, var_n, var_el_sz);

	code_call->add_arg(var_sz);
	fundecl->add_arg(var_sz->type());

	_activeBlock->add_code(code_mul);
	_activeBlock->add_code(code_call);
	_env->add_runtime_function(fname, fundecl);
	_irfun->register_function_call(fname);
	return r;
}

std::shared_ptr<IRVar> FunctionDecomposer::_cast(
				std::shared_ptr<IRVar> what, 
				std::shared_ptr<IRType> to_type)
{
	auto var_where = _env->next_var(to_type);
	auto code = std::make_shared<IRCodeCast>(var_where, what);
	_activeBlock->add_code(code);
	return var_where;
}

void FunctionDecomposer::_store(std::shared_ptr<IRVar> what, 
				std::shared_ptr<IRVar> where)
{
	auto code = std::make_shared<IRCodeStore>(where, what);
	_activeBlock->add_code(code);
}

std::shared_ptr<IRVar> FunctionDecomposer::_get_class_property(
				std::shared_ptr<IRVar> objptr, 
				std::string base_label,
				std::shared_ptr<IRType> prop_type, 
				int order)
{
	auto var_prop_ptr = _env->next_var(prop_type);
	var_prop_ptr->to_ptr();
	auto code = std::make_shared<IRCodeGetProperty>(var_prop_ptr, objptr, base_label, order);
	_activeBlock->add_code(code);
	return var_prop_ptr;
}

std::shared_ptr<IRVar> FunctionDecomposer::_load(std::shared_ptr<IRVar> var_ptr2)
{
	assert(var_ptr2->ptr());
	auto var_el = _env->next_var(var_ptr2);
	var_el->no_ptr();
	auto code = std::make_shared<IRCodeLoad>(var_el, var_ptr2);
	_activeBlock->add_code(code);
	return var_el;
}

std::shared_ptr<IRVar> FunctionDecomposer::_getelementptr(
				std::shared_ptr<IRVar> var_base_ptr,
				std::shared_ptr<IRVar> var_idx)
{
	assert(var_base_ptr->ptr()); // should be a ptr
	auto el_ptr = _env->next_var(var_base_ptr);

	auto code = std::make_shared<IRCodeGetElem>(el_ptr, var_base_ptr, var_idx);
	_activeBlock->add_code(code);
	return el_ptr;
}

//=============================================================================

void FunctionDecomposer::visitListStmt(ListStmt *list_stmt)
{
	for (ListStmt::iterator i = list_stmt->begin() ; i != list_stmt->end() ; ++i)
	{
		(*i)->accept(this);
		if (_activeBlock == nullptr)
			// Block contains while(true) loop or RET
			// Nothing to do
			return;
	}
}

void FunctionDecomposer::visitBStmt(BStmt *b_stmt)
{
	// Going to visit new block of code, it's just new variable scope
	auto oldEnv = _env;
	_env = std::make_shared<IREnvironment>(*_env);
	b_stmt->block_->accept(this);	
	_env = oldEnv;
}

//=== DECLARATION

void FunctionDecomposer::visitDecl(Decl *decl)
{
	// Checks were done by Typechecker
	auto type = Common::Type::from_type_AST(decl->type_);
	_logger.debug_at(decl->line_number, "Declaration of type: " + type->str());
	_inType = type;
	decl->listdeclitem_->accept(this);
	_inType = nullptr;
}

void FunctionDecomposer::visitInit(Init *init)
{
	_logger.debug("Initializing variable: " + init->ident_);
	init->expr_->accept(this);

	std::shared_ptr<IRVar> varloc;
	auto v = _load_if_needed(init->expr_);
	if (_inType->is<Common::Class>() || _inType->is<Common::Array>())
		varloc = _env->add_local_variable(std::make_shared<Common::String>(), init->ident_);
	else 
		varloc = _env->add_local_variable(_inType, init->ident_);
	auto code_alloc = std::make_shared<IRCodeAlloc>(varloc, v->type());
	if (_in_loop > 0) {
		_allocs.push_back(code_alloc);
	} else 
		_activeBlock->add_code(code_alloc);
	_activeBlock->add_code(std::make_shared<IRCodeStore>(varloc, v));
}


void FunctionDecomposer::visitNoInit(NoInit *no_init)
{
	std::shared_ptr<IRVar> var;
	std::shared_ptr<IRVar> defaultVar;
	if (_inType->is<Common::Class>() || _inType->is<Common::Array>()) {
		var = _env->add_local_variable(std::make_shared<Common::String>(), no_init->ident_);
		auto var_null = std::make_shared<IRNull>();
		auto var_type = std::make_shared<IRString>();
		defaultVar =  _cast(var_null, var_type);
	} else  {
		var = _env->add_local_variable(_inType, no_init->ident_);
		defaultVar = _generate_default_initializer(var);
	}
	auto code_alloc = std::make_shared<IRCodeAlloc>(var, defaultVar->type());
	if (_in_loop > 0) {
		_allocs.push_back(code_alloc);
	} else 
		_activeBlock->add_code(code_alloc);
	_activeBlock->add_code(std::make_shared<IRCodeStore>(var, defaultVar));
}

//================

void FunctionDecomposer::visitAssign(Assign *assign)
{
	// Generate code to get variable location
	assign->expr_1->accept(this);
	assert(assign->expr_1->var->ptr()); // must be a pointer
	// Generate code to get new value
	assign->expr_2->accept(this);

	auto var = _load_if_needed(assign->expr_2);
	_activeBlock->add_code(std::make_shared<IRCodeStore>(assign->expr_1->var, var)); 
}

void FunctionDecomposer::visitIncr(Incr *incr)
{
	incr->expr_->accept(this);
	assert(incr->expr_->var->ptr());
	auto var = _load_if_needed(incr->expr_);
	auto res = _env->next_var(var); 
	_activeBlock->add_code(std::make_shared<IRCodeAdd>(res, Common::ADDOP_ADD, var, std::make_shared<IRConst>(1)));
	_activeBlock->add_code(std::make_shared<IRCodeStore>(incr->expr_->var, res));
}

void FunctionDecomposer::visitDecr(Decr *decr)
{
	decr->expr_->accept(this);
	assert(decr->expr_->var->ptr());
	auto var = _load_if_needed(decr->expr_);
	auto res = _env->next_var(var); 
	_activeBlock->add_code(std::make_shared<IRCodeAdd>(res, Common::ADDOP_SUB, var, std::make_shared<IRConst>(1)));
	_activeBlock->add_code(std::make_shared<IRCodeStore>(decr->expr_->var, res));
}



void FunctionDecomposer::visitRet(Ret *ret)
{
	ret->expr_->accept(this);
	auto var = _load_if_needed(ret->expr_);
	_activeBlock->add_code(std::make_shared<IRCodeRet>(var));
	// That's end, no more code can be generated
	_activeBlock = nullptr;
}

void FunctionDecomposer::visitVRet(VRet *v_ret)
{
	_activeBlock->add_code(std::make_shared<IRCodeRetV>());
	// That's end, no more code can be generated
	_activeBlock = nullptr;
}

void FunctionDecomposer::visitWhile(While *while_)
{
	if (while_->expr_->isConst) {
		if (while_->expr_->const_->b()) {
			// Ok, code will be always executed, ommit condition
			auto lloop = _env->next_block_label();
			auto loopblock = std::make_shared<IRBlock>(lloop);
			auto tmp = _activeBlock;
			loopblock->add_entry_label(_activeBlock->label());
			_irfun->add_block(loopblock);
			_activeBlock = loopblock;

			_in_loop += 1;
			// Typechecker checked prohibited declarations without new scope
			while_->stmt_->accept(this); // Generate while code
			_in_loop -= 1;
			if (_in_loop == 0) {
				for (auto x : _allocs)
					tmp->add_code(x);
				_allocs.clear();
			}
			tmp->add_code(std::make_shared<IRCodeGoto>(lloop));
			if (_activeBlock == nullptr)
				// Nested block contains while(true) loop
				// Nothing to do
				return;

			// Now _activeBlock can be any.
			// Because while COND is TRUE, go back to begining of LOOP block
			_activeBlock->add_code(std::make_shared<IRCodeGoto>(lloop));
			loopblock->add_entry_label(_activeBlock->label());

			// That's end, no more code can be generated
			_activeBlock = nullptr;
			return;
		} else {
			// Code will never be executed, skip
			return;
		}
	}
	// Will or will not be executed...
	auto lcond = _env->next_block_label();
	auto lend = _env->next_block_label();
	auto lloop = _env->next_block_label();

	auto condblock = std::make_shared<IRBlock>(lcond);
	auto endblock = std::make_shared<IRBlock>(lend);
	auto loopblock = std::make_shared<IRBlock>(lloop);

	auto tmp = _activeBlock;
	condblock->add_entry_label(_activeBlock->label());
	_irfun->add_block(condblock);
	_activeBlock = condblock;

	while_->expr_->accept(this); // compute expression

	if (while_->expr_->var->ptr()) { 
		// Expression is pointer to boolean variable, cannot be used directly
		// It needs to be load from memory
		auto var = _env->next_var(IRType::from_common_type(while_->expr_->type));
		_activeBlock->add_code(std::make_shared<IRCodeLoad>(var, while_->expr_->var));
		_activeBlock->add_code(std::make_shared<IRCodeIf>(var, lloop, lend)); 
	} else
		// Expr was already a computed value, just use it
		_activeBlock->add_code(std::make_shared<IRCodeIf>(while_->expr_->var, lloop, lend)); 

	loopblock->add_entry_label(_activeBlock->label()); // enter on TRUE evaluation
	endblock->add_entry_label(_activeBlock->label()); // enter on FALSE evaluation

	_irfun->add_block(loopblock);
	_activeBlock = loopblock;

	_in_loop += 1;
	while_->stmt_->accept(this);
	_in_loop -= 1;
	if (_in_loop == 0) {
		for (auto x : _allocs)
			tmp->add_code(x);
		_allocs.clear();
	}
	tmp->add_code(std::make_shared<IRCodeGoto>(lcond));

	// Loop ended, back to LCOND
	_activeBlock->add_code(std::make_shared<IRCodeGoto>(lcond));
	condblock->add_entry_label(_activeBlock->label());

	// Done, continue in LEND block
	_irfun->add_block(endblock);
	_activeBlock = endblock;
}

void FunctionDecomposer::visitFor(For *for_)
{
	for_->expr_->accept(this);
	auto iter_name = for_->ident_;
	auto var_arr = _load_if_needed(for_->expr_);
	auto ircls = _env->get_irclass("_array");
	var_arr = _cast(var_arr, ircls->type_objptr());
	auto var_len_ptr = _get_class_property(
					var_arr,
					ircls->type_label(), 
					ircls->get_property_type("length"),
					ircls->get_property_num("length"));
	auto var_len = _load(var_len_ptr); // For loop guard
	
	//------------- Blocks 
	auto lcond = _env->next_block_label();
	auto lloop = _env->next_block_label();
	auto lend = _env->next_block_label();
	// COND block
	auto bcond = std::make_shared<IRBlock>(lcond);
	_irfun->add_block(bcond);
	bcond->add_entry_label(_activeBlock->label());
	bcond->add_entry_label(lloop);

	auto var_iter_loc = _alloca(IRType::from_common_type(Common::Type::from_type_AST(for_->type_)));
	auto tmp = _activeBlock;

	// LOOP block
	auto bloop = std::make_shared<IRBlock>(lloop);
	_irfun->add_block(bloop);
	bloop->add_entry_label(lcond);

	// END block
	auto bend = std::make_shared<IRBlock>(lend);
	_irfun->add_block(bend);
	bend->add_entry_label(lcond);

	//------------ Conditions
	auto var_cond_i = _env->next_var(std::make_shared<IRInt>());
	auto var_loop_i = _env->next_var(std::make_shared<IRInt>());
	auto phi = std::make_shared<IRCodePhi>(var_cond_i);
	phi->add(std::make_shared<IRConst>(0), _activeBlock->label());
	
	auto var_cmp = _env->next_var(std::make_shared<IRBool>());
	auto bcond_code0 = std::make_shared<IRCodeRel>(
				var_cmp, 
				Common::RELOP_LTH,
				var_cond_i,
				var_len);
	auto bcond_code1 = std::make_shared<IRCodeIf>(var_cmp, lloop, lend);
	
	_activeBlock = bloop;
	auto var_ptr_ptr = _get_class_property(
					var_arr,
					ircls->type_label(), 
					ircls->get_property_type("ptr"),
					ircls->get_property_num("ptr"));
	auto var_base_ptr = _load(var_ptr_ptr);
	auto eltype_ptr= IRType::from_common_type(Common::Type::from_type_AST(for_->type_));
	eltype_ptr->set_ptr(true);
	var_base_ptr = _cast(var_base_ptr, eltype_ptr);
	auto var_el_ptr = _getelementptr(var_base_ptr, var_cond_i);
	auto var_el = _load(var_el_ptr);
	_store(var_el, var_iter_loc);
	auto oldEnv = _env;
	_env = std::make_shared<IREnvironment>(*_env);
	_env->add_local_variable(var_iter_loc, iter_name);
	// build LOOP block
	_in_loop += 1;
	for_->stmt_->accept(this);
	_env = oldEnv;
	_in_loop -= 1;
	if (_in_loop == 0) {
		for (auto x : _allocs)
			tmp->add_code(x);
		_allocs.clear();
	}
	tmp->add_code(std::make_shared<IRCodeGoto>(lcond));

	phi->add(var_loop_i, _activeBlock->label()); // We add second phi entry here because for stmts can create new blocks 
	bcond->add_code(phi);
	bcond->add_code(bcond_code0);
	bcond->add_code(bcond_code1);

	_activeBlock->add_code(std::make_shared<IRCodeAdd>(
					var_loop_i, 
					Common::ADDOP_ADD, 
					var_cond_i, 
					std::make_shared<IRConst>(1)));
	_activeBlock->add_code(std::make_shared<IRCodeGoto>(lcond));

	_activeBlock = bend;
}

void FunctionDecomposer::visitIf(If *if_)
{
	int line = if_->line_number;
	_logger.debug_at(line, "STATEMENT IF ---");
	if_->expr_->accept(this); // Maybe const but expressions can have side effects

	if (if_->expr_->isConst) {
		if (if_->expr_->const_->b()) {
			// Always execute IF branch
			if_->stmt_->accept(this);
			return;
		} else {
			// Never execute...
			return;
		}
	}

	auto lt = _env->next_block_label();
	auto la = _env->next_block_label();

	auto tblock = std::make_shared<IRBlock>(lt);
	auto ablock = std::make_shared<IRBlock>(la);

	if (if_->expr_->var->ptr()) { 
		// Expression is pointer to boolean variable, cannot be used directly
		// It needs to be load from memory
		auto var = _env->next_var(IRType::from_common_type(if_->expr_->type));
		_activeBlock->add_code(std::make_shared<IRCodeLoad>(var, if_->expr_->var));
		_activeBlock->add_code(std::make_shared<IRCodeIf>(var, lt, la)); 
	} else
		// Expr was already a computed value, just use it
		_activeBlock->add_code(std::make_shared<IRCodeIf>(if_->expr_->var, lt, la)); 
	ablock->add_entry_label(_activeBlock->label());
	tblock->add_entry_label(_activeBlock->label());
	
	_irfun->add_block(tblock);
	_activeBlock = tblock;
	if_->stmt_->accept(this);
	if (_activeBlock != nullptr) {
		// If we can back from block, add goto 
		_activeBlock->add_code(std::make_shared<IRCodeGoto>(la));
		ablock->add_entry_label(_activeBlock->label());
	}
	// If we cannot back just move to next block
	
	_irfun->add_block(ablock);
	_activeBlock = ablock;
}

void FunctionDecomposer::visitIfElse(IfElse *if_else)
{
	int line = if_else->line_number;
	_logger.debug_at(line, "STATEMENT IF-ELSE ---");

	if_else->expr_->accept(this);
	if (if_else->expr_->isConst) {
		_logger.debug_at(line, "--- CONST");
		if (if_else->expr_->const_->b()) {
			// Always execute IF branch
			if_else->stmt_1->accept(this);
			return;
		} else {
			// Always execute ELSE branch
			if_else->stmt_2->accept(this);
			return;
		}
	}
	_logger.debug_at(line, "--- NOT CONST");

	auto lt = _env->next_block_label();
	auto lf = _env->next_block_label();
	auto la = _env->next_block_label();

	auto tblock = std::make_shared<IRBlock>(lt);
	auto fblock = std::make_shared<IRBlock>(lf);
	auto ablock = std::make_shared<IRBlock>(la);

	if (if_else->expr_->var->ptr()) { 
		// Expression is pointer to boolean variable, cannot be used directly
		// It needs to be load from memory
		auto var = _env->next_var(IRType::from_common_type(if_else->expr_->type));
		_activeBlock->add_code(std::make_shared<IRCodeLoad>(var, if_else->expr_->var));
		_activeBlock->add_code(std::make_shared<IRCodeIf>(var, lt, lf)); 
	} else
		// Expr was already a computed value, just use it
		_activeBlock->add_code(std::make_shared<IRCodeIf>(if_else->expr_->var, lt, lf)); 

	// Enter from actuall block into T/F branches
	tblock->add_entry_label(_activeBlock->label());
	fblock->add_entry_label(_activeBlock->label());
	_irfun->add_block(tblock);
	_irfun->add_block(fblock);

	//==== TRUE BLOCK
	_activeBlock = tblock;
	if_else->stmt_1->accept(this);
	if (_activeBlock != nullptr) {
		_activeBlock->add_code(std::make_shared<IRCodeGoto>(la));
		ablock->add_entry_label(_activeBlock->label());
	} 
	_activeBlock = fblock;
	if_else->stmt_2->accept(this);
	if (_activeBlock != nullptr) {
		_activeBlock->add_code(std::make_shared<IRCodeGoto>(la));
		ablock->add_entry_label(_activeBlock->label());
	}

	if (ablock->entry_labels().size() == 0) {
		// No exit from both branches
		_activeBlock = nullptr;
	} else {
		_irfun->add_block(ablock);
		_activeBlock = ablock;
	}
}

void FunctionDecomposer::visitExp(Exp *exp)
{
	exp->expr_->accept(this);
}


void FunctionDecomposer::visitBlk(Blk *blk)
{
	/* Code For Blk Goes Here */

	blk->liststmt_->accept(this);

}

//=============================================================================
//=========  EXPRESSIONS  =====================================================
//=============================================================================
void FunctionDecomposer::visitELitInt(ELitInt *e_lit_int)
{
	e_lit_int->var = std::make_shared<IRConst>(e_lit_int->integer_);
}

void FunctionDecomposer::visitELitTrue(ELitTrue *e_lit_true)
{
	e_lit_true->var = std::make_shared<IRConst>(true);
}

void FunctionDecomposer::visitELitFalse(ELitFalse *e_lit_false)
{
	e_lit_false->var = std::make_shared<IRConst>(false);
}

void FunctionDecomposer::visitEString(EString *e_string)
{
	auto globalVar = std::dynamic_pointer_cast<IRGlobalString>(_env->add_global_string(e_string->string_));
	auto v = _env->next_var(std::make_shared<IRString>());
	_activeBlock->add_code(std::make_shared<IRCodeGetGlobalStr>(v, globalVar));
	e_string->var = v;
}

void FunctionDecomposer::visitEVar(EVar *e_var)
{
	_logger.debug_at(e_var->line_number, "Visiting EVar: " + e_var->ident_);
	auto var = _env->get_local_variable(e_var->ident_);
	if (var != nullptr) {
		e_var->var = var;
	} else if (_thisClass != nullptr) {
		auto name = e_var->ident_;
		auto generic_self = std::make_shared<IRVar>(std::make_shared<IRString>(), "self");
		if (name == "self") {
			e_var->var = generic_self;
			return;
		}
		if (_thisClass->has_property(name)) {
			auto self = _cast(generic_self, _thisClass->type_objptr());
			e_var->var = _get_class_property(
							self,
							_thisClass->type_label(), 
							_thisClass->get_property_type(name),
							_thisClass->get_property_num(name));
			return;
		}
	} else {
		// Shouldn't happen
		assert(false);
	}
}

void FunctionDecomposer::visitENull(ENull *e_null)
{
	e_null->var = std::make_shared<IRNull>(); 
}


void FunctionDecomposer::visitEFunCall(EFunCall *e_fun_call)
{
	auto fundecl = std::make_shared<IRDeclaration>(e_fun_call->ident_, IRType::from_common_type(e_fun_call->type));
	auto r = _env->next_var(IRType::from_common_type(e_fun_call->type));
	auto code = std::make_shared<IRCodeCall>(r, e_fun_call->ident_);
	e_fun_call->var = r;
	for (ListExpr::iterator i = e_fun_call->listexpr_->begin() ; i != e_fun_call->listexpr_->end() ; ++i)
	{
		(*i)->accept(this);
		auto v = _load_if_needed(*i);
		code->add_arg(v);
		fundecl->add_arg(v->type());
	}
	_activeBlock->add_code(code);
	// Try, maybe it's in runtime  lib
	_env->add_runtime_function(e_fun_call->ident_, fundecl);
	_irfun->register_function_call(e_fun_call->ident_);
}

void FunctionDecomposer::visitNeg(Neg *neg)
{
	if (neg->isConst) {
		neg->var = std::make_shared<IRConst>(neg->const_->i());
		return;
	}
	neg->expr_->accept(this);
	auto v = _load_if_needed(neg->expr_);
	auto r = _env->next_var(std::make_shared<IRInt>());
	neg->var = r;
	_activeBlock->add_code(std::make_shared<IRCodeAdd>(
		r, 
		Common::ADDOP_SUB,
		std::make_shared<IRConst>(0),
		v));
}

void FunctionDecomposer::visitNot(Not *not_)
{
	if (not_->isConst) {
		not_->var = std::make_shared<IRConst>(not_->const_->b());
		return;
	}
	not_->expr_->accept(this);
	auto v = _load_if_needed(not_->expr_);
	auto r = _env->next_var(std::make_shared<IRBool>());
	not_->var = r;
	_activeBlock->add_code(std::make_shared<IRCodeNot>(r, v));
}

void FunctionDecomposer::visitEMul(EMul *e_mul)
{
	if (e_mul->isConst) {
		e_mul->var = std::make_shared<IRConst>(e_mul->const_->i());
		return;
	}

	e_mul->expr_1->accept(this);
	e_mul->expr_2->accept(this);

	auto v1 = _load_if_needed(e_mul->expr_1);
	auto v2 = _load_if_needed(e_mul->expr_2);
	auto r = _env->next_var(std::make_shared<IRInt>());

	e_mul->var = r;
	_activeBlock->add_code(std::make_shared<IRCodeMul>(
		r, 
		e_mul->mulop_->op,
		v1,
		v2));
}

void FunctionDecomposer::visitEAdd(EAdd *e_add)
{
	if (e_add->isConst) {
		if (e_add->type->is<Common::String>()) {
			auto globalVar = std::dynamic_pointer_cast<IRGlobalString>(
						_env->add_global_string(e_add->const_->s())
					);
			auto v = _env->next_var(std::make_shared<IRString>());
			_activeBlock->add_code(std::make_shared<IRCodeGetGlobalStr>(v, globalVar));
			e_add->var = v;
		 } else
			e_add->var = std::make_shared<IRConst>(e_add->const_->i());
		return;
	}

	e_add->expr_1->accept(this);
	e_add->expr_2->accept(this);
	auto v1 = _load_if_needed(e_add->expr_1);
	auto v2 = _load_if_needed(e_add->expr_2);

	if (e_add->type->is<Common::String>()) {
		auto r = _env->next_var(std::make_shared<IRString>());
		e_add->var = r;

		auto fundecl = std::make_shared<IRDeclaration>("_concat", std::make_shared<IRString>());
		fundecl->add_arg(std::make_shared<IRString>());
		fundecl->add_arg(std::make_shared<IRString>());
		_env->add_runtime_function("_concat", fundecl);
		_irfun->register_function_call("_concat");

		auto code = std::make_shared<IRCodeCall>(r, "_concat");
		code->add_arg(v1);
		code->add_arg(v2);
		_activeBlock->add_code(code);
	} else {
		auto r = _env->next_var(std::make_shared<IRInt>());
		e_add->var = r;
		_activeBlock->add_code(std::make_shared<IRCodeAdd>(
			r, 
			e_add->addop_->op,
			v1,
			v2));
	}

}

void FunctionDecomposer::visitERel(ERel *e_rel)
{
	if (e_rel->isConst) {
		e_rel->var = std::make_shared<IRConst>(e_rel->const_->b());
		return;
	}

	e_rel->expr_1->accept(this);
	e_rel->expr_2->accept(this);
	auto v1 = _load_if_needed(e_rel->expr_1);
	auto v2 = _load_if_needed(e_rel->expr_2);
	auto r = _env->next_var(std::make_shared<IRBool>());
	e_rel->var = r;

	if (e_rel->expr_1->type->is<Common::String>()) { // Call string CMP
		std::string fname = e_rel->relop_->op == Common::RELOP_EQU ? "_strcmp" : "_strcmpn";

		auto fundecl = std::make_shared<IRDeclaration>(fname, std::make_shared<IRBool>());
		fundecl->add_arg(std::make_shared<IRString>());
		fundecl->add_arg(std::make_shared<IRString>());
		_env->add_runtime_function(fname, fundecl);
		_irfun->register_function_call(fname);

		auto code = std::make_shared<IRCodeCall>(r, fname);
		code->add_arg(v1);
		code->add_arg(v2);
		_activeBlock->add_code(code);
	} else if (e_rel->expr_1->type->is<Common::Array>() || e_rel->expr_1->type->is<Common::Class>()) {
		assert(e_rel->relop_->op == Common::RELOP_EQU ||
			e_rel->relop_->op == Common::RELOP_NE);
		auto v1_i8p = _cast(v1, std::make_shared<IRString>());
		auto v2_i8p = _cast(v2, std::make_shared<IRString>());
		_activeBlock->add_code(std::make_shared<IRCodeRel>(
			r, 
			e_rel->relop_->op,
			v1,
			v2));
	} else {
		_activeBlock->add_code(std::make_shared<IRCodeRel>(
			r, 
			e_rel->relop_->op,
			v1,
			v2));
	}
}

void FunctionDecomposer::visitEAnd(EAnd *e_and)
{
	int line = e_and->line_number;
	if (e_and->isConst) {
		_logger.debug_at(line, "AND CONST");
		e_and->var = std::make_shared<IRConst>(e_and->const_->b());
		return;
	}

	e_and->expr_1->accept(this);
	if (e_and->expr_1->isConst && e_and->expr_1->const_->b()) {
		e_and->expr_2->accept(this);
		auto v2 = _load_if_needed(e_and->expr_2);
		e_and->var = v2;
		e_and->isConst = e_and->expr_2->isConst;
		e_and->const_ = e_and->expr_2->const_;
	} else if (e_and->expr_1->isConst && !e_and->expr_1->const_->b()) {
		e_and->var = e_and->expr_1->var;
		e_and->isConst = false;
		e_and->const_ = std::make_shared<Common::Const>(false);
	} else if (e_and->expr_2->isConst && e_and->expr_2->const_->b()) {
		e_and->var = e_and->expr_1->var;
		e_and->isConst = e_and->expr_1->isConst;
		e_and->const_ = e_and->expr_1->const_;
	} else if (e_and->expr_2->isConst && !e_and->expr_2->const_->b()) {
		e_and->var = std::make_shared<IRConst>(false);
		e_and->isConst = false;
		e_and->const_ = std::make_shared<Common::Const>(false);
	} else {
		auto lsnd = _env->next_block_label();
		auto lend = _env->next_block_label();
		auto bsnd = std::make_shared<IRBlock>(lsnd);
		auto bend = std::make_shared<IRBlock>(lend);
		auto v1 = _load_if_needed(e_and->expr_1);
		_activeBlock->add_code(std::make_shared<IRCodeIf>(v1, lsnd, lend));
		auto tmp = _activeBlock;

		bsnd->add_entry_label(_activeBlock->label());
		_activeBlock = bsnd;
		e_and->expr_2->accept(this);
		auto v2 = _load_if_needed(e_and->expr_2);
		_activeBlock->add_code(std::make_shared<IRCodeGoto>(lend));


		bend->add_entry_label(tmp->label());
		bend->add_entry_label(_activeBlock->label());

		auto r = _env->next_var(std::make_shared<IRBool>());
		auto phi = std::make_shared<IRCodePhi>(r);
		phi->add(std::make_shared<IRConst>(false), tmp->label());
		phi->add(v2, _activeBlock->label());
		bend->add_code(phi);

		e_and->var = r;
		_irfun->add_block(bsnd);
		_irfun->add_block(bend);
		_activeBlock = bend;
	}
}

void FunctionDecomposer::visitEOr(EOr *e_or)
{
	int line = e_or->line_number;
	if (e_or->isConst) {
		_logger.debug_at(line, "OR CONST");
		e_or->var = std::make_shared<IRConst>(e_or->const_->b());
		return;
	}

	e_or->expr_1->accept(this);
	if (e_or->expr_1->isConst && e_or->expr_1->const_->b()) {
		_logger.debug_at(line, "OR EXPR1 = TRUE");
		e_or->var = e_or->expr_1->var;
		e_or->isConst = true;
		e_or->const_ = std::make_shared<Common::Const>(true);
	} else if (e_or->expr_1->isConst && !e_or->expr_1->const_->b()) {
		e_or->expr_2->accept(this);
		auto v2 = _load_if_needed(e_or->expr_2);
		e_or->var = v2;
		e_or->isConst = e_or->expr_2->isConst;
		e_or->const_ = e_or->expr_2->const_;
	} else if (e_or->expr_2->isConst && e_or->expr_2->const_->b()) {
		e_or->var = std::make_shared<IRConst>(true);
		e_or->isConst = true;
		e_or->const_ = std::make_shared<Common::Const>(true);
	} else if (e_or->expr_2->isConst && !e_or->expr_2->const_->b()) {
		e_or->var = e_or->expr_1->var;
		e_or->isConst = e_or->expr_1->isConst;
		e_or->const_ = e_or->expr_1->const_;
	} else {
		auto lsnd = _env->next_block_label();
		auto lend = _env->next_block_label();
		auto bsnd = std::make_shared<IRBlock>(lsnd);
		auto bend = std::make_shared<IRBlock>(lend);
		auto v1 = _load_if_needed(e_or->expr_1);
		_activeBlock->add_code(std::make_shared<IRCodeIf>(v1, lend, lsnd));
		auto tmp = _activeBlock;

		bsnd->add_entry_label(_activeBlock->label());
		_activeBlock = bsnd;
		e_or->expr_2->accept(this);
		auto v2 = _load_if_needed(e_or->expr_2);
		_activeBlock->add_code(std::make_shared<IRCodeGoto>(lend));


		bend->add_entry_label(tmp->label());
		bend->add_entry_label(_activeBlock->label());

		auto r = _env->next_var(std::make_shared<IRBool>());
		auto phi = std::make_shared<IRCodePhi>(r);
		phi->add(std::make_shared<IRConst>(true), tmp->label());
		phi->add(v2, _activeBlock->label());
		bend->add_code(phi);

		e_or->var = r;
		_irfun->add_block(bsnd);
		_irfun->add_block(bend);
		_activeBlock = bend;
	}
}

//=============================================================================

void FunctionDecomposer::visitENewArr(ENewArr *e_new_arr)
{
	// Evaluate size of array
	e_new_arr->expr_->accept(this);
	auto var_n = _load_if_needed(e_new_arr->expr_);
	// Now `var` is correct i32 const or var-label

	auto ircls = _env->get_irclass("_array");
	auto var_mem_i8ptr = _malloc(ircls->size());
	auto var_mem_objptr = _cast(var_mem_i8ptr, ircls->type_objptr());
	auto var_len_ptr = _get_class_property(
					var_mem_objptr, 
					ircls->type_label(), 
					ircls->get_property_type("length"),
					ircls->get_property_num("length"));
	_store(var_n, var_len_ptr);
	auto var_ptr_ptr = _get_class_property(
					var_mem_objptr, 
					ircls->type_label(), 
					ircls->get_property_type("ptr"),
					ircls->get_property_num("ptr"));

	auto eltype = std::dynamic_pointer_cast<Common::Array>(e_new_arr->type)->elem_type();
	int sz = IRType::from_common_type(eltype)->type_size();
	auto var_ptr_i8ptr = _malloc(var_n, sz);
	_store(var_ptr_i8ptr, var_ptr_ptr);
	
	e_new_arr->var = var_mem_i8ptr;
}

void FunctionDecomposer::visitENewCls(ENewCls *e_new_cls)
{
	auto name = e_new_cls->ident_;
	auto ircls = _env->get_irclass(name);
	auto var_mem_i8ptr = _malloc(ircls->size());
	if (ircls->has_vtable()) {
		auto var_mem_objptr = _cast(var_mem_i8ptr, ircls->type_objptr());
		auto var_vtable_loc = _get_class_property(
						var_mem_objptr, 
						ircls->type_label(), 
						ircls->get_property_type("_vtable"),
						ircls->get_property_num("_vtable"));
		auto var_vtable = ircls->vtable_var();
		auto var_vtable_i8ptr = _cast(var_vtable, std::make_shared<IRString>());
		_store(var_vtable_i8ptr, var_vtable_loc);
	}
	e_new_cls->var = var_mem_i8ptr;
}

void FunctionDecomposer::visitEAt(EAt *e_at)
{
	e_at->expr_1->accept(this);
	auto arr_obj_ptr = _load_if_needed(e_at->expr_1);
	auto ircls = _env->get_irclass("_array");
	arr_obj_ptr = _cast(arr_obj_ptr, ircls->type_objptr());
	auto var_ptr_ptr = _get_class_property(
					arr_obj_ptr, 
					ircls->type_label(), 
					ircls->get_property_type("ptr"),
					ircls->get_property_num("ptr"));
	auto var_base_ptr = _load(var_ptr_ptr);
	std::shared_ptr<IRType> eltype_ptr;

	if (e_at->type->is<Common::Class>() || e_at->type->is<Common::Array>())
		eltype_ptr= std::make_shared<IRString>();
	else 
		eltype_ptr= IRType::from_common_type(e_at->type);
	eltype_ptr->set_ptr(true);
	var_base_ptr = _cast(var_base_ptr, eltype_ptr);

	e_at->expr_2->accept(this);
	auto var_idx = _load_if_needed(e_at->expr_2);

	auto var_el_ptr = _getelementptr(var_base_ptr, var_idx);
	e_at->var = var_el_ptr;
}

std::shared_ptr<IRVar> FunctionDecomposer::_trunc(std::shared_ptr<IRVar> var_int)
{
	auto var = _env->next_var(std::make_shared<IRBool>()); 
	_activeBlock->add_code(std::make_shared<IRCodeTrunc>(var, var_int));
	return var;
}

std::shared_ptr<IRVar> FunctionDecomposer::_zext(std::shared_ptr<IRVar> var_bool)
{
	auto var = _env->next_var(std::make_shared<IRInt>()); 
	_activeBlock->add_code(std::make_shared<IRCodeZext>(var, var_bool));
	return var;
}

void FunctionDecomposer::visitECast(ECast *e_cast)
{
	// Eval expression.
	e_cast->expr_->accept(this);
	if (e_cast->type->is<Common::Class>()) { // All classes are i8*
		e_cast->var = e_cast->expr_->var;
		return;
	}
		
	auto to_type = IRType::from_common_type(e_cast->type);
	auto etype = e_cast->expr_->var->type();
	if (etype->is<IRInt>()) {
		if (to_type->is<IRBool>())
			e_cast->var = _trunc(_load_if_needed(e_cast->expr_));
		else 
			e_cast->var = e_cast->expr_->var;
	} else if (etype->is<IRBool>()) {
		if (to_type->is<IRInt>())
			e_cast->var = _zext(_load_if_needed(e_cast->expr_));
		else 
			e_cast->var = e_cast->expr_->var;
	} else if (etype->is<IRString>()) { // Poor internal representation of Null... :)
		auto var_null = std::make_shared<IRNull>();
		e_cast->var = _cast(var_null, to_type);
	} else {
		// Shouldn't happen
		assert(false);
	}
}

void FunctionDecomposer::visitEMemb(EMemb *e_memb)
{
	// Generate code
	e_memb->expr_->accept(this);

	// We expect pointer to object ptr in `var`
	auto var_obj_ptr = _load_if_needed(e_memb->expr_);

	std::string classname;
	if (e_memb->expr_->type->is<Common::Array>())
		classname = "_array";
	else 
	 	classname = std::dynamic_pointer_cast<Common::Class>(e_memb->expr_->type)->name();
	auto ircls = _env->get_irclass(classname);
	auto memb_name = e_memb->ident_;
	
	var_obj_ptr = _cast(var_obj_ptr, ircls->type_objptr());
	auto var_ptr_ptr = _get_class_property(
					var_obj_ptr,
					ircls->type_label(), 
					ircls->get_property_type(memb_name),
					ircls->get_property_num(memb_name));
	e_memb->var = var_ptr_ptr;
}

void FunctionDecomposer::visitEMembCall(EMembCall *e_memb_call)
{
	auto memb = e_memb_call->ident_;
	auto type = std::dynamic_pointer_cast<Common::Class>(e_memb_call->expr_->type);
	auto ircls = _env->get_irclass(type->name());

	e_memb_call->expr_->accept(this);
	auto var_obj_i8ptr = _load_if_needed(e_memb_call->expr_);
	auto var_obj_ptr = _cast(var_obj_i8ptr, ircls->type_objptr());
	auto var_vtable_loc = _get_class_property(
					var_obj_ptr, 
					ircls->type_label(), 
					ircls->get_property_type("_vtable"),
					ircls->get_property_num("_vtable"));

	auto var_vtable_i8ptr = _load(var_vtable_loc);
	auto var_vtable_ptr = _cast(var_vtable_i8ptr, ircls->vtable_objptr());

	auto method_idx = ircls->get_method_idx(memb);
	auto method = ircls->get_method(memb);

	auto var_fun_loc = _get_class_property(
					var_vtable_ptr, 
					ircls->vtable_type_label(), 
					std::make_shared<IRFun>(method->typestr()),
					method_idx);
	auto var_fun_ptr = _load(var_fun_loc);

	auto r = _env->next_var(IRType::from_common_type(e_memb_call->type));
	auto code = std::make_shared<IRCodeCallPtr>(r, var_fun_ptr);
	code->add_arg(var_obj_i8ptr);
	e_memb_call->var = r;
	for (ListExpr::iterator i = e_memb_call->listexpr_->begin() ; i != e_memb_call->listexpr_->end() ; ++i)
	{
		(*i)->accept(this);
		auto v = _load_if_needed(*i);
		code->add_arg(v);
	}
	_activeBlock->add_code(code);
}

//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================
//=============================================================================

//=============================================================================

//=============================================================================

//=============================================================================


void FunctionDecomposer::visitTClass(TClass *t_class)
{
  /* Code For TClass Goes Here */

  visitIdent(t_class->ident_);

}

void FunctionDecomposer::visitTInt(TInt *t_int)
{
  /* Code For TInt Goes Here */


}

void FunctionDecomposer::visitTStr(TStr *t_str)
{
  /* Code For TStr Goes Here */


}

void FunctionDecomposer::visitTBool(TBool *t_bool)
{
  /* Code For TBool Goes Here */


}

void FunctionDecomposer::visitTNVC(TNVC *tnvc)
{
  /* Code For TNVC Goes Here */

  tnvc->classtype_->accept(this);

}

void FunctionDecomposer::visitTNVB(TNVB *tnvb)
{
  /* Code For TNVB Goes Here */

  tnvb->basictype_->accept(this);

}

void FunctionDecomposer::visitTArray(TArray *t_array)
{
  /* Code For TArray Goes Here */

  t_array->nonvoidtype_->accept(this);

}

void FunctionDecomposer::visitTSingle(TSingle *t_single)
{
  /* Code For TSingle Goes Here */

  t_single->nonvoidtype_->accept(this);

}

void FunctionDecomposer::visitTVoid(TVoid *t_void)
{
  /* Code For TVoid Goes Here */


}

void FunctionDecomposer::visitCastOnClass(CastOnClass *cast_on_class)
{
  /* Code For CastOnClass Goes Here */

  visitIdent(cast_on_class->ident_);

}

void FunctionDecomposer::visitCastOnArr(CastOnArr *cast_on_arr)
{
  /* Code For CastOnArr Goes Here */

  cast_on_arr->nonvoidtype_->accept(this);

}

void FunctionDecomposer::visitCastOnBasic(CastOnBasic *cast_on_basic)
{
  /* Code For CastOnBasic Goes Here */

  cast_on_basic->basictype_->accept(this);

}

void FunctionDecomposer::visitIdentP(IdentP *t) {} //abstract class
void FunctionDecomposer::visitProgram(Program *t) {} //abstract class
void FunctionDecomposer::visitTopDef(TopDef *t) {} //abstract class
void FunctionDecomposer::visitFunDef(FunDef *t) {} //abstract class
void FunctionDecomposer::visitArg(Arg *t) {} //abstract class
void FunctionDecomposer::visitClassDef(ClassDef *t) {} //abstract class
void FunctionDecomposer::visitClassBlock(ClassBlock *t) {} //abstract class
void FunctionDecomposer::visitClassBlockDef(ClassBlockDef *t) {} //abstract class
void FunctionDecomposer::visitClsFldDefItem(ClsFldDefItem *t) {} //abstract class
void FunctionDecomposer::visitBlock(Block *t) {} //abstract class
void FunctionDecomposer::visitStmt(Stmt *t) {} //abstract class
void FunctionDecomposer::visitDeclItem(DeclItem *t) {} //abstract class
void FunctionDecomposer::visitClassType(ClassType *t) {} //abstract class
void FunctionDecomposer::visitBasicType(BasicType *t) {} //abstract class
void FunctionDecomposer::visitNonVoidType(NonVoidType *t) {} //abstract class
void FunctionDecomposer::visitType(Type *t) {} //abstract class
void FunctionDecomposer::visitCastOn(CastOn *t) {} //abstract class
void FunctionDecomposer::visitExpr(Expr *t) {} //abstract class
void FunctionDecomposer::visitAddOp(AddOp *t) {} //abstract class
void FunctionDecomposer::visitMulOp(MulOp *t) {} //abstract class
void FunctionDecomposer::visitRelOp(RelOp *t) {} //abstract class

void FunctionDecomposer::visitIdentPos(IdentPos *ident_pos)
{
  /* Code For IdentPos Goes Here */

  visitIdent(ident_pos->ident_);

}

void FunctionDecomposer::visitProg(Prog *prog)
{
  /* Code For Prog Goes Here */

  prog->listtopdef_->accept(this);

}

void FunctionDecomposer::visitClsDefTop(ClsDefTop *cls_def_top)
{
  /* Code For ClsDefTop Goes Here */

  cls_def_top->classdef_->accept(this);

}

void FunctionDecomposer::visitFnDefTop(FnDefTop *fn_def_top)
{
  /* Code For FnDefTop Goes Here */

  fn_def_top->fundef_->accept(this);

}

void FunctionDecomposer::visitFnDef(FnDef *fn_def)
{
  /* Code For FnDef Goes Here */

  fn_def->type_->accept(this);
  fn_def->identp_->accept(this);
  fn_def->listarg_->accept(this);
  fn_def->block_->accept(this);

}

void FunctionDecomposer::visitEmpty(Empty *p) {}

void FunctionDecomposer::visitAr(Ar *ar)
{
  /* Code For Ar Goes Here */

  ar->type_->accept(this);
  ar->identp_->accept(this);

}

void FunctionDecomposer::visitClsDef(ClsDef *cls_def)
{
  /* Code For ClsDef Goes Here */

  cls_def->identp_->accept(this);
  cls_def->classblock_->accept(this);

}

void FunctionDecomposer::visitClsExtDef(ClsExtDef *cls_ext_def)
{
  /* Code For ClsExtDef Goes Here */

  cls_ext_def->identp_->accept(this);
  visitIdent(cls_ext_def->ident_);
  cls_ext_def->classblock_->accept(this);

}

void FunctionDecomposer::visitClsBlk(ClsBlk *cls_blk)
{
  /* Code For ClsBlk Goes Here */

  cls_blk->listclassblockdef_->accept(this);

}

void FunctionDecomposer::visitClsMthDef(ClsMthDef *cls_mth_def)
{
  /* Code For ClsMthDef Goes Here */

  cls_mth_def->fundef_->accept(this);

}

void FunctionDecomposer::visitClsFldDef(ClsFldDef *cls_fld_def)
{
  /* Code For ClsFldDef Goes Here */

  cls_fld_def->type_->accept(this);
  cls_fld_def->listclsflddefitem_->accept(this);

}

void FunctionDecomposer::visitClsFldDefIt(ClsFldDefIt *cls_fld_def_it)
{
  /* Code For ClsFldDefIt Goes Here */

  cls_fld_def_it->identp_->accept(this);

}

void FunctionDecomposer::visitPlus(Plus *plus)
{
  /* Code For Plus Goes Here */


}

void FunctionDecomposer::visitMinus(Minus *minus)
{
  /* Code For Minus Goes Here */


}

void FunctionDecomposer::visitTimes(Times *times)
{
  /* Code For Times Goes Here */


}

void FunctionDecomposer::visitDiv(Div *div)
{
  /* Code For Div Goes Here */


}

void FunctionDecomposer::visitMod(Mod *mod)
{
  /* Code For Mod Goes Here */


}

void FunctionDecomposer::visitLTH(LTH *lth)
{
  /* Code For LTH Goes Here */


}

void FunctionDecomposer::visitLE(LE *le)
{
  /* Code For LE Goes Here */


}

void FunctionDecomposer::visitGTH(GTH *gth)
{
  /* Code For GTH Goes Here */


}

void FunctionDecomposer::visitGE(GE *ge)
{
  /* Code For GE Goes Here */


}

void FunctionDecomposer::visitEQU(EQU *equ)
{
  /* Code For EQU Goes Here */


}

void FunctionDecomposer::visitNE(NE *ne)
{
  /* Code For NE Goes Here */


}


void FunctionDecomposer::visitListTopDef(ListTopDef *list_top_def)
{
  for (ListTopDef::iterator i = list_top_def->begin() ; i != list_top_def->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void FunctionDecomposer::visitListArg(ListArg *list_arg)
{
  for (ListArg::iterator i = list_arg->begin() ; i != list_arg->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void FunctionDecomposer::visitListClsFldDefItem(ListClsFldDefItem *list_cls_fld_def_item)
{
  for (ListClsFldDefItem::iterator i = list_cls_fld_def_item->begin() ; i != list_cls_fld_def_item->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void FunctionDecomposer::visitListClassBlockDef(ListClassBlockDef *list_class_block_def)
{
  for (ListClassBlockDef::iterator i = list_class_block_def->begin() ; i != list_class_block_def->end() ; ++i)
  {
    (*i)->accept(this);
  }
}


void FunctionDecomposer::visitListDeclItem(ListDeclItem *list_decl_item)
{
  for (ListDeclItem::iterator i = list_decl_item->begin() ; i != list_decl_item->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void FunctionDecomposer::visitListExpr(ListExpr *list_expr)
{
  for (ListExpr::iterator i = list_expr->begin() ; i != list_expr->end() ; ++i)
  {
    (*i)->accept(this);
  }
}


void FunctionDecomposer::visitInteger(Integer x)
{
  /* Code for Integer Goes Here */
}

void FunctionDecomposer::visitChar(Char x)
{
  /* Code for Char Goes Here */
}

void FunctionDecomposer::visitDouble(Double x)
{
  /* Code for Double Goes Here */
}

void FunctionDecomposer::visitString(String x)
{
  /* Code for String Goes Here */
}

void FunctionDecomposer::visitIdent(Ident x)
{
  /* Code for Ident Goes Here */
}

}

