#include <latte/backend/LabelGenerator.h>


namespace Backend {

std::string LabelGenerator::next_temp_label()
{
	return "t_" + std::to_string(_tmp_id++);
}

std::string LabelGenerator::next_block_label()
{
	return "L_" + std::to_string(_block_id++);
}

std::string LabelGenerator::next_glob_label()
{
	return "G_" + std::to_string(_glob_id++);
}

}
