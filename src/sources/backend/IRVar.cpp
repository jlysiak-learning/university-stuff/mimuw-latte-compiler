#include <latte/backend/IRVar.h>

namespace Backend
{


std::string IRVar::tstr() const
{
	return _type->code();
}

std::string IRVar::lstr() const
{
	return "%" + _id;
}

std::string IRVar::code() const
{
	return tstr() + " " + lstr();
}

std::string IRConst::lstr() const
{
	return _id;
}

std::string IRNull::lstr() const
{
	return _id;
}

}
