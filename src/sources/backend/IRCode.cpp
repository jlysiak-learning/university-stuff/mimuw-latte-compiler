// Generate LLVM

#include <latte/backend/IRCode.h>

namespace Backend {

std::string IRCodeGoto::code() const 
{
	return "br label %" + label();
}

std::string IRCodeLabel::code() const 
{
	return label() + ":";
}

std::string IRCodeRet::code() const 
{
	return  "ret " + _var->code();
}

std::string IRCodeRetV::code() const 
{
	return "ret void";
}

std::string IRCodeIf::code() const 
{
	return "br " + _var->code() + ", label %" + _lt + ", label %" + _lf;
}

std::string IRCodeMemb::code() const 
{
	return ""; //std::string(GETMEMB) + _tar->str() + " " + _from->str() + " " + _membName;
}

std::string IRCodeStore::code() const 
{
	return "store " + _what->code() + ", " + _where->code();
}

std::string IRCodeLoad::code() const 
{
	return _where->lstr() + " = load " + _where->tstr() + ", " + _from->code();
}

std::string IRCodeAlloc::code() const 
{
	return _where->lstr() + " = alloca " + _what->code();
}

std::string IRCodeAdd::code() const 
{ 
	std::string s = _res->lstr() + " = ";

	switch(_op) {
		case Common::ADDOP_ADD:
			s += "add";
			break;
		case Common::ADDOP_SUB:
			s += "sub";
			break;
	}
	return s + " " + _res->tstr() + " " + _x1->lstr() + ", " + _x2->lstr(); 
}

std::string IRCodeMul::code() const 
{ 
	std::string s = _res->lstr() + " = ";
	switch(_op) {
		case Common::MULOP_MUL:
			s += "mul";
			break;
		case Common::MULOP_DIV:
			s += "sdiv";
			break;
		case Common::MULOP_MOD:
			s += "srem";
			break;
	}
	return s + " " + _res->tstr() + " " + _x1->lstr() + ", " + _x2->lstr(); 
}

std::string IRCodeRel::code() const 
{ 
	std::string s = _res->lstr() + " = icmp ";
	switch(_op) {
		case Common::RELOP_EQU:
			s += "eq";
			break;
		case Common::RELOP_NE:
			s += "ne";
			break;
		case Common::RELOP_GE:
			s += "sge";
			break;
		case Common::RELOP_GTH:
			s += "sgt";
			break;
		case Common::RELOP_LE:
			s += "sle";
			break;
		case Common::RELOP_LTH:
			s += "slt";
			break;
	}
	return s + " " + _x1->tstr() + " " + _x1->lstr() + ", " + _x2->lstr(); 
}


std::string IRCodeConcat::code() const 
{ 
	return _res->lstr() + "= call i8* @_concat(" + _x1->code() + ", " + _x2->code() + ")";
}

std::string IRCodeAnd::code() const
{
	return _res->lstr() + " = and " + _res->tstr() + " " + _x1->lstr() + ", " + _x2->lstr();
}

std::string IRCodeOr::code() const
{
	return _res->lstr() + " = or " + _res->tstr() + " " + _x1->lstr() + ", " + _x2->lstr();
}

std::string IRCodeNot::code() const
{
	return _res->lstr() + " = xor " + _res->tstr() + " " + _x1->lstr() + ", true";
}

void IRCodePhi::add(std::shared_ptr<IRVar> v, std::string l)
{
	_entries.push_back(std::make_pair(v, l));
}

std::string IRCodePhi::code() const
{
	std::string s = _res->lstr() + " = phi " + _res->tstr() + " ";
	int l =  _entries.size();
	int i = 0;
	for (; i < l - 1; i++) {
		s += "[" + _entries[i].first->lstr() + ", %" + _entries[i].second + "], ";
	}
	s += "[" + _entries[i].first->lstr() + ", %" + _entries[i].second + "]";
	return s;
}

std::string IRCodeCall::code() const
{
	std::string s = "";
	if (_res == nullptr || _res->tstr() == "void")
		s += "call void";
	else 
		s += _res->lstr() + " = call " + _res->tstr();
	s += + " @" + _id + " (";

	int l =  _args.size();
	if (l) {
		int i = 0;
		for (; i < l - 1; i++) {
			s += _args[i]->code() + ", ";
		}
		s += _args[i]->code() + ")";
	} else
		s += ")";
	return s;
}

void IRCodeCall::add_arg(std::shared_ptr<IRVar> v)
{
	_args.push_back(v);
}

std::string IRCodeCallPtr::code() const
{
	std::string s = "";
	if (_res == nullptr || _res->tstr() == "void")
		s += "call ";
	else 
		s += _res->lstr() + " = call " ;
	s += _res->tstr() + " " + _ptr->lstr() + " (";
	int l =  _args.size();
	if (l) {
		int i = 0;
		for (; i < l - 1; i++) {
			s += _args[i]->code() + ", ";
		}
		s += _args[i]->code() + ")";
	} else
		s += ")";
	return s;
}

void IRCodeCallPtr::add_arg(std::shared_ptr<IRVar> v)
{
	_args.push_back(v);
}

std::string IRCodeGetGlobalStr::code() const
{
	std::string s = _res->lstr();
	s += " = getelementptr inbounds " + _gstr->tstr() + ", " + _gstr->tstr() + "* " + _gstr->lstr() + ", i32 0, i32 0";
	return s;
}

std::string IRCodeCast::code() const
{
	std::string s = _where->lstr() + " = bitcast ";
	s += _what->code() + " to " + _where->tstr();
	return s;
}

std::string IRCodeGetProperty::code() const
{
	std::string s = _where->lstr() + " = getelementptr inbounds ";
	s += _base_label + ", " + _from->code() + ", i32 0, i32 " + std::to_string(_order);
	return s;
}

std::string IRCodeGetElem::code() const
{
	std::string s = _where->lstr() + " = getelementptr inbounds ";
	auto el_type = _where->type()->clone();
	el_type->set_ptr(false);
	s += el_type->code() + ", " + _base->code() + ", " + _idx->code();
	return s;
}

std::string IRCodeTrunc::code() const
{
	std::string s = _where->lstr() + " = trunc ";
	s += _what->code() + " to " + _where->tstr();
	return s;
}

std::string IRCodeZext::code() const
{
	std::string s = _where->lstr() + " = zext ";
	s += _what->code() + " to " + _where->tstr();
	return s;
}
}

