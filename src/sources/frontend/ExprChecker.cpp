/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 */

#include <latte/frontend/ExprChecker.h>
#include <latte/common/ClassDefinition.h>
#include <latte/common/FunctionDefinition.h>
#include <latte/common/RelOp.h>
#include <latte/common/AddOp.h>
#include <latte/common/MulOp.h>
#include <latte/common/Const.h>


namespace Frontend
{

ExprChecker::ExprChecker(Common::LocalEnvironment& env, 
		Common::Logger& logger, Expr *expr) :
	_env(env), _logger(logger), _expr(NULL)
{
	_expr = expr;
	_expr->accept(this);
}

void ExprChecker::visitENewArr(ENewArr *e_new_arr)
{
	int line = e_new_arr->line_number;
	_logger.debug_at(line, "Visiting new ARRAY");

	auto eltype = Common::Type::from_type_AST((Latte::Type *) e_new_arr->nonvoidtype_);
	if (eltype->is<Common::Class>()) {
		auto name = std::dynamic_pointer_cast<Common::Class>(eltype)->name();
		auto cls = _env.get_class_def(name);
		if (cls == nullptr) {
			_logger.error_at(line, "Class " + 
					name + " is not defined!");
			e_new_arr->ok = false;
			return;
		}
	}

	e_new_arr->expr_->accept(this);

	if (!e_new_arr->expr_->ok) {
		e_new_arr->ok = false;
		return;
	}
	if (!e_new_arr->expr_->type->is<Common::Int>()) {
		_logger.error_at(line, "Array size is not Int!");
		e_new_arr->ok = false;
		return;
	}
	e_new_arr->type = std::make_shared<Common::Array>(eltype);
	e_new_arr->ok = true;
	e_new_arr->isLVal = false;
	e_new_arr->isConst = false;
	e_new_arr->const_ = nullptr;
}

void ExprChecker::visitENewCls(ENewCls *e_new_cls)
{
	auto name = e_new_cls->ident_;
	int line = e_new_cls->line_number;
	auto cls = _env.get_class_def(name);

	_logger.debug_at(line, "Visiting new CLASS");

	if (cls == nullptr) {
		_logger.error_at(line, "Class " + 
				name + " is not defined!");
		e_new_cls->ok = false;
		return;
	}
	e_new_cls->ok = true;
	e_new_cls->type = std::make_shared<Common::Class>(name);
	e_new_cls->isLVal = false;
	e_new_cls->isConst = false;
	e_new_cls->const_ = nullptr;
}

void ExprChecker::visitELitInt(ELitInt *e_lit_int)
{
	_logger.debug_at(e_lit_int->line_number,"Visiting Int = " + std::to_string(e_lit_int->integer_));
	e_lit_int->ok = true;
	e_lit_int->isConst = true;
	e_lit_int->isLVal = false;
	e_lit_int->type = std::make_shared<Common::Int>();
	e_lit_int->const_ = std::make_shared<Common::Const>(e_lit_int->integer_);
}

void ExprChecker::visitELitTrue(ELitTrue *e_lit_true)
{
	_logger.debug("Visiting Bool = " + std::to_string(true));
	e_lit_true->ok = true;
	e_lit_true->isConst = true;
	e_lit_true->isLVal = false;
	e_lit_true->type = std::make_shared<Common::Bool>();
	e_lit_true->const_ = std::make_shared<Common::Const>(true);
}

void ExprChecker::visitELitFalse(ELitFalse *e_lit_false)
{
	_logger.debug("Visiting Bool = " + std::to_string(false));
	e_lit_false->ok = true;
	e_lit_false->isConst = true;
	e_lit_false->isLVal = false;
	e_lit_false->type = std::make_shared<Common::Bool>();
	e_lit_false->const_ = std::make_shared<Common::Const>(false);
}

void ExprChecker::visitEString(EString *e_string)
{
	_logger.debug("Visiting String = \"" + e_string->string_ + "\"");
	e_string->ok = true;
	e_string->isConst = true;
	e_string->isLVal = false;
	e_string->type = std::make_shared<Common::String>();
	e_string->const_ = std::make_shared<Common::Const>(e_string->string_);
}

void ExprChecker::visitEVar(EVar *e_var)
{
	auto name = e_var->ident_;
	int line = e_var->line_number;
	_logger.debug_at(line, "Visiting variable " + name);

	e_var->type = _env.get_variable_type(e_var->ident_);
	if (e_var->type == nullptr) {
		_logger.error_at(e_var->line_number, "Variable " +
			e_var->ident_ + " doesn't exist!");
		e_var->ok = false;
	} else {
		e_var->ok = true;
	}

	e_var->isConst = false;
	e_var->isLVal = true;
	e_var->const_ = nullptr;
}

void ExprChecker::visitENull(ENull *e_null)
{
	int line = e_null->line_number;
	_logger.debug_at(line, "Visiting NULL");

	e_null->type = std::make_shared<Common::Null>();
	e_null->ok = true;
	e_null->isConst = true;
	e_null->isLVal = false;
	e_null->const_ = std::make_shared<Common::Const>();
}

void ExprChecker::visitEMemb(EMemb *e_memb)
{
	int line = e_memb->line_number;
	std::string name = e_memb->ident_;
	_logger.debug_at(line, "Visiting MEMBER " + name);

	e_memb->expr_->accept(this);

	if (!e_memb->expr_->ok)  {
		e_memb->ok = false;
		return;
	}
	_logger.debug_at(line, "Expression has type " + e_memb->expr_->type->str());

	if (e_memb->expr_->type->is<Common::Class>()) {
		auto cls = std::dynamic_pointer_cast<Common::Class> (e_memb->expr_->type);
		auto T = _env.get_class_def(cls->name())->get_field_type(name);
		if (T == nullptr) {
			_logger.error_at(line, "Class " + cls->name() +
				" doesn't have field named " + name);
			e_memb->ok = false;
			return;
		}
		e_memb->type = T;
		e_memb->const_ = nullptr;
		e_memb->isConst = false;
		e_memb->ok = true;
		e_memb->isLVal = true;
		return;
	}
	if (e_memb->expr_->type->is<Common::Array>() && name == "length") {
		e_memb->type = std::make_shared<Common::Int>();
		e_memb->const_ = nullptr;
		e_memb->isConst = false;
		e_memb->ok = true;
		e_memb->isLVal = false; // `length` property is Read-Only
		return;
	}
	_logger.error_at(line, "Expression has no member `" + name + "`!");
	e_memb->ok = false;
}

void ExprChecker::visitEFunCall(EFunCall *e_fun_call)
{
	int line = e_fun_call->line_number;
	std::string name = e_fun_call->ident_;
	_logger.debug_at(line, "Visiting function call - " + name);

	auto funDef = _env.get_fun_def(name);
	if (funDef == nullptr) {
		_logger.error_at(line, "Function " + name + " doesn't exist!");
		e_fun_call->ok = false;
		return;
	}

	auto args = funDef->get_args();
	auto list_expr = e_fun_call->listexpr_;
	if (args.size() != list_expr->size()) {
		_logger.error_at(line, "Function " + name + " takes " + 
			std::to_string(args.size()) + " but " + 
			std::to_string(list_expr->size()) + " was given!");
		e_fun_call->ok = false;
		return;
	}

	auto ai = args.begin();
	ListExpr::iterator i = list_expr->begin();
	for ( ; i != list_expr->end() ; ++i, ++ai)
	{
		(*i)->accept(this);
		if (!(*i)->ok) {
			e_fun_call->ok = false;
			return;
		}
		if (*(ai->first) != *((*i)->type)) {
			if (!_env.is_child_of((*i)->type, ai->first)) {
				_logger.error_at(line, name + " - argument `" + ai->second + "` has wrong type!");
				e_fun_call->ok = false;
				return;
			}
		}
	}

	e_fun_call->ok = true;
	e_fun_call->isLVal = false;
	e_fun_call->isConst = false;
	e_fun_call->type = funDef->type();
	e_fun_call->const_= nullptr;
}

void ExprChecker::visitEMembCall(EMembCall *e_memb_call)
{
	int line = e_memb_call->line_number;
	std::string name = e_memb_call->ident_;
	_logger.debug_at(line, "Visiting member call - " + name);

	e_memb_call->expr_->accept(this);
	if (!e_memb_call->expr_->ok) {
		e_memb_call->ok = false;
		return;
	}

	if (!e_memb_call->expr_->type->is<Common::Class>()) {
		_logger.error_at(line, "Expression is not a Class!");
		e_memb_call->ok = false;
		return;
	}

	auto cls = std::dynamic_pointer_cast<Common::Class> (e_memb_call->expr_->type);
	auto method = _env.get_class_def(cls->name())->get_method(name);
	if (method == nullptr) {
		_logger.error_at(line, "Class `" + cls->name() +
			"` doesn't have method named `" + name + "`!");
		e_memb_call->ok = false;
		return;
	}

	auto args = method->get_args();
	auto list_expr = e_memb_call->listexpr_;
	if (args.size() != list_expr->size()) {
		_logger.error_at(line, "Method `" + name + "` of class `" + 
			cls->name() + "` takes " + std::to_string(args.size()) +
			" but " + std::to_string(list_expr->size()) + " was given!");
		e_memb_call->ok = false;
		return;
	}
	auto ai = args.begin();
	ListExpr::iterator i = list_expr->begin();
	for ( ; i != list_expr->end() ; ++i, ++ai)
	{
		(*i)->accept(this);
		if (!(*i)->ok) {
			e_memb_call->ok = false;
			return;
		}
		if (*(ai->first) != *((*i)->type)) {
			if (!_env.is_child_of((*i)->type, ai->first)) {
				_logger.error_at(line, name + " - argument `" + ai->second + "` has wrong type!");
				e_memb_call->ok = false;
				return;
			}
		}
	}
	e_memb_call->ok = true;
	e_memb_call->isLVal = false;
	e_memb_call->isConst = false;
	e_memb_call->type = method->type();
	e_memb_call->const_= nullptr;
}

void ExprChecker::visitEAt(EAt *e_at)
{
	int line = e_at->line_number;
	_logger.debug_at(line, "Visiting AT");

	e_at->expr_1->accept(this);
	e_at->expr_2->accept(this);

	if (!e_at->expr_1->ok || !e_at->expr_2->ok)  {
		e_at->ok = false;
		return;
	}
	if (!e_at->expr_1->type->is<Common::Array>()) {
		_logger.error_at(line, "Expression is not an Array!");
		e_at->ok = false;
		return;
	}
	if (!e_at->expr_2->type->is<Common::Int>()) {
		_logger.error_at(line, "Array index expression is not an Integer!");
		e_at->ok = false;
		return;
	}
	e_at->ok = true;
	e_at->type = std::dynamic_pointer_cast<Common::Array>(e_at->expr_1->type)->elem_type();
	e_at->isLVal = true;
	e_at->isConst = false;
	e_at->const_ = nullptr;
}

void ExprChecker::visitECast(ECast *e_cast)
{
	int line = e_cast->line_number;
	_logger.debug_at(line, "Visiting CAST");

	auto ctype = Common::Type::from_cast_AST(e_cast->caston_);
	e_cast->expr_->accept(this);
	if (!e_cast->expr_->ok) {
		e_cast->ok = false;
		return;
	}
	if (!e_cast->expr_->type->is_cast_possible(ctype)) {
		if (!_env.is_child_of(e_cast->expr_->type, ctype)) {
			_logger.error_at(line, "Cannot cast to " + ctype->str() + 
				" from " + e_cast->expr_->type->str());
			e_cast->ok = false;
			return;
		}
	}

	e_cast->ok = true;
	e_cast->isLVal = false;
	e_cast->type = ctype;
	if (e_cast->expr_->isConst && !(e_cast->expr_->type->is<Common::Array>() || e_cast->expr_->type->is<Common::Class>())) {
			e_cast->isConst = true;
			e_cast->const_ = e_cast->expr_->const_->cast_on(ctype);
	} else {
		e_cast->isConst = false;
		e_cast->const_ = nullptr;
	}
}

void ExprChecker::visitNeg(Neg *neg)
{
	int line = neg->line_number;
	_logger.debug_at(line, "Visiting NEG");

	neg->expr_->accept(this);
	if (!neg->expr_->ok) {
		neg->ok = false;
		return;
	}
	if (!neg->expr_->type->is<Common::Int>()) {
		_logger.error_at(line, "Not integer, cannot negate!");
		neg->ok = false;
		return;
	}
	neg->ok = true;
	neg->isConst = neg->expr_->isConst;
	neg->isLVal = false;
	neg->type = neg->expr_->type;
	if (neg->isConst)
		neg->const_  = -neg->expr_->const_;
	else
		neg->const_  = nullptr;
}

void ExprChecker::visitNot(Not *not_)
{
	int line = not_->line_number;
	_logger.debug_at(line, "Visiting NOT");

	not_->expr_->accept(this);
	if (!not_->expr_->ok) {
		not_->ok = false;
		return;
	}

	if (!not_->expr_->type->is<Common::Bool>()) {
		_logger.error_at(line, "Not Bool, cannot do `not`!");
		not_->ok = false;
		return;
	}

	not_->ok = not_->expr_->ok;
	not_->isConst = not_->expr_->isConst;
	not_->isLVal = false;
	not_->type = not_->expr_->type;
	if (not_->isConst)
		not_->const_  = !not_->expr_->const_;
	else
		not_->const_  = nullptr;
}

void ExprChecker::visitEMul(EMul *e_mul)
{
	int line = e_mul->line_number;
	_logger.debug_at(line, "Visiting MULOP");

	e_mul->expr_1->accept(this);
	e_mul->mulop_->accept(this);
	e_mul->expr_2->accept(this);

	if (!(e_mul->expr_1->ok && e_mul->expr_2->ok)) {
		e_mul->ok=false;
		return;
	}
	if (*(e_mul->expr_1->type) != *(e_mul->expr_2->type)) {
		_logger.error_at(line, "Incompatible types - " + 
				e_mul->expr_1->type->str() + " != " + 
				e_mul->expr_2->type->str());
		e_mul->ok=false;
		return;
	}
	if (!e_mul->expr_1->type->is<Common::Int>()){
		_logger.error_at(line, "Cannot use *, /, \% operator on type " + e_mul->expr_2->type->str());
		e_mul->ok=false;
		return;
	}
	e_mul->ok = true;
	e_mul->type = e_mul->expr_1->type;
	e_mul->isLVal = false;
	e_mul->isConst = e_mul->expr_1->isConst && e_mul->expr_2->isConst;
	if (e_mul->isConst) {
		e_mul->const_ = Common::Const::mul(e_mul->expr_1->const_, e_mul->expr_2->const_, e_mul->mulop_->op);
		_logger.debug_at(line, "MULOP of const expressions - " + e_mul->const_->to_string());
	} else {
		e_mul->const_ = nullptr;
	}
}

void ExprChecker::visitEAdd(EAdd *e_add)
{
	int line = e_add->line_number;
	_logger.debug_at(line, "Visiting AddOp");

	e_add->expr_1->accept(this);
	e_add->addop_->accept(this);
	e_add->expr_2->accept(this);

	if (!(e_add->expr_1->ok && e_add->expr_2->ok)) {
		e_add->ok=false;
		return;
	}
	if (*(e_add->expr_1->type) != *(e_add->expr_2->type)) {
		_logger.error_at(line, "Incompatible types - " + 
				e_add->expr_1->type->str() + " != " + 
				e_add->expr_2->type->str());
		e_add->ok=false;
		return;
	}
	if (!e_add->expr_1->type->is<Common::Int>() && 
		!e_add->expr_1->type->is<Common::String>()) {
		_logger.error_at(line, "Cannot use any ADDOP operator on type " + e_add->expr_1->type->str());
		e_add->ok=false;
		return;
	}

	if (e_add->expr_1->type->is<Common::String>() && e_add->addop_->op != Common::ADDOP_ADD) {
		_logger.error_at(line, "Cannot use SUB operator on type " + e_add->expr_1->type->str());
		e_add->ok=false;
		return;
	}

	e_add->ok = true;
	e_add->type = e_add->expr_1->type;
	e_add->isConst = e_add->expr_1->isConst && e_add->expr_2->isConst;
	e_add->isLVal = false;
	if (e_add->isConst) {
		e_add->const_ = Common::Const::add(e_add->expr_1->const_, e_add->expr_2->const_, e_add->addop_->op);
		_logger.debug_at(line, "ADDOP of const expressions - " + e_add->const_->to_string());
	} else {
		e_add->const_ = nullptr;
	}
}

void ExprChecker::visitERel(ERel *e_rel)
{
	int line = e_rel->line_number;
	_logger.debug_at(line, "Visiting RELOP");

	e_rel->expr_1->accept(this);
	e_rel->relop_->accept(this);
	e_rel->expr_2->accept(this);

	if (!(e_rel->expr_1->ok && e_rel->expr_2->ok)) {
		e_rel->ok=false;
		return;
	}
	if (*(e_rel->expr_1->type) != *(e_rel->expr_2->type)) {
		if (!_env.is_child_of(e_rel->expr_1->type, e_rel->expr_2->type) && 
		    !_env.is_child_of(e_rel->expr_2->type, e_rel->expr_1->type)) {
			_logger.error_at(line, "Incompatible types - " + 
					e_rel->expr_1->type->str() + " != " + 
					e_rel->expr_2->type->str());
			e_rel->ok=false;
			return;
		}
	}

	if (!e_rel->expr_1->type->is_relop_compatible(e_rel->relop_->op)) {
		_logger.error_at(line, "Operator " + Common::relop_to_string(e_rel->relop_->op) +
				" cannot be used with type " + e_rel->expr_1->type->str());
		e_rel->ok=false;
		return;
	}
	
	e_rel->ok = true;
	e_rel->type = std::make_shared<Common::Bool>();
	e_rel->isConst = e_rel->expr_1->isConst && e_rel->expr_2->isConst;
	e_rel->isLVal = false;
	if (e_rel->isConst)  {
		e_rel->const_ = Common::Const::rel(e_rel->expr_1->const_, e_rel->expr_2->const_, e_rel->relop_->op);
		_logger.debug_at(line, "RELOP of const expressions - " + e_rel->const_->to_string());
	} else
		e_rel->const_ = nullptr;
}

void ExprChecker::visitEAnd(EAnd *e_and)
{
	int line = e_and->line_number;
	_logger.debug_at(line, "Visiting ANDOP");

	e_and->expr_1->accept(this);
	e_and->expr_2->accept(this);

	if (!(e_and->expr_1->ok && e_and->expr_2->ok)) {
		e_and->ok=false;
		return;
	}
	if (!e_and->expr_1->type->is<Common::Bool>()){
		_logger.error_at(line, "Cannot use AND operator on type " + e_and->expr_1->type->str());
		e_and->ok=false;
		return;
	}
	if (*(e_and->expr_1->type) != *(e_and->expr_2->type)) {
		_logger.error_at(line, "Incompatible types - " + 
				e_and->expr_1->type->str() + " != " + 
				e_and->expr_2->type->str());
		e_and->ok=false;
		return;
	}
	e_and->ok = true;
	e_and->isLVal = false;
	e_and->type = e_and->expr_1->type;

	e_and->isConst = true;
	if (e_and->expr_1->isConst && e_and->expr_2->isConst) {
		e_and->const_ = e_and->expr_1->const_ && e_and->expr_2->const_;
	} else if (e_and->expr_1->isConst && !e_and->expr_1->const_->b()) {
		e_and->const_ = e_and->expr_1->const_;
	} else {
		e_and->isConst = false;
		e_and->const_ = nullptr;
	}
}

void ExprChecker::visitEOr(EOr *e_or)
{
	int line = e_or->line_number;
	_logger.debug_at(line, "Visiting ANDOP");

	e_or->expr_1->accept(this);
	e_or->expr_2->accept(this);

	if (!(e_or->expr_1->ok && e_or->expr_2->ok)) {
		e_or->ok=false;
		return;
	}

	if (!e_or->expr_1->type->is<Common::Bool>()){
		_logger.error_at(line, "Cannot use OR operator on type " + e_or->expr_1->type->str());
		e_or->ok=false;
		return;
	}

	if (*(e_or->expr_1->type) != *(e_or->expr_2->type)) {
		_logger.error_at(line, "Incompatible types - " + 
				e_or->expr_1->type->str() + " != " + 
				e_or->expr_2->type->str());
		e_or->ok=false;
		return;
	}
	e_or->ok = true;
	e_or->isLVal = false;
	e_or->type = e_or->expr_1->type;

	e_or->isConst = true;
	if (e_or->expr_1->isConst && e_or->expr_2->isConst) {
		e_or->const_ = e_or->expr_1->const_ || e_or->expr_2->const_;
	} else if (e_or->expr_1->isConst && e_or->expr_1->const_->b()) {
		e_or->const_ = e_or->expr_1->const_;
	} else {
		e_or->isConst = false;
		e_or->const_ = nullptr;
	}
}

//===============================  UNUSED


void ExprChecker::visitPlus(Plus *plus)
{
	plus->op = Common::ADDOP_ADD;
}

void ExprChecker::visitMinus(Minus *minus)
{
	minus->op = Common::ADDOP_SUB;
}

void ExprChecker::visitTimes(Times *times)
{
	times->op = Common::MULOP_MUL;
}

void ExprChecker::visitDiv(Div *div)
{
	div->op = Common::MULOP_DIV;
}

void ExprChecker::visitMod(Mod *mod)
{
	mod->op = Common::MULOP_MOD;
}

void ExprChecker::visitLTH(LTH *lth)
{
	lth->op = Common::relop_t::RELOP_LTH;
}

void ExprChecker::visitLE(LE *le)
{
	le->op = Common::RELOP_LE;
}

void ExprChecker::visitGTH(GTH *gth)
{
	gth->op = Common::RELOP_GTH;
}

void ExprChecker::visitGE(GE *ge)
{
	ge->op = Common::RELOP_GE;
}

void ExprChecker::visitEQU(EQU *equ)
{
	equ->op = Common::RELOP_EQU;
}

void ExprChecker::visitNE(NE *ne)
{
	ne->op = Common::RELOP_NE;
}


void ExprChecker::visitInteger(Integer x)
{
	/* Code for Integer Goes Here */
}

void ExprChecker::visitChar(Char x)
{
	/* Code for Char Goes Here */
}

void ExprChecker::visitDouble(Double x)
{
	/* Code for Double Goes Here */
}

void ExprChecker::visitString(String x)
{
	/* Code for String Goes Here */
}

void ExprChecker::visitIdent(Ident x)
{
	/* Code for Ident Goes Here */
}
void ExprChecker::visitExp(Exp *exp)
{
	exp->expr_->accept(this);
}

void ExprChecker::visitListExpr(ListExpr *list_expr)
{
	for (ListExpr::iterator i = list_expr->begin() ; i != list_expr->end() ; ++i)
	{
		(*i)->accept(this);
	}
}

void ExprChecker::visitIdentP(IdentP *t) {} //abstract class
void ExprChecker::visitProgram(Program *t) {} //abstract class
void ExprChecker::visitTopDef(TopDef *t) {} //abstract class
void ExprChecker::visitFunDef(FunDef *t) {} //abstract class
void ExprChecker::visitArg(Arg *t) {} //abstract class
void ExprChecker::visitClassDef(ClassDef *t) {} //abstract class
void ExprChecker::visitClassBlock(ClassBlock *t) {} //abstract class
void ExprChecker::visitClassBlockDef(ClassBlockDef *t) {} //abstract class
void ExprChecker::visitClsFldDefItem(ClsFldDefItem *t) {} //abstract class
void ExprChecker::visitBlock(Block *t) {} //abstract class
void ExprChecker::visitStmt(Stmt *t) {} //abstract class
void ExprChecker::visitDeclItem(DeclItem *t) {} //abstract class
void ExprChecker::visitClassType(ClassType *t) {} //abstract class
void ExprChecker::visitBasicType(BasicType *t) {} //abstract class
void ExprChecker::visitNonVoidType(NonVoidType *t) {} //abstract class
void ExprChecker::visitType(Type *t) {} //abstract class
void ExprChecker::visitCastOn(CastOn *t) {} //abstract class
void ExprChecker::visitExpr(Expr *t) {} //abstract class
void ExprChecker::visitAddOp(AddOp *t) {} //abstract class
void ExprChecker::visitMulOp(MulOp *t) {} //abstract class
void ExprChecker::visitRelOp(RelOp *t) {} //abstract class
void ExprChecker::visitIdentPos(IdentPos *ident_pos) { }
void ExprChecker::visitProg(Prog *prog) {}
void ExprChecker::visitClsDefTop(ClsDefTop *cls_def_top) { }
void ExprChecker::visitFnDefTop(FnDefTop *fn_def_top) { }
void ExprChecker::visitFnDef(FnDef *fn_def) { }
void ExprChecker::visitAr(Ar *ar) { }
void ExprChecker::visitClsDef(ClsDef *cls_def)
{
	/* Code For ClsDef Goes Here */

	cls_def->identp_->accept(this);
	cls_def->classblock_->accept(this);

}

void ExprChecker::visitClsExtDef(ClsExtDef *cls_ext_def)
{
	/* Code For ClsExtDef Goes Here */

	cls_ext_def->identp_->accept(this);
	visitIdent(cls_ext_def->ident_);
	cls_ext_def->classblock_->accept(this);

}

void ExprChecker::visitClsBlk(ClsBlk *cls_blk)
{
	/* Code For ClsBlk Goes Here */

	cls_blk->listclassblockdef_->accept(this);

}

void ExprChecker::visitClsMthDef(ClsMthDef *cls_mth_def)
{
	/* Code For ClsMthDef Goes Here */

	cls_mth_def->fundef_->accept(this);

}

void ExprChecker::visitClsFldDef(ClsFldDef *cls_fld_def)
{
	/* Code For ClsFldDef Goes Here */

	cls_fld_def->type_->accept(this);
	cls_fld_def->listclsflddefitem_->accept(this);

}

void ExprChecker::visitClsFldDefIt(ClsFldDefIt *cls_fld_def_it)
{
	/* Code For ClsFldDefIt Goes Here */

	cls_fld_def_it->identp_->accept(this);

}

void ExprChecker::visitBlk(Blk *blk)
{
	/* Code For Blk Goes Here */

	blk->liststmt_->accept(this);

}

void ExprChecker::visitDecl(Decl *decl)
{
	/* Code For Decl Goes Here */

	decl->type_->accept(this);
	decl->listdeclitem_->accept(this);

}

void ExprChecker::visitAssign(Assign *assign)
{
	/* Code For Assign Goes Here */

	assign->expr_1->accept(this);
	assign->expr_2->accept(this);

}

void ExprChecker::visitIncr(Incr *incr)
{
	/* Code For Incr Goes Here */

	incr->expr_->accept(this);

}

void ExprChecker::visitDecr(Decr *decr)
{
	/* Code For Decr Goes Here */

	decr->expr_->accept(this);

}

void ExprChecker::visitRet(Ret *ret)
{
	/* Code For Ret Goes Here */

	ret->expr_->accept(this);

}

void ExprChecker::visitVRet(VRet *v_ret)
{
	/* Code For VRet Goes Here */


}

void ExprChecker::visitWhile(While *while_)
{
	/* Code For While Goes Here */

	while_->expr_->accept(this);
	while_->stmt_->accept(this);

}

void ExprChecker::visitFor(For *for_)
{
	/* Code For For Goes Here */

	for_->type_->accept(this);
	visitIdent(for_->ident_);
	for_->expr_->accept(this);
	for_->stmt_->accept(this);

}

void ExprChecker::visitIf(If *if_)
{
	/* Code For If Goes Here */

	if_->expr_->accept(this);
	if_->stmt_->accept(this);

}

void ExprChecker::visitIfElse(IfElse *if_else)
{
	/* Code For IfElse Goes Here */

	if_else->expr_->accept(this);
	if_else->stmt_1->accept(this);
	if_else->stmt_2->accept(this);

}

void ExprChecker::visitBStmt(BStmt *b_stmt)
{
	/* Code For BStmt Goes Here */

	b_stmt->block_->accept(this);

}

void ExprChecker::visitEmpty(Empty *empty)
{
	/* Code For Empty Goes Here */


}

void ExprChecker::visitListTopDef(ListTopDef *list_top_def)
{
	for (ListTopDef::iterator i = list_top_def->begin() ; i != list_top_def->end() ; ++i)
	{
		(*i)->accept(this);
	}
}

void ExprChecker::visitListArg(ListArg *list_arg)
{
	for (ListArg::iterator i = list_arg->begin() ; i != list_arg->end() ; ++i)
	{
		(*i)->accept(this);
	}
}

void ExprChecker::visitListClsFldDefItem(ListClsFldDefItem *list_cls_fld_def_item)
{
	for (ListClsFldDefItem::iterator i = list_cls_fld_def_item->begin() ; i != list_cls_fld_def_item->end() ; ++i)
	{
		(*i)->accept(this);
	}
}

void ExprChecker::visitListClassBlockDef(ListClassBlockDef *list_class_block_def)
{
	for (ListClassBlockDef::iterator i = list_class_block_def->begin() ; i != list_class_block_def->end() ; ++i)
	{
		(*i)->accept(this);
	}
}

void ExprChecker::visitListStmt(ListStmt *list_stmt)
{
	for (ListStmt::iterator i = list_stmt->begin() ; i != list_stmt->end() ; ++i)
	{
		(*i)->accept(this);
	}
}

void ExprChecker::visitListDeclItem(ListDeclItem *list_decl_item)
{
	for (ListDeclItem::iterator i = list_decl_item->begin() ; i != list_decl_item->end() ; ++i)
	{
		(*i)->accept(this);
	}
}


void ExprChecker::visitNoInit(NoInit *no_init)
{
	/* Code For NoInit Goes Here */

	visitIdent(no_init->ident_);

}

void ExprChecker::visitInit(Init *init)
{
	/* Code For Init Goes Here */

	visitIdent(init->ident_);
	init->expr_->accept(this);

}

void ExprChecker::visitTClass(TClass *t_class)
{
	/* Code For TClass Goes Here */

	visitIdent(t_class->ident_);

}

void ExprChecker::visitTInt(TInt *t_int)
{
	/* Code For TInt Goes Here */


}

void ExprChecker::visitTStr(TStr *t_str)
{
	/* Code For TStr Goes Here */


}

void ExprChecker::visitTBool(TBool *t_bool)
{
	/* Code For TBool Goes Here */


}

void ExprChecker::visitTNVC(TNVC *tnvc)
{
	/* Code For TNVC Goes Here */

	tnvc->classtype_->accept(this);

}

void ExprChecker::visitTNVB(TNVB *tnvb)
{
	/* Code For TNVB Goes Here */

	tnvb->basictype_->accept(this);

}

void ExprChecker::visitTArray(TArray *t_array)
{
	/* Code For TArray Goes Here */

	t_array->nonvoidtype_->accept(this);

}

void ExprChecker::visitTSingle(TSingle *t_single)
{
	/* Code For TSingle Goes Here */

	t_single->nonvoidtype_->accept(this);

}

void ExprChecker::visitTVoid(TVoid *t_void)
{
	/* Code For TVoid Goes Here */


}

void ExprChecker::visitCastOnClass(CastOnClass *cast_on_class)
{
	/* Code For CastOnClass Goes Here */

	visitIdent(cast_on_class->ident_);

}

void ExprChecker::visitCastOnArr(CastOnArr *cast_on_arr)
{
	/* Code For CastOnArr Goes Here */

	cast_on_arr->nonvoidtype_->accept(this);

}

void ExprChecker::visitCastOnBasic(CastOnBasic *cast_on_basic)
{
	/* Code For CastOnBasic Goes Here */

	cast_on_basic->basictype_->accept(this);

}

}
