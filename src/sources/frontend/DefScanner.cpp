/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Definitions scanner.
 */

#include <cassert>
#include <latte/frontend/DefScanner.h>

namespace Frontend
{

using namespace Latte;

DefScanner::DefScanner(Common::Environment &env, Common::Logger &logger) :
	_env(env), _logger(logger) {};

err_t DefScanner::scan_defs(Visitable *ast_root) 
{
	_logger.info("Now scan all classes and functions...");
	visitProg(static_cast<Prog *>(ast_root));
	if (_logger.has_errors())
		return FAIL;

	_check_ext_classes();
	if (_logger.has_errors())
		return FAIL;
	
	return SUCCESS;
}

void DefScanner::_check_ext_classes()
{
	// Check extended class
	const auto classes = _env.classes();
	for (auto it = classes.begin(); it != classes.end(); ++it) {
		int line = it->second->get_line();
		auto cls_name = it->second->get_name();
		auto cls_parent = it->second->get_parent_name();
		if (cls_parent != "") {
			if (classes.find(cls_parent) == classes.end()) {
				_logger.error_at(line, "Parent " + cls_parent +
					" of class " + cls_name + " doesn't exist!");
			} else {
				it->second->set_parent(classes.at(cls_parent));
				if (!it->second->valid())
					_logger.error("Property redefined or wrong method signature!");
			}
		}
	}
}

/*=========================== VISITOR STUFF */
void DefScanner::visitFnDef(FnDef *fn_def)
{
	int line = fn_def->identp_->line_number;
	std::string name = static_cast<IdentPos *>(fn_def->identp_)->ident_;
	auto fnType = Common::Type::from_type_AST(fn_def->type_);

	if (_env.is_reserved_ident(name)) {
		_logger.error_at(line, "Cannot use reserved identifier: " + name);
		goto err;
	}

	_inFun = std::make_shared<Common::FunctionDefinition>(line, fnType, name);

	fn_def->listarg_->accept(this);

	if (_inFun == nullptr)
		return;
	
	_inFun->add_body(static_cast<Blk *>(fn_def->block_));

	if (_inClass != nullptr) {

		// CANNOT DEFINE FUNCTIONS WITH SAME IDs
		if (_inClass->has_method(name)) { 
			_logger.error_at(line, "Method `" + name + \
				"` already defined in class " + \
				_inClass->get_name());
			goto err;
		} 
		if (!_inClass->add_method(_inFun)) {
			// Same ID in superclass but wrong signature
			_logger.error_at(line, "Function `" + name + \
				"` probably was defined in superclass " +\
				"but has wrong signature.");
			goto err;
		}
		_logger.info_at(line, "Method " + _inFun->str() + \
			" defined in class " + _inClass->get_name());
	} else {
		if (_env.get_fun_def(name) != nullptr) {
			_logger.error_at(line, "Top function " + name + " already defined!");
			goto err;
		}
		_env.add_fun_def(_inFun);
		_logger.info_at(line, "Method " + _inFun->str() + \
			" defined in global scope");
	}
err:
	_inFun = nullptr;
}

void DefScanner::visitAr(Ar *ar)
{
	std::string name = static_cast<IdentPos *>(ar->identp_)->ident_;
	auto type = Common::Type::from_type_AST(ar->type_);

	if (type->is<Common::Void>()) {
		_logger.error_at(_inFun->get_line(), "Function argument cannot be of type VOID");
		goto err;
	}

	if (_inFun->is_arg_name_defined(name)) {
		_logger.error_at(_inFun->get_line(), "Argument name is not unique!");
		goto err;
	}

	if (_inClass != nullptr && name == "self") {
		_logger.error_at(_inFun->get_line(), "`self` is reserved identifier!");
		goto err;
	}
	_inFun->add_arg(type, name);
	return;
err:
	_inFun = nullptr;
}

void DefScanner::visitClsExtDef(ClsExtDef *cls_ext_def)
{
	int line = cls_ext_def->identp_->line_number;
	std::string name = static_cast<IdentPos *>(cls_ext_def->identp_)->ident_;
	std::string parent_name = cls_ext_def->ident_;

	if (_env.is_reserved_ident(name)) {
		_logger.error_at(line, "Cannot use reserved identifier: " + name);
		return;
	}

	_inClass = _env.get_class_def(name);
	if (_inClass != nullptr) {
		std::string msg = "Class " + _inClass->get_name() + \
				   " already defined at line " +\
				   std::to_string(_inClass->get_line());
		_logger.error_at(line, msg);
		_inClass = nullptr;
		return;
	}	

	// cls is null
	_inClass = std::make_shared<Common::ClassDefinition>(line, name, parent_name);
	_logger.info_at(line, "Found definition of class: " + name + " --> " + parent_name);
	cls_ext_def->classblock_->accept(this);

	_env.add_class_def(_inClass);

	_inClass = nullptr;
}

void DefScanner::visitClsDef(ClsDef *cls_def)
{
	int line = cls_def->identp_->line_number;
	std::string name = static_cast<IdentPos *>(cls_def->identp_)->ident_;

	if (_env.is_reserved_ident(name)) {
		_logger.error_at(line, "Cannot use reserved identifier: " + name);
		goto err;
	}

	_inClass = _env.get_class_def(name);
	if (_inClass != nullptr) {
		std::string msg = "Class " + _inClass->get_name() +
				   " already defined at line " +
				   std::to_string(_inClass->get_line());
		_logger.error_at(line, msg);
		goto err;
	}	

	// cls is null
	_inClass = std::make_shared<Common::ClassDefinition>(line, name); 
	_logger.info_at(line, "Found definition of class: " + name);
	cls_def->classblock_->accept(this);
	_env.add_class_def(_inClass);

err:
	_inClass = nullptr;
}

void DefScanner::visitClsFldDef(ClsFldDef *cls_fld_def)
{
	int line = cls_fld_def->line_number;
	auto type = Common::Type::from_type_AST(cls_fld_def->type_);

	
	if (type->is<Common::Void>()) {
		_logger.error_at(line, "Class fields cannot be of type VOID");
		return;
	}

	_inType = type;
	cls_fld_def->listclsflddefitem_->accept(this);
	_inType = nullptr;
}

void DefScanner::visitClsFldDefIt(ClsFldDefIt *cls_fld_def_it)
{
	int line = cls_fld_def_it->line_number;
	std::string name = static_cast<IdentPos *>(cls_fld_def_it->identp_)->ident_;

	assert(_inClass != nullptr);
	assert(_inType != nullptr);

	if (!_inClass->add_field(_inType, name)) {
		std::string msg = "Cannot redefine property `"+ name +"`!";
		_logger.error_at(line, msg);
		return;
	}
	_logger.info_at(line, "Field defined: " + _inType->str() + " " + name);
}

//========================== UNUSED =========================================*/

void DefScanner::visitIdentP(IdentP *t) {} //abstract class
void DefScanner::visitProgram(Program *t) {} //abstract class
void DefScanner::visitTopDef(TopDef *t) {} //abstract class
void DefScanner::visitFunDef(FunDef *t) {} //abstract class
void DefScanner::visitArg(Arg *t) {} //abstract class
void DefScanner::visitClassDef(ClassDef *t) {} //abstract class
void DefScanner::visitClassBlock(ClassBlock *t) {} //abstract class
void DefScanner::visitClassBlockDef(ClassBlockDef *t) {} //abstract class
void DefScanner::visitClsFldDefItem(ClsFldDefItem *t) {} //abstract class
void DefScanner::visitBlock(Block *t) {} //abstract class
void DefScanner::visitStmt(Stmt *t) {} //abstract class
void DefScanner::visitDeclItem(DeclItem *t) {} //abstract class
void DefScanner::visitClassType(ClassType *t) {} //abstract class
void DefScanner::visitBasicType(BasicType *t) {} //abstract class
void DefScanner::visitNonVoidType(NonVoidType *t) {} //abstract class
void DefScanner::visitType(Type *t) {} //abstract class
void DefScanner::visitCastOn(CastOn *t) {} //abstract class
void DefScanner::visitExpr(Expr *t) {} //abstract class
void DefScanner::visitAddOp(AddOp *t) {} //abstract class
void DefScanner::visitMulOp(MulOp *t) {} //abstract class
void DefScanner::visitRelOp(RelOp *t) {} //abstract class

void DefScanner::visitIdentPos(IdentPos *ident_pos)
{
  /* Code For IdentPos Goes Here */

  visitIdent(ident_pos->ident_);

}

void DefScanner::visitProg(Prog *prog)
{
  /* Code For Prog Goes Here */

  prog->listtopdef_->accept(this);

}

void DefScanner::visitClsDefTop(ClsDefTop *cls_def_top)
{
  /* Code For ClsDefTop Goes Here */

  cls_def_top->classdef_->accept(this);

}

void DefScanner::visitFnDefTop(FnDefTop *fn_def_top)
{
  /* Code For FnDefTop Goes Here */

  fn_def_top->fundef_->accept(this);

}

void DefScanner::visitClsBlk(ClsBlk *cls_blk)
{
  /* Code For ClsBlk Goes Here */

  cls_blk->listclassblockdef_->accept(this);

}

void DefScanner::visitClsMthDef(ClsMthDef *cls_mth_def)
{
  /* Code For ClsMthDef Goes Here */

  cls_mth_def->fundef_->accept(this);

}

void DefScanner::visitBlk(Blk *blk)
{
  /* Code For Blk Goes Here */

  blk->liststmt_->accept(this);

}

void DefScanner::visitDecl(Decl *decl)
{
  /* Code For Decl Goes Here */

  decl->type_->accept(this);
  decl->listdeclitem_->accept(this);

}

void DefScanner::visitAssign(Assign *assign)
{
  /* Code For Assign Goes Here */

  assign->expr_1->accept(this);
  assign->expr_2->accept(this);

}

void DefScanner::visitIncr(Incr *incr)
{
  /* Code For Incr Goes Here */

  incr->expr_->accept(this);

}

void DefScanner::visitDecr(Decr *decr)
{
  /* Code For Decr Goes Here */

  decr->expr_->accept(this);

}

void DefScanner::visitRet(Ret *ret)
{
  /* Code For Ret Goes Here */

  ret->expr_->accept(this);

}

void DefScanner::visitVRet(VRet *v_ret)
{
  /* Code For VRet Goes Here */


}

void DefScanner::visitWhile(While *while_)
{
  /* Code For While Goes Here */

  while_->expr_->accept(this);
  while_->stmt_->accept(this);

}

void DefScanner::visitFor(For *for_)
{
  /* Code For For Goes Here */

  for_->type_->accept(this);
  visitIdent(for_->ident_);
  for_->expr_->accept(this);
  for_->stmt_->accept(this);

}

void DefScanner::visitIf(If *if_)
{
  /* Code For If Goes Here */

  if_->expr_->accept(this);
  if_->stmt_->accept(this);

}

void DefScanner::visitIfElse(IfElse *if_else)
{
  /* Code For IfElse Goes Here */

  if_else->expr_->accept(this);
  if_else->stmt_1->accept(this);
  if_else->stmt_2->accept(this);

}

void DefScanner::visitBStmt(BStmt *b_stmt)
{
  /* Code For BStmt Goes Here */

  b_stmt->block_->accept(this);

}

void DefScanner::visitEmpty(Empty *empty)
{
  /* Code For Empty Goes Here */


}

void DefScanner::visitExp(Exp *exp)
{
  /* Code For Exp Goes Here */

  exp->expr_->accept(this);

}

void DefScanner::visitNoInit(NoInit *no_init)
{
  /* Code For NoInit Goes Here */

  visitIdent(no_init->ident_);

}

void DefScanner::visitInit(Init *init)
{
  /* Code For Init Goes Here */

  visitIdent(init->ident_);
  init->expr_->accept(this);

}

void DefScanner::visitTClass(TClass *t_class)
{
  /* Code For TClass Goes Here */

  visitIdent(t_class->ident_);

}

void DefScanner::visitTInt(TInt *t_int)
{
  /* Code For TInt Goes Here */


}

void DefScanner::visitTStr(TStr *t_str)
{
  /* Code For TStr Goes Here */


}

void DefScanner::visitTBool(TBool *t_bool)
{
  /* Code For TBool Goes Here */


}

void DefScanner::visitTNVC(TNVC *tnvc)
{
  /* Code For TNVC Goes Here */

  tnvc->classtype_->accept(this);

}

void DefScanner::visitTNVB(TNVB *tnvb)
{
  /* Code For TNVB Goes Here */

  tnvb->basictype_->accept(this);

}

void DefScanner::visitTArray(TArray *t_array)
{
  /* Code For TArray Goes Here */

  t_array->nonvoidtype_->accept(this);

}

void DefScanner::visitTSingle(TSingle *t_single)
{
  /* Code For TSingle Goes Here */

  t_single->nonvoidtype_->accept(this);

}

void DefScanner::visitTVoid(TVoid *t_void)
{
  /* Code For TVoid Goes Here */


}

void DefScanner::visitCastOnClass(CastOnClass *cast_on_class)
{
  /* Code For CastOnClass Goes Here */

  visitIdent(cast_on_class->ident_);

}

void DefScanner::visitCastOnArr(CastOnArr *cast_on_arr)
{
  /* Code For CastOnArr Goes Here */

  cast_on_arr->nonvoidtype_->accept(this);

}

void DefScanner::visitCastOnBasic(CastOnBasic *cast_on_basic)
{
  /* Code For CastOnBasic Goes Here */

  cast_on_basic->basictype_->accept(this);

}

void DefScanner::visitENewArr(ENewArr *e_new_arr)
{
  /* Code For ENewArr Goes Here */

  e_new_arr->nonvoidtype_->accept(this);
  e_new_arr->expr_->accept(this);

}

void DefScanner::visitENewCls(ENewCls *e_new_cls)
{
  /* Code For ENewCls Goes Here */

  visitIdent(e_new_cls->ident_);

}

void DefScanner::visitELitInt(ELitInt *e_lit_int)
{
  /* Code For ELitInt Goes Here */

  visitInteger(e_lit_int->integer_);

}

void DefScanner::visitELitTrue(ELitTrue *e_lit_true)
{
  /* Code For ELitTrue Goes Here */


}

void DefScanner::visitELitFalse(ELitFalse *e_lit_false)
{
  /* Code For ELitFalse Goes Here */


}

void DefScanner::visitEString(EString *e_string)
{
  /* Code For EString Goes Here */

  visitString(e_string->string_);

}

void DefScanner::visitEVar(EVar *e_var)
{
  /* Code For EVar Goes Here */

  visitIdent(e_var->ident_);

}

void DefScanner::visitENull(ENull *e_null)
{
  /* Code For ENull Goes Here */


}

void DefScanner::visitEMemb(EMemb *e_memb)
{
  /* Code For EMemb Goes Here */

  e_memb->expr_->accept(this);
  visitIdent(e_memb->ident_);

}

void DefScanner::visitEFunCall(EFunCall *e_fun_call)
{
  /* Code For EFunCall Goes Here */

  visitIdent(e_fun_call->ident_);
  e_fun_call->listexpr_->accept(this);

}

void DefScanner::visitEMembCall(EMembCall *e_memb_call)
{
  /* Code For EMembCall Goes Here */

  e_memb_call->expr_->accept(this);
  visitIdent(e_memb_call->ident_);
  e_memb_call->listexpr_->accept(this);

}

void DefScanner::visitEAt(EAt *e_at)
{
  /* Code For EAt Goes Here */

  e_at->expr_1->accept(this);
  e_at->expr_2->accept(this);

}

void DefScanner::visitECast(ECast *e_cast)
{
  /* Code For ECast Goes Here */

  e_cast->caston_->accept(this);
  e_cast->expr_->accept(this);

}

void DefScanner::visitNeg(Neg *neg)
{
  /* Code For Neg Goes Here */

  neg->expr_->accept(this);

}

void DefScanner::visitNot(Not *not_)
{
  /* Code For Not Goes Here */

  not_->expr_->accept(this);

}

void DefScanner::visitEMul(EMul *e_mul)
{
  /* Code For EMul Goes Here */

  e_mul->expr_1->accept(this);
  e_mul->mulop_->accept(this);
  e_mul->expr_2->accept(this);

}

void DefScanner::visitEAdd(EAdd *e_add)
{
  /* Code For EAdd Goes Here */

  e_add->expr_1->accept(this);
  e_add->addop_->accept(this);
  e_add->expr_2->accept(this);

}

void DefScanner::visitERel(ERel *e_rel)
{
  /* Code For ERel Goes Here */

  e_rel->expr_1->accept(this);
  e_rel->relop_->accept(this);
  e_rel->expr_2->accept(this);

}

void DefScanner::visitEAnd(EAnd *e_and)
{
  /* Code For EAnd Goes Here */

  e_and->expr_1->accept(this);
  e_and->expr_2->accept(this);

}

void DefScanner::visitEOr(EOr *e_or)
{
  /* Code For EOr Goes Here */

  e_or->expr_1->accept(this);
  e_or->expr_2->accept(this);

}

void DefScanner::visitPlus(Plus *plus)
{
  /* Code For Plus Goes Here */


}

void DefScanner::visitMinus(Minus *minus)
{
  /* Code For Minus Goes Here */


}

void DefScanner::visitTimes(Times *times)
{
  /* Code For Times Goes Here */


}

void DefScanner::visitDiv(Div *div)
{
  /* Code For Div Goes Here */


}

void DefScanner::visitMod(Mod *mod)
{
  /* Code For Mod Goes Here */


}

void DefScanner::visitLTH(LTH *lth)
{
  /* Code For LTH Goes Here */


}

void DefScanner::visitLE(LE *le)
{
  /* Code For LE Goes Here */


}

void DefScanner::visitGTH(GTH *gth)
{
  /* Code For GTH Goes Here */


}

void DefScanner::visitGE(GE *ge)
{
  /* Code For GE Goes Here */


}

void DefScanner::visitEQU(EQU *equ)
{
  /* Code For EQU Goes Here */


}

void DefScanner::visitNE(NE *ne)
{
  /* Code For NE Goes Here */


}


void DefScanner::visitListTopDef(ListTopDef *list_top_def)
{
  for (ListTopDef::iterator i = list_top_def->begin() ; i != list_top_def->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void DefScanner::visitListArg(ListArg *list_arg)
{
  for (ListArg::iterator i = list_arg->begin() ; i != list_arg->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void DefScanner::visitListClsFldDefItem(ListClsFldDefItem *list_cls_fld_def_item)
{
  for (ListClsFldDefItem::iterator i = list_cls_fld_def_item->begin() ; i != list_cls_fld_def_item->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void DefScanner::visitListClassBlockDef(ListClassBlockDef *list_class_block_def)
{
  for (ListClassBlockDef::iterator i = list_class_block_def->begin() ; i != list_class_block_def->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void DefScanner::visitListStmt(ListStmt *list_stmt)
{
  for (ListStmt::iterator i = list_stmt->begin() ; i != list_stmt->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void DefScanner::visitListDeclItem(ListDeclItem *list_decl_item)
{
  for (ListDeclItem::iterator i = list_decl_item->begin() ; i != list_decl_item->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void DefScanner::visitListExpr(ListExpr *list_expr)
{
  for (ListExpr::iterator i = list_expr->begin() ; i != list_expr->end() ; ++i)
  {
    (*i)->accept(this);
  }
}


void DefScanner::visitInteger(Integer x)
{
  /* Code for Integer Goes Here */
}

void DefScanner::visitChar(Char x)
{
  /* Code for Char Goes Here */
}

void DefScanner::visitDouble(Double x)
{
  /* Code for Double Goes Here */
}

void DefScanner::visitString(String x)
{
  /* Code for String Goes Here */
}

void DefScanner::visitIdent(Ident x)
{
  /* Code for Ident Goes Here */
}
} // end of Frontend


