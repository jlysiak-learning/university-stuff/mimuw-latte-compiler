/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 */

#include <cassert>
#include <latte/frontend/BlockChecker.h>
#include <latte/frontend/ExprChecker.h>

namespace Frontend
{

using namespace Latte;

BlockChecker::BlockChecker(Common::LocalEnvironment &env, Common::Logger &logger,
		Blk* blk, std::shared_ptr<Common::Type> retType, bool allowDecl) :
	_env(env), _logger(logger), _retType(retType), _returns(false), 
	_can_return(false), _allowDecl(allowDecl)
	 
{
	_check(blk);
};

void BlockChecker::_check(Blk* blk)
{
	// Iterate over statements in Block
	for (ListStmt::iterator i = blk->liststmt_->begin() ; i != blk->liststmt_->end() ; ++i)
	{
		(*i)->accept(this);

		if (_logger.has_errors()) // Stop on first error..
			return;

		if (_returns && (blk->liststmt_->end() != (i+1))) {
			_logger.warn_at((*i)->line_number, "Some code is not reachable!");
			break; // Do not accept unreachable code
		}
	}
}

//=============================================================================

void BlockChecker::visitBStmt(BStmt *b_stmt)
{
	_logger.debug_at(b_stmt->line_number, "Checking nested block...");
	// Create copy of environment
	Common::LocalEnvironment localEnv(_env);
	// Visit nested block
	BlockChecker blockChecker(localEnv, _logger, (Blk*) b_stmt->block_, _retType, true);

	_returns = blockChecker.returns();
	_can_return = _can_return ? 1 : blockChecker.can_return();
}

void BlockChecker::visitDecl(Decl *decl)
{
	int line = decl->line_number;
	if (!_allowDecl) {
		_logger.error_at(line, "Cannot declare a variable in only "
			"statement of the loop of if/else! Use Block expression!");
		return;
	}

	auto type = Common::Type::from_type_AST(decl->type_);
	_logger.debug_at(line, "Declaring variables of type " + type->str());
	if (type->is<Common::Void>()) {
		_logger.error_at(line, "Cannot declare VOID type variable!");
		return;
	}
	_inType = type;
	decl->listdeclitem_->accept(this);
	_inType = nullptr;
}

void BlockChecker::visitAssign(Assign *assign)
{
	int line = assign->line_number;
	_logger.debug_at(line, "Statement - assign...");

	ExprChecker expr1(_env, _logger, assign->expr_1);
	ExprChecker expr2(_env, _logger, assign->expr_2);

	if (!expr1.is_ok()) { 
		_logger.error_at(line, "Incorrect left-side assignment expression!");
		return;
	}

	if (!expr2.is_ok()) {
		_logger.error_at(line, "Incorrect right-side assignment expression!");
		return;
	}

	if (!expr1.is_lval()) {
		_logger.error_at(line, "First expression is not a l-value!");
		return;
	}

	if ((*expr1.type() == *expr2.type()))
		return; // ok

	if (_env.is_child_of(expr2.type(), expr1.type()))
		return;

	_logger.error_at(assign->line_number, "Assignment of different types!");
}

void BlockChecker::visitIncr(Incr *incr)
{
	ExprChecker expr(_env, _logger, incr->expr_);
	if (!expr.is_ok()) {
		_logger.error_at(incr->line_number, "Incorrect expression!");
		return;
	}

	if (!expr.is_lval()) {
		_logger.error_at(incr->line_number, "Expression is not l-value!");
		return;
	}
	
	if (!expr.type()->is<Common::Int>()) 
		_logger.error_at(incr->line_number, "Expression type is not an Integer!");
}

void BlockChecker::visitDecr(Decr *decr)
{
	ExprChecker expr(_env, _logger, decr->expr_);
	if (!expr.is_ok()) {
		_logger.error_at(decr->line_number, "Incorrect expression!");
		return;
	}

	if (!expr.is_lval()) {
		_logger.error_at(decr->line_number, "Expression is not l-value!");
		return;
	}
	
	if (!expr.type()->is<Common::Int>())
		_logger.error_at(decr->line_number, "Expression type is not an Integer!");
}

void BlockChecker::visitRet(Ret *ret)
{
	ExprChecker expr(_env, _logger, ret->expr_);
	if (!expr.is_ok()) {
		_logger.error_at(ret->line_number, "Incorrect expression in RETURN!");
		return;
	}

	if (*expr.type() == *_retType) {
		_returns = true;
		_can_return = true;
		return;
	}

	if (_retType->is<Common::Void>()) {
		_logger.error_at(ret->line_number, "VOID function cannot return other type!!");
		return;
	}
		

	if (_env.is_child_of(expr.type(), _retType)) {
		_returns = true;
		_can_return = true;
		return;
	}

	_logger.error_at(ret->line_number, "Expression type is not compatible with " + _retType->str());
}

void BlockChecker::visitVRet(VRet *v_ret)
{
	if (!_retType->is<Common::Void>()) {
		_logger.error_at(v_ret->line_number,
			"Wrong return type. Should be: " + _retType->str() +
			" but there is VOID.");
		return;
	}
	_returns = true;
	_can_return = true;
}

void BlockChecker::visitWhile(While *while_)
{
	int line = while_->line_number;
	ExprChecker expr(_env, _logger, while_->expr_);
	if (!expr.is_ok()) {
		_logger.error_at(line, "Incorrect expression!");
		return;
	}

	if (!expr.type()->is<Common::Bool>()) {
		_logger.error_at(line, "Expression type in while condition is not Bool!");
		return;
	}

	/* First check block... */
	ListStmt *_tmpList = new ListStmt;
	_tmpList->push_back(while_->stmt_);
	Blk * _tmpBlk = new Blk(_tmpList);

	/* Do not allow do declare a variable in case of no-block statement.
	 * Visiting lang-level block will allow again to declare them. */
	BlockChecker blockChecker(_env, _logger, _tmpBlk, _retType, false); 

	/* Now check if it can be executed. */
	if (!expr.is_const()) // Both options possible, nothing more to do
		return;

	/* Ok, we know. */
	if (expr.const_()->b()) {  // Always true
		_logger.warn_at(line, "Possible infinite WHILE loop detected!");
		_returns = blockChecker.returns();
		if (blockChecker.can_return())
			_returns = true; // We can return from infinite loop, so why no...
		else
			_logger.error_at(line, "Infinite WHILE loop!");
		_can_return = _can_return ? 1 : blockChecker.can_return(); // We could ret before loop
		return;
	}
	_logger.debug_at(line, "WHILE loop will not be executed!");
}

void BlockChecker::visitFor(For *for_)
{
	int line = for_->expr_->line_number;
	std::string elName = for_->ident_;
	ExprChecker expr(_env, _logger, for_->expr_);

	if (!expr.is_ok()) {
		_logger.error_at(line, "Incorrect expression!");
		return;
	}
	if (!expr.type()->is<Common::Array>()) {
		_logger.error_at(line, "Expression type in FOR loop is not an ARRAY!");
		return;
	}

	auto type = Common::Type::from_type_AST(for_->type_);
	auto elType = std::dynamic_pointer_cast<Common::Array>(expr.type())->elem_type();
	if (*type != *elType) {
		if (!_env.is_child_of(elType, type)) {
			_logger.error_at(line, "FOR iterator variable type is "
				"not compatible with ARRAY element type!");
			return;
		}
	}

	ListStmt *_tmpList = new ListStmt;
	_tmpList->push_back(for_->stmt_);
	Blk * _tmpBlk = new Blk(_tmpList);
	Common::LocalEnvironment _tempEnv(_env);
	_tempEnv.add_variable(type, elName);
	/* Do not allow do declare a variable in case of no-block statement.
	 * Visiting lang-level block will allow again to declare them. */
	BlockChecker blockChecker(_tempEnv, _logger, _tmpBlk, _retType, false); 
	/* If block always returns -> this block also */
	_returns = blockChecker.returns();
	_can_return = _can_return ? 1 : blockChecker.can_return();
}

void BlockChecker::visitIf(If *if_)
{
	int line = if_->line_number;

	ExprChecker expr(_env, _logger, if_->expr_);
	if (!expr.is_ok()) {
		_logger.error_at(line, "Incorrect expression!");
		return;
	}
	if (!expr.type()->is<Common::Bool>()) {
		_logger.error_at(line, "IF condition expression is not of BOOL type!");
		return;
	}

	/* First check block... */
	ListStmt *_tmpList = new ListStmt;
	_tmpList->push_back(if_->stmt_);
	Blk * _tmpBlk = new Blk(_tmpList);
	/* Do not allow do declare a variable in case of no-block statement.
	 * Visiting lang-level block will allow again to declare them. */
	BlockChecker blockChecker(_env, _logger, _tmpBlk, _retType, false); 
	
	/* Now check if it can be executed. */
	if (!expr.is_const()) {// Both options possible, nothing more to do
		_can_return = _can_return ? 1 : blockChecker.can_return();
		return;
	}

	/* Ok, we know. */
	if (expr.const_()->b()) {  // Always true
		_logger.info_at(line, "IF branch will be always executed!");
		_returns = blockChecker.returns(); // If IF returns, return!
		_can_return = _can_return ? 1 : blockChecker.can_return();
		return;
	}
	_logger.warn_at(line, "IF branch will never be executed!");
}

void BlockChecker::visitIfElse(IfElse *if_else)
{
	int line = if_else->line_number;

	ExprChecker expr(_env, _logger, if_else->expr_);
	if (!expr.is_ok()) {
		_logger.error_at(line, "Incorrect expression!");
		return;
	}
	if (!expr.type()->is<Common::Bool>()) {
		_logger.error_at(line, "IF ELSE condition expression is not of BOOL type!");
		return;
	}

	/* First check blocks... */
	ListStmt *_tmpList1 = new ListStmt;
	ListStmt *_tmpList2 = new ListStmt;
	_tmpList1->push_back(if_else->stmt_1);
	_tmpList2->push_back(if_else->stmt_2);
	Blk * _tmpBlk1 = new Blk(_tmpList1);
	Blk * _tmpBlk2 = new Blk(_tmpList2);
	/* Do not allow do declare a variable in case of no-block statement.
	 * Visiting lang-level block will allow again to declare them. */
	BlockChecker blockChecker1(_env, _logger, _tmpBlk1, _retType, false); 
	BlockChecker blockChecker2(_env, _logger, _tmpBlk2, _retType, false); 
	
	/* Now check if it can be executed. */
	if (!expr.is_const()) {// Both options possible
		_returns = blockChecker1.returns() && blockChecker2.returns();
		_can_return = _can_return ? 1 : blockChecker1.can_return() || blockChecker2.can_return();
		return;
	}

	/* Ok, we know. */
	if (expr.const_()->b()) {  // IF branchj
		_logger.info_at(line, "IF branch will always be executed!");
		_returns = blockChecker1.returns(); // If IF returns, return!
		_can_return = _can_return ? 1 : blockChecker1.can_return();
	} else {
		_logger.info_at(line, "ELSE branch will always be executed!");
		_returns = blockChecker2.returns(); // If IF returns, return!
		_can_return = _can_return ? 1 : blockChecker2.can_return();
	}
}



void BlockChecker::visitNoInit(NoInit *no_init)
{
	int line = no_init->line_number;
	auto name = no_init->ident_;
	if (!_env.add_variable(_inType, name)) {
		_logger.error_at(line, "Variable `" + name + "` is not unique in this scope!");
	}
}

void BlockChecker::visitExp(Exp *exp)
{
	ExprChecker expr(_env, _logger, exp->expr_);
	if (!expr.is_ok())
		_logger.error_at(exp->line_number, "Incorrect expression!");

}

void BlockChecker::visitInit(Init *init)
{
	int line = init->line_number;
	auto name = init->ident_;

	_logger.debug_at(line, "Visiting declaration (" + _inType->str() + ")item " + name);

	ExprChecker expr(_env, _logger, init->expr_);
	if (!expr.is_ok()) {
		_logger.error_at(line, "Incorrect initialization expression!");
		return;
	}

	_logger.debug_at(line, "Init expr type is " + expr.type()->str());

	if (*expr.type() != *_inType) {
		if (!_env.is_child_of(expr.type(), _inType)) {
			_logger.error_at(line,
				"Initialization expression has incorrect type (" +
				expr.type()->str() + ") but variable should be of type " + 
				_inType->str());
			return;
		}
	}

	if (!_env.add_variable(_inType, name)) {
		_logger.error_at(line, "Variable `" + name + "` is not unique in this scope!");
	}
}




void BlockChecker::visitIdentP(IdentP *t) {} //abstract class
void BlockChecker::visitProgram(Program *t) {} //abstract class
void BlockChecker::visitTopDef(TopDef *t) {} //abstract class
void BlockChecker::visitFunDef(FunDef *t) {} //abstract class
void BlockChecker::visitArg(Arg *t) {} //abstract class
void BlockChecker::visitClassDef(ClassDef *t) {} //abstract class
void BlockChecker::visitClassBlock(ClassBlock *t) {} //abstract class
void BlockChecker::visitClassBlockDef(ClassBlockDef *t) {} //abstract class
void BlockChecker::visitClsFldDefItem(ClsFldDefItem *t) {} //abstract class
void BlockChecker::visitBlock(Block *t) {} //abstract class
void BlockChecker::visitStmt(Stmt *t) {} //abstract class
void BlockChecker::visitDeclItem(DeclItem *t) {} //abstract class
void BlockChecker::visitClassType(ClassType *t) {} //abstract class
void BlockChecker::visitBasicType(BasicType *t) {} //abstract class
void BlockChecker::visitNonVoidType(NonVoidType *t) {} //abstract class
void BlockChecker::visitType(Type *t) {} //abstract class
void BlockChecker::visitCastOn(CastOn *t) {} //abstract class
void BlockChecker::visitExpr(Expr *t) {} //abstract class
void BlockChecker::visitAddOp(AddOp *t) {} //abstract class
void BlockChecker::visitMulOp(MulOp *t) {} //abstract class
void BlockChecker::visitRelOp(RelOp *t) {} //abstract class

void BlockChecker::visitIdentPos(IdentPos *ident_pos)
{
  /* Code For IdentPos Goes Here */

  visitIdent(ident_pos->ident_);

}

void BlockChecker::visitProg(Prog *prog)
{
  /* Code For Prog Goes Here */

  prog->listtopdef_->accept(this);

}

void BlockChecker::visitClsDefTop(ClsDefTop *cls_def_top)
{
  /* Code For ClsDefTop Goes Here */

  cls_def_top->classdef_->accept(this);

}

void BlockChecker::visitFnDefTop(FnDefTop *fn_def_top)
{
  /* Code For FnDefTop Goes Here */

  fn_def_top->fundef_->accept(this);

}

void BlockChecker::visitFnDef(FnDef *fn_def)
{
  /* Code For FnDef Goes Here */

  fn_def->type_->accept(this);
  fn_def->identp_->accept(this);
  fn_def->listarg_->accept(this);
  fn_def->block_->accept(this);

}

void BlockChecker::visitEmpty(Empty *p) {}

void BlockChecker::visitAr(Ar *ar)
{
  /* Code For Ar Goes Here */

  ar->type_->accept(this);
  ar->identp_->accept(this);

}

void BlockChecker::visitClsDef(ClsDef *cls_def)
{
  /* Code For ClsDef Goes Here */

  cls_def->identp_->accept(this);
  cls_def->classblock_->accept(this);

}

void BlockChecker::visitClsExtDef(ClsExtDef *cls_ext_def)
{
  /* Code For ClsExtDef Goes Here */

  cls_ext_def->identp_->accept(this);
  visitIdent(cls_ext_def->ident_);
  cls_ext_def->classblock_->accept(this);

}

void BlockChecker::visitClsBlk(ClsBlk *cls_blk)
{
  /* Code For ClsBlk Goes Here */

  cls_blk->listclassblockdef_->accept(this);

}

void BlockChecker::visitClsMthDef(ClsMthDef *cls_mth_def)
{
  /* Code For ClsMthDef Goes Here */

  cls_mth_def->fundef_->accept(this);

}

void BlockChecker::visitClsFldDef(ClsFldDef *cls_fld_def)
{
  /* Code For ClsFldDef Goes Here */

  cls_fld_def->type_->accept(this);
  cls_fld_def->listclsflddefitem_->accept(this);

}

void BlockChecker::visitClsFldDefIt(ClsFldDefIt *cls_fld_def_it)
{
  /* Code For ClsFldDefIt Goes Here */

  cls_fld_def_it->identp_->accept(this);

}

void BlockChecker::visitBlk(Blk *blk)
{
  /* Code For Blk Goes Here */

  blk->liststmt_->accept(this);

}


void BlockChecker::visitTClass(TClass *t_class)
{
  /* Code For TClass Goes Here */

  visitIdent(t_class->ident_);

}

void BlockChecker::visitTInt(TInt *t_int)
{
  /* Code For TInt Goes Here */


}

void BlockChecker::visitTStr(TStr *t_str)
{
  /* Code For TStr Goes Here */


}

void BlockChecker::visitTBool(TBool *t_bool)
{
  /* Code For TBool Goes Here */


}

void BlockChecker::visitTNVC(TNVC *tnvc)
{
  /* Code For TNVC Goes Here */

  tnvc->classtype_->accept(this);

}

void BlockChecker::visitTNVB(TNVB *tnvb)
{
  /* Code For TNVB Goes Here */

  tnvb->basictype_->accept(this);

}

void BlockChecker::visitTArray(TArray *t_array)
{
  /* Code For TArray Goes Here */

  t_array->nonvoidtype_->accept(this);

}

void BlockChecker::visitTSingle(TSingle *t_single)
{
  /* Code For TSingle Goes Here */

  t_single->nonvoidtype_->accept(this);

}

void BlockChecker::visitTVoid(TVoid *t_void)
{
  /* Code For TVoid Goes Here */


}

void BlockChecker::visitCastOnClass(CastOnClass *cast_on_class)
{
  /* Code For CastOnClass Goes Here */

  visitIdent(cast_on_class->ident_);

}

void BlockChecker::visitCastOnArr(CastOnArr *cast_on_arr)
{
  /* Code For CastOnArr Goes Here */

  cast_on_arr->nonvoidtype_->accept(this);

}

void BlockChecker::visitCastOnBasic(CastOnBasic *cast_on_basic)
{
  /* Code For CastOnBasic Goes Here */

  cast_on_basic->basictype_->accept(this);

}

void BlockChecker::visitENewArr(ENewArr *e_new_arr)
{
  /* Code For ENewArr Goes Here */

  e_new_arr->nonvoidtype_->accept(this);
  e_new_arr->expr_->accept(this);

}

void BlockChecker::visitENewCls(ENewCls *e_new_cls)
{
  /* Code For ENewCls Goes Here */

  visitIdent(e_new_cls->ident_);

}

void BlockChecker::visitELitInt(ELitInt *e_lit_int)
{
  /* Code For ELitInt Goes Here */

  visitInteger(e_lit_int->integer_);

}

void BlockChecker::visitELitTrue(ELitTrue *e_lit_true)
{
  /* Code For ELitTrue Goes Here */


}

void BlockChecker::visitELitFalse(ELitFalse *e_lit_false)
{
  /* Code For ELitFalse Goes Here */


}

void BlockChecker::visitEString(EString *e_string)
{
  /* Code For EString Goes Here */

  visitString(e_string->string_);

}

void BlockChecker::visitEVar(EVar *e_var)
{
  /* Code For EVar Goes Here */

  visitIdent(e_var->ident_);

}

void BlockChecker::visitENull(ENull *e_null)
{
  /* Code For ENull Goes Here */


}

void BlockChecker::visitEMemb(EMemb *e_memb)
{
  /* Code For EMemb Goes Here */

  e_memb->expr_->accept(this);
  visitIdent(e_memb->ident_);

}

void BlockChecker::visitEFunCall(EFunCall *e_fun_call)
{
  /* Code For EFunCall Goes Here */

  visitIdent(e_fun_call->ident_);
  e_fun_call->listexpr_->accept(this);

}

void BlockChecker::visitEMembCall(EMembCall *e_memb_call)
{
  /* Code For EMembCall Goes Here */

  e_memb_call->expr_->accept(this);
  visitIdent(e_memb_call->ident_);
  e_memb_call->listexpr_->accept(this);

}

void BlockChecker::visitEAt(EAt *e_at)
{
  /* Code For EAt Goes Here */

  e_at->expr_1->accept(this);
  e_at->expr_2->accept(this);

}

void BlockChecker::visitECast(ECast *e_cast)
{
  /* Code For ECast Goes Here */

  e_cast->caston_->accept(this);
  e_cast->expr_->accept(this);

}

void BlockChecker::visitNeg(Neg *neg)
{
  /* Code For Neg Goes Here */

  neg->expr_->accept(this);

}

void BlockChecker::visitNot(Not *not_)
{
  /* Code For Not Goes Here */

  not_->expr_->accept(this);

}

void BlockChecker::visitEMul(EMul *e_mul)
{
  /* Code For EMul Goes Here */

  e_mul->expr_1->accept(this);
  e_mul->mulop_->accept(this);
  e_mul->expr_2->accept(this);

}

void BlockChecker::visitEAdd(EAdd *e_add)
{
  /* Code For EAdd Goes Here */

  e_add->expr_1->accept(this);
  e_add->addop_->accept(this);
  e_add->expr_2->accept(this);

}

void BlockChecker::visitERel(ERel *e_rel)
{
  /* Code For ERel Goes Here */

  e_rel->expr_1->accept(this);
  e_rel->relop_->accept(this);
  e_rel->expr_2->accept(this);

}

void BlockChecker::visitEAnd(EAnd *e_and)
{
  /* Code For EAnd Goes Here */

  e_and->expr_1->accept(this);
  e_and->expr_2->accept(this);

}

void BlockChecker::visitEOr(EOr *e_or)
{
  /* Code For EOr Goes Here */

  e_or->expr_1->accept(this);
  e_or->expr_2->accept(this);

}

void BlockChecker::visitPlus(Plus *plus)
{
  /* Code For Plus Goes Here */


}

void BlockChecker::visitMinus(Minus *minus)
{
  /* Code For Minus Goes Here */


}

void BlockChecker::visitTimes(Times *times)
{
  /* Code For Times Goes Here */


}

void BlockChecker::visitDiv(Div *div)
{
  /* Code For Div Goes Here */


}

void BlockChecker::visitMod(Mod *mod)
{
  /* Code For Mod Goes Here */


}

void BlockChecker::visitLTH(LTH *lth)
{
  /* Code For LTH Goes Here */


}

void BlockChecker::visitLE(LE *le)
{
  /* Code For LE Goes Here */


}

void BlockChecker::visitGTH(GTH *gth)
{
  /* Code For GTH Goes Here */


}

void BlockChecker::visitGE(GE *ge)
{
  /* Code For GE Goes Here */


}

void BlockChecker::visitEQU(EQU *equ)
{
  /* Code For EQU Goes Here */


}

void BlockChecker::visitNE(NE *ne)
{
  /* Code For NE Goes Here */


}


void BlockChecker::visitListTopDef(ListTopDef *list_top_def)
{
  for (ListTopDef::iterator i = list_top_def->begin() ; i != list_top_def->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void BlockChecker::visitListArg(ListArg *list_arg)
{
  for (ListArg::iterator i = list_arg->begin() ; i != list_arg->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void BlockChecker::visitListClsFldDefItem(ListClsFldDefItem *list_cls_fld_def_item)
{
  for (ListClsFldDefItem::iterator i = list_cls_fld_def_item->begin() ; i != list_cls_fld_def_item->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void BlockChecker::visitListClassBlockDef(ListClassBlockDef *list_class_block_def)
{
  for (ListClassBlockDef::iterator i = list_class_block_def->begin() ; i != list_class_block_def->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void BlockChecker::visitListStmt(ListStmt *list_stmt)
{
  for (ListStmt::iterator i = list_stmt->begin() ; i != list_stmt->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void BlockChecker::visitListDeclItem(ListDeclItem *list_decl_item)
{
  for (ListDeclItem::iterator i = list_decl_item->begin() ; i != list_decl_item->end() ; ++i)
  {
    (*i)->accept(this);
  }
}

void BlockChecker::visitListExpr(ListExpr *list_expr)
{
  for (ListExpr::iterator i = list_expr->begin() ; i != list_expr->end() ; ++i)
  {
    (*i)->accept(this);
  }
}


void BlockChecker::visitInteger(Integer x)
{
  /* Code for Integer Goes Here */
}

void BlockChecker::visitChar(Char x)
{
  /* Code for Char Goes Here */
}

void BlockChecker::visitDouble(Double x)
{
  /* Code for Double Goes Here */
}

void BlockChecker::visitString(String x)
{
  /* Code for String Goes Here */
}

void BlockChecker::visitIdent(Ident x)
{
  /* Code for Ident Goes Here */
}

}

