/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * The TypeChecker.
 */

#include <cassert>
#include <latte/frontend/TypeChecker.h>
#include <latte/frontend/BlockChecker.h>
#include <latte/common/LocalEnvironment.h>

namespace Frontend
{

using namespace Latte;

TypeChecker::TypeChecker(Common::Environment &env, Common::Logger &logger):
	_env(env), _logger(logger) { }

error_t TypeChecker::check()
{
	const auto clsMap = _env.classes();
	const auto funMap = _env.functions();
	error_t err;

	// Check classes
	for (auto clsIt = clsMap.begin(); clsIt != clsMap.end(); ++clsIt) {
		Common::LocalEnvironment localEnv(_env);
		auto cls = clsIt->second;
		while (cls != nullptr) {
			for (auto fld : cls->get_fields())
				localEnv.add_variable(fld.second, fld.first);
			cls = cls->get_parent();
		}
		localEnv.add_variable(clsIt->second->type(), std::string("self"));
		// Check methods
		for (auto fun : clsIt->second->get_methods()) {
			_logger.debug("TypeChecker: Check method " +
				fun.first + " in class " + clsIt->first);
			err = _check_function(localEnv, fun.second);
			if (err != SUCCESS)
				return err;
		}
	}

	// Check functions
	for (auto fun : funMap) {
		if (fun.second->isPredefined())
			continue;
		Common::LocalEnvironment localEnv(_env);
		_logger.debug("TypeChecker: Check function " +
			fun.first + " in global scope");
		err = _check_function(localEnv, fun.second);
		if (err != SUCCESS)
			return err;
	}

	return SUCCESS;
}

error_t TypeChecker::_check_function(const Common::LocalEnvironment& env, 
			std::shared_ptr<Common::FunctionDefinition> fun)
{
	Common::LocalEnvironment localEnv(env);

	for (auto arg : fun->get_args())
		localEnv.add_variable(arg.first, arg.second);

	bool shouldRet = !fun->type()->is<Common::Void>();
	Common::LocalEnvironment localEnv2(localEnv);
	BlockChecker blockChecker(localEnv2, _logger, fun->get_body(), fun->type(), true); // allow to declare var. in block
	if (_logger.has_errors())
		return FAIL;
	if (shouldRet) {
		if (blockChecker.returns())
			return SUCCESS;
		_logger.error_at(fun->get_line(), "Function should return a value of type " + 
			fun->type()->str() + " but it doesn't!");
		return FAIL;
	}
	return SUCCESS;
}


}
